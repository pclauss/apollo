#!/usr/bin/env bash
#===--- utils/docker/scripts/checkout.sh  --------------------------------===//
#
# APOLLO - Automatic speculative POLyhedral Loop Optimizer.
#
#===----------------------------------------------------------------------===//
#
# The BSD 3-Clause License
#
# Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Main Contributors:
#     Professor Philippe Clauss     <clauss@unistra.fr>
#     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
#     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
#     Artiom Baloian                <artiom.baloian@inria.fr>
#
# Participated as internships:
#     Willy Wolff
#     Matias Perez
#     Esteban Campostrini
#
#===-----------------------------------------------------------------------===//
#===- based on llvm/utils/docker/scripts/checkout.sh ----------------------===//
#
# Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
# See https://llvm.org/LICENSE.txt for license information.
# SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
#
#===-----------------------------------------------------------------------===//

set -e

function show_usage() {
  cat << EOF
Usage: checkout.sh [options]

Checkout git sources into /tmp/apollo-build/src. Used inside a docker container.

Available options:
  -h|--help           show this help message
  -b|--branch         git branch to checkout, i.e. 'main',
                      'release/10.x'
                      (default: 'main')
  -r|--revision       git revision to checkout
  -c|--cherrypick     revision to cherry-pick. Can be specified multiple times.
                      Cherry-picks are performed in the sorted order using the
                      following command:
                      'git cherry-pick \$rev)'.
  --apollo-dir        checkout directory
EOF
}

LLVM_GIT_REV=""
CHERRYPICKS=""
LLVM_BRANCH=""
CLANG_BUILD_DIR=/tmp/apollo-build

while [[ $# -gt 0 ]]; do
  case "$1" in
    -r|--revision)
      shift
      LLVM_GIT_REV="$1"
      shift
      ;;
    -c|--cherrypick)
      shift
      CHERRYPICKS="$CHERRYPICKS $1"
      shift
      ;;
    -b|--branch)
      shift
      LLVM_BRANCH="$1"
      shift
      ;;
    -h|--help)
      show_usage
      exit 0
      ;;
    --apollo-dir)
      shift
      CLANG_BUILD_DIR=$1
      shift
      ;;
    *)
      echo "Unknown option: $1"
      exit 1
  esac
done

if [ "$LLVM_BRANCH" == "" ]; then
  LLVM_BRANCH="master"
fi

if [ "$LLVM_GIT_REV" != "" ]; then
  GIT_REV_ARG="$LLVM_GIT_REV"
  echo "Checking out git revision $LLVM_GIT_REV."
else
  GIT_REV_ARG=""
  echo "Checking out latest git revision."
fi

# Sort cherrypicks and remove duplicates.
CHERRYPICKS="$(echo "$CHERRYPICKS" | xargs -n1 | sort | uniq | xargs)"

function apply_cherrypicks() {
  local CHECKOUT_DIR="$1"

  [ "$CHERRYPICKS" == "" ] || echo "Applying cherrypicks"
  pushd "$CHECKOUT_DIR"

  # This function is always called on a sorted list of cherrypicks.
  for CHERRY_REV in $CHERRYPICKS; do
    echo "Cherry-picking $CHERRY_REV into $CHECKOUT_DIR"
    EMAIL="someone@somewhere.net" git cherry-pick $CHERRY_REV
  done

  popd
}

# Get the sources from git.
echo "Checking out sources from git"
mkdir -p "$CLANG_BUILD_DIR/src"
CHECKOUT_DIR="$CLANG_BUILD_DIR/src"

echo "Checking out https://gitlab.inria.fr/pclauss/apollo.git to $CHECKOUT_DIR"
git clone -b $LLVM_BRANCH --single-branch \
  "https://gitlab.inria.fr/pclauss/apollo.git" \
  "$CHECKOUT_DIR"

pushd $CHECKOUT_DIR
git checkout -q $GIT_REV_ARG
popd

  # We apply cherrypicks to all repositories regardless of whether the revision
  # changes this repository or not. For repositories not affected by the
  # cherrypick, applying the cherrypick is a no-op.
  apply_cherrypicks "$CHECKOUT_DIR"

echo "Done"
