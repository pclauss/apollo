#!/usr/bin/env bash
#===--- utils/docker/scripts/build_install_llvm.sh -----------------------===//
#
# APOLLO - Automatic speculative POLyhedral Loop Optimizer.
#
#===----------------------------------------------------------------------===//
#
# The BSD 3-Clause License
#
# Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Main Contributors:
#     Professor Philippe Clauss     <clauss@unistra.fr>
#     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
#     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
#     Artiom Baloian                <artiom.baloian@inria.fr>
#
# Participated as internships:
#     Willy Wolff
#     Matias Perez
#     Esteban Campostrini
#
#===-----------------------------------------------------------------------===//
#===- based on llvm/utils/docker/scripts/build_install_llvm.sh ------------===//
#
# Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
# See https://llvm.org/LICENSE.txt for license information.
# SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
#
#===-----------------------------------------------------------------------===//

set -e

function show_usage() {
  cat << EOF
Usage: build_install_apollo.sh [options] -- [cmake-args]

Run cmake with the specified arguments. Used inside docker container.
Passes additional -DCMAKE_INSTALL_PREFIX and puts the build results into
the directory specified by --to option.

Available options:
  -h|--help           show this help message
  -i|--install-target name of a cmake install target to build and include in
                      the resulting archive. Can be specified multiple times.
  --to                destination directory where to install the targets.
  --apollo-dir        apollo directory
  --no-clean          prevent directory cleaning (for CI usage)
Required options: --to, at least one --install-target.

All options after '--' are passed to CMake invocation.
EOF
}

CMAKE_ARGS=""
CMAKE_INSTALL_TARGETS=""
CLANG_INSTALL_DIR=""
CLEAN_BUILD_DIR=1
APOLLO_BUILD_DIR=/tmp/apollo-build

while [[ $# -gt 0 ]]; do
  case "$1" in
    -i|--install-target)
      shift
      CMAKE_INSTALL_TARGETS="$CMAKE_INSTALL_TARGETS $1"
      shift
      ;;
    --to)
      shift
      CLANG_INSTALL_DIR="$1"
      shift
      ;;
    --)
      shift
      CMAKE_ARGS="$*"
      shift $#
      ;;
    -h|--help)
      show_usage
      exit 0
      ;;
    --no-clean)
      CLEAN_BUILD_DIR=0
      shift
      ;;
    --apollo-dir)
      shift
      APOLLO_BUILD_DIR=$1
      shift
      ;;
    *)
      echo "Unknown option: $1"
      exit 1
  esac
done

if [ "$CMAKE_INSTALL_TARGETS" == "" ]; then
  echo "No install targets. Please pass one or more --install-target."
  exit 1
fi

if [ "$CLANG_INSTALL_DIR" == "" ]; then
  echo "No install directory. Please specify the --to argument."
  exit 1
fi

mkdir -p "$CLANG_INSTALL_DIR"

mkdir -p "$APOLLO_BUILD_DIR/build"
pushd "$APOLLO_BUILD_DIR/build"

# Run the build as specified in the build arguments.
echo "Running build"
cmake -GNinja \
  -DCMAKE_INSTALL_PREFIX="$CLANG_INSTALL_DIR" \
  $CMAKE_ARGS \
  "$APOLLO_BUILD_DIR/src/"
ninja $CMAKE_INSTALL_TARGETS

popd

# Cleanup.
if [ $CLEAN_BUILD_DIR -eq 1 ]; then
  rm -rf "$APOLLO_BUILD_DIR/build"
fi

echo "Done"
