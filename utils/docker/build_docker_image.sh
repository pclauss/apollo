#!/bin/bash
#===--- utils/docker/build_docker_image.sh -------------------------------===//
#
# APOLLO - Automatic speculative POLyhedral Loop Optimizer.
#
#===----------------------------------------------------------------------===//
#
# The BSD 3-Clause License
#
# Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Main Contributors:
#     Professor Philippe Clauss     <clauss@unistra.fr>
#     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
#     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
#     Artiom Baloian                <artiom.baloian@inria.fr>
#
# Participated as internships:
#     Willy Wolff
#     Matias Perez
#     Esteban Campostrini
#
#===----------------------------------------------------------------------===//
#===- based on llvm/utils/docker/build_docker_image.sh -------------------===//
#
# Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
# See https://llvm.org/LICENSE.txt for license information.
# SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
#
#===----------------------------------------------------------------------===//
set -e

IMAGE_SOURCE=""
DOCKER_REPOSITORY=""
DOCKER_TAG=""
BUILDSCRIPT_ARGS=""
CHECKOUT_ARGS=""

function show_usage() {
  cat << EOF
Usage: build_docker_image.sh [options] [-- [cmake_args]...]

Available options:
  General:
    -h|--help               show this help message
    --apollo-dir            build directory
  Docker-specific:
    -s|--source             image source dir (i.e. debian10, nvidia-cuda, etc)
    -d|--docker-repository  docker repository for the image
    -t|--docker-tag         docker tag for the image
  Checkout arguments:
    -b|--branch             git branch to checkout, i.e. 'main',
                            'release/10.x'
                            (default: 'main')
    -r|--revision           git revision to checkout
    -c|--cherrypick         revision to cherry-pick. Can be specified multiple times.
                            Cherry-picks are performed in the sorted order using the
                            following command:
                            'git cherry-pick \$rev'.
  Build-specific:
    -i|--install-target     name of a cmake install target to build and include in
                            the resulting archive. Can be specified multiple times.
    --no-clean              prevent directory cleaning (for CI usage)

Required options: --source and --docker-repository

All options after '--' are passed to CMake invocation.

For example, running:
$ build_docker_image.sh -s debian10 -d debian10 -t latest -i apollo -i install
will produce one docker image containing the built apollo."
EOF
}

CHECKSUMS_FILE=""
SEEN_INSTALL_TARGET=0
SEEN_CMAKE_ARGS=0
while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      show_usage
      exit 0
      ;;
    -s|--source)
      shift
      IMAGE_SOURCE="$1"
      shift
      ;;
    -d|--docker-repository)
      shift
      DOCKER_REPOSITORY="$1"
      shift
      ;;
    -t|--docker-tag)
      shift
      DOCKER_TAG="$1"
      shift
      ;;
    -r|--revision|-c|--cherrypick|-b|--branch)
      CHECKOUT_ARGS="$CHECKOUT_ARGS $1 $2"
      shift 2
      ;;
    -i|--install-target)
      SEEN_INSTALL_TARGET=1
      BUILDSCRIPT_ARGS="$BUILDSCRIPT_ARGS $1 $2"
      shift 2
      ;;
    --no-clean)
      BUILDSCRIPT_ARGS="$BUILDSCRIPT_ARGS --no-clean"
      shift
      ;;
    --apollo-dir)
      CHECKOUT_ARGS="$CHECKOUT_ARGS $1 $2"
      BUILDSCRIPT_ARGS="$BUILDSCRIPT_ARGS $1 $2"
      shift 2
      ;;
    --)
      shift
      BUILDSCRIPT_ARGS="$BUILDSCRIPT_ARGS -- $*"
      SEEN_CMAKE_ARGS=1
      shift $#
      ;;
    *)
      echo "Unknown argument $1"
      exit 1
      ;;
  esac
done

command -v docker >/dev/null ||
  {
    echo "Docker binary cannot be found. Please install Docker to use this script."
    exit 1
  }

if [ "$IMAGE_SOURCE" == "" ]; then
  echo "Required argument missing: --source"
  exit 1
fi

if [ "$DOCKER_REPOSITORY" == "" ]; then
  echo "Required argument missing: --docker-repository"
  exit 1
fi

if [ $SEEN_INSTALL_TARGET -eq 0 ]; then
  echo "No --install-target precised, defaulting to --instal-target install"
  BUILDSCRIPT_ARGS="-i install $BUILDSCRIPT_ARGS"
fi

SOURCE_DIR=$(dirname $0)
if [ ! -d "$SOURCE_DIR/$IMAGE_SOURCE" ]; then
  echo "No sources for '$IMAGE_SOURCE' were found in $SOURCE_DIR"
  exit 1
fi

BUILD_DIR=$(mktemp -d)
trap "rm -rf $BUILD_DIR" EXIT
echo "Using a temporary directory for the build: $BUILD_DIR"

cp -r "$SOURCE_DIR/$IMAGE_SOURCE" "$BUILD_DIR/$IMAGE_SOURCE"
cp -r "$SOURCE_DIR/scripts" "$BUILD_DIR/scripts"

if [ "$DOCKER_TAG" != "" ]; then
  DOCKER_TAG=":$DOCKER_TAG"
fi

echo "Building ${DOCKER_REPOSITORY}${DOCKER_TAG} from $IMAGE_SOURCE"
docker build -t "${DOCKER_REPOSITORY}${DOCKER_TAG}" \
  --build-arg "checkout_args=$CHECKOUT_ARGS" \
  --build-arg "buildscript_args=$BUILDSCRIPT_ARGS" \
  -f "$BUILD_DIR/$IMAGE_SOURCE/Dockerfile" \
  "$BUILD_DIR"
echo "Done"
