-*- mode: org -*-

* APOLLO Implementation

-  Modified Clang frontend:  at the output of the  clang frontend, all
   the LLVM IR  instructions, except the Phi ones (MANU,  I don't know
   yet why)  inside a loop nest  tagged with #pragma apollo  dcop have
   the metadata kind apollo.pragma with the value 1

-  ApolloPass::runOnModule() method  returns true  only if  the *last*
   runOnApollo(Function *F) returned  true. Is it normal ?  To me it's
   an error and it should return true if *at least one* of the call to
   runOnApollo(Function *F) returned true.
  
** Apollo Static Passes

Some standard LLVM  passes are called before the Apollo  ones and some
standard  LLVM passes  are interleaved  with the  Apollo ones.  Apollo
passes are executed in the following order:

*** Preparation

1. *ApolloLoopExtract* - Preparation - ApolloPass - This pass extracts
   the loop nests tagged by the frontend with #pragma apollo dcop into
   their  own functions.  These  functions  are named  apollo_loop_IDl
   where IDl is a simple counter incremented for each tagged nest. The
   extracted    function   has    "noinline"   and    "privatelinkage"
   properties. Moreover,  this passes  also removes  the apollo.pragma
   metadata associated by the frontend to each instruction in th eloop
   nest.

2. *ApolloPromoteMemIntrinsics* - Preparation - ApolloPass - This pass
   transforms   llvm  memory   intrisics  (llv.memcpy,   llvm.memmove,
   llvm.memset)  into  store  instructions.  If the  number  of  store
   instructions to  be performed is  known statically and less  than a
   threshold, this pass unrolls store instructions, otherwise they are
   performed in a loop

3. *ApolloIgnoreEmpty* -  Preparation - ApolloPas -  This pass changes
   the   "noinline"   property   of   apollo_loop_IDl   functions   to
   "alwaysinline" for apollo functions that do not contain any loop.

4. *ApolloSignatureNormalize*  - Preparation - ApolloPass  - This pass
   normalizes the signature of apollo_loop_IDl  functions to be in the
   following       form:      %apollo.apollo_loop_IDl_param*       %p,
   %apollo.apollo_loop_ID_phi_state* %ps.I don't know  yet what is the
   purpose of that

5. *APolloPhiHandling* - MetadataAdders - ApolloPass - This pass sets
   (through metadata) a PhiId and a parent loopId to each PhiNode (one
   at each depth level) of the apollo loop nest

6. *ApolloStatementMetadataAdder* - MetadataAdders - ApolloPass - This pass
   sets (through  metadata) a StatmentId  and a parent loopId  to each
   memory access (both loads and stores) of the apollo loop nest

7. *ApolloFunctionCallHandling* -  MetadaAdders -  ApolloPass -  This
   pass sets (through  metadata) a ParentLoopId to  each function call
   of the apollo loop nest

8. *ApolloStatementVerificationAvoidance*   -    MetadataAdders   -
   ApolloPass - This pass identifies  apollo loop iterators and memory
   accesses that are affine.  It  attaches the folowing metadata kinds
   to loop iterators and  memory access statements: - isAffineIterator
   isAffineIterator   -  isAffine   isAffine  -   isAffineFromVerified
   VerifIds - NonLinearAtLevel NonLinearLoopId MANU - This information
   is  probably used  by  the runtime  to  avoid performing  expensive
   checks and  verifications on code that  we know statically it  is a
   SCoP

*** Skeleton 0 - Register Static Info

9. *RegisterStaticInfo*  - Dumpers  - ApolloPass  - This  pass first
   declares  the   runtime  function   apollo_register_static_info  as
   external.  Then it  creates an  apollo_loop_IDl_skeleton_0 function
   where it  inserts a  basic block  called "register".  Then severals
   calls to the apollo_register_static_info function are added to this
   basic  block to  register:  init info,  loops  info, scalars  info,
   statements info and params size info.

10. *RegisterStmtAlias* -  Dumpers  - ApolloPass  -  For each  memory
    access  in  the  apollo_loop_ID function,  this  passes  registers
    (calling the the  apollo_register_static_info external function in
    the   apollo_loop_IDl_skeleton_0   function)   the   NO   aliasing
    information. In other words, it registers  the ID of all the other
    statements  that are  for sure  not  alias to  the current  memory
    access.

*** Skeleton 1 - Instrumentation

11. *ApolloCloneLoopFunction*  - Preparation  -  ApolloPassOnSkeleton
     ""  -  From  ""  to   "skeleton_1"  -  This  passe  simply  clones
    apollo_loop_IDl    functions    with     a    new    name    being
    apollo_loop_IDl_skeleton_1. Skeleton  number 1 corresponds  to the
    instrumented  version  of the  loop.  In  other words,  this  pass
    creates the function representing  the instrumentated version (for
    profiling) of an apollo loop nest. This function (representing the
    instrumented  version)  will be  then  modified  by the  following
    passes to  insert profiling  code and  save the  results somewhere
    accessible by the runtime.

12. *InstrumentFixFunctionSignature*    -     Instrumentation    -
    ApolloPassOnSkeleton  "skeleton_1"  -   This  passe  modifies  the
    signature  of the  instrumented function  by adding  the following
    parameters:  i8* %instrumentation_result,  i64 %apollo_lower,  i64
    %apollo_upper,     i64     %apollo_outer_instrument_bound,     i64
    %apollo_inner_instrument_bound.

13. *ApolloInstrumentLinearEquation*    -     Instrumentation    -
    ApolloPassOnSkeleton "skeleton_1" - This pass  creates a new block
    in   the    instrumentation   function   (==    skeleton)   called
    register_static_lf.      This     block     makes     calls     to
    apollo_register_event  function  with  types: ScaSta,  MemSta  and
    BoundSta.   In  other words,  these  function  calls register  the
    coefficient of  statically computable linear equations  for memory
    accesses,  loop  bounds  and  scalar values.  When  this  pass  is
    executed, virtual iterators  have not yet been  introduced. So the
    scalars  (the phi  instructions) correspond  to the  initial loops
    counters (i,j,k, ...) and I guess to scalars that could be used in
    the loop.  This pass  also add  the LinEq  metadata to  the memory
    accesses and the  phi instructions. For loop  bounds, because they
    are not associated to a particular instruction, a fake instruction
    (fake_lineq_for_iteration_count_pass)  performing   a  bitcast  is
    added to store the metadata.

14. *ApolloReg2Mem*   -   Instrumentation   -   ApolloPassOnSkeleton
    "skeleton_1" - (to be exact, this  is an ApolloPass running on the
    skeleton_1 function but because  it needs the original apollo_loop
    function to get some information it's not an ApolloPassOnSkeleton)
    This  pass removes  all  the phi  instructions  in the  skeleton_1
    function  by   replacing  them   with  alloca   /  load   /  store
    instructions. This  pass also  inserts the  BasicScalarInitial and
    the BasicScalarFinal metada to the new instructions added.
    
15. *ApolloViInsert*   -  Instrumentation   -   ApolloPassOnSkeleton
    "skeleton_1" -  This pass inserts  a new virtual iterator  at each
    level of  the loop nest.  This  iterator is incremented by  one on
    each iteration of  the loop containing it. The  outer most virtual
    iterator is  used to check the  exit condition for the  outer most
    loop. Indeed, the instrumentation function  is called on chunks of
    data, and the caller is reponsible  to ensure that the upper value
    of the chunk is  not above the upper bound of  the outer most loop
    as specified  in original code. For  the other loops in  the nest,
    the initial "loop counter" which is  now a scalar allocated on the
    stack following  the previous (ApolloReg2Mem) pass  are still used
    toi check the exit condition.

16. *ApolloInstrumentViBounds*      -      Instrumentation      -
    ApolloPassOnSkeleton  "skeleton_1" -  This pass  inserts the  loop
    exit condition  based on  the virtual iterator  of the  outer most
    loop.  The exit condition is reached when this outer most iterator
    value  > apollo_upper  which is  provided  as an  argument of  the
    instrumentation  function. This  pass  also insert  initialization
    code for  the virtual iterators  at each  level of the  loop nest.
    For the outer most VI, the  init value is apollo_lower and for all
    the other VIs it's 0.

17. *ApolloInstrumentPhiState*      -      Instrumentation      -
    ApolloPassOnSkeleton  "skeleton_1" -  This pass  inserts the  code
    required  to initialize  all the  scalars of  the outermost  loop.
    These  scalars corresponds  to alloca  instructions before/outside
    the  outermost loop  with the  metadata ApolloPhiID:  these alloca
    where previously  phi nodes). The initialization  of these scalars
    is  required because  the  instrumentation function  is called  on
    different  chunks   between  apollo_lower  and   apollo_upper.  If
    apollo_lower is 0,  then scalar are intialized as  in the original
    code.  If   apollo_lower  is   different  from  0,   scalaras  are
    initialized   from  values   provided  as   a  parameter   to  the
    instrumentation function. In order to have these parameters on the
    next invokation,  the values of  these scalars are saved  when the
    outer most loop exits. They  are saved in the structure %phi_state
    which  pointer is  provided  as parameter  of the  instrumentation
    function.

18. *ApolloInstrumentOnCondition*      -     Instrumentation     -
    ApolloPassOnSkeleton  "skeleton_1" -  This pass  inserts the  code
    that generates a  boolean value saying whether or  not the dynamic
    instrumentation code should  be invoked.  For the  outer most loop
    the  condition is  %apollo_outer_instrument_bound  != 0  AND VI  <
    %apollo_lower +  %apollo_outer_instrument_bound.  The  first check
    is to ensure that we are  not running the sequential version while
    the  second  one is  to  ensure  that  we  record only  the  first
    %apollo_outer_instrument_bound (16  by default I think).   For the
    nested levels VIN, then the condition  is based on the codition of
    the  level just  above (including  by recursivity  all the  levels
    above) AND  VIN <  %apollo_outer_instrument_bound (no need  to add
    apollo_lower because  nested levels  always restart at  0).  These
    conditions  will be  used  later  to invoke  apollo_register_event
    function with dynamic  type at each level of the  loop nest having
    at list one memory access or one scalar or one bounds.

19. *InstrumentFunctionCall*  - Instrumentation  - ApolloPassOnSkeleton
    "skeleton_1" -  This pass inserts a  call to apollo_register_event
    with type FunCall (6) for every function call performed inside the
    loop  nest.  This  will be  simply used  by the  runtime to  check
    wether or  not we can  optimize a loop  nest. In other  words, the
    runtime will  optimize a loop  nest only  if it has  ZERO function
    calls. Function calls  only account if they are made  while we are
    sampling, I am not sure to udnerstand why it is like that.
    
20. *InstrumentIterrationCount*      -     Instrumentation      -
    ApolloPassOnSkeleton "skeleton_1"  - This  pass inserts a  call to
    apollo_register_event with type BoundDyn (2)  for all loops in the
    nest  having dynamic  bounds  and excluding  the  outer most  loop
    because it is executed by chunks.

21. *ApolloInstrumentMemAccess*      -     Instrumentation      -
    ApolloPassOnSkeleton "skeleton_1"  - This  pass inserts a  call to
    apollo_register_event for each memory access of the loop nest with
    the type MemDyn.   The register event call  parameters include the
    access  id, the  target  address  of the  access,  the loop  depth
    (number of  following params)  and$ the value  of all  the virtual
    iterators  containing the  statement. Static  memory accesses  are
    also recorded.

22.  *ApolloInstrumentPhi*  - Instrumentation  -  ApolloPassOnSkeleton
    "skeleton_1" -  This pass inserts a  call to apollo_register_event
    for  each scalar  (which  is no  more  a phi  but  an alloca  with
    metadata  ApolloPhiId) with  the type  ScaDyn. This  only concerns
    dynamic scalars.   The register event call  parameters include the
    scalar id (ApolloPhiId),  the value or address of  the scalar, the
    loop depth (number of following params)  and$ the value of all the
    virtual iterators containing the scalar.
    
*** Skeleton 2 - Basic Scalar Prediction

23.  *BasicScalarPredict* -  ApolloPass -  This pass  creates a  a new
    skeleton_2 function  that computes the  values of all  the scalars
    belonging  only  to  the  outermost loop.   For  a  given  virtual
    iterator  0  value.   To  that   end  it  declares  and  uses  the
    @get_coefficient(i8*, i64,  i64, i64).  The first  argument is the
    prediction  model  pointer.   The  second   is  the  type  of  the
    prediction   among   0==MemPrediction,   1==ScalarPrediction   and
    2==BoundPrediction.   The third  argument is  the id  of the  meme
    access or scalar or bound to  predict.  The fourth argument is the
    coefficient index we want (in ax*VI0 + bx*VI1 + c: a, b and c have
    indexes 0,1 and 2).  Because we are at the first level of the loop
    nest, we only predict according to  VI0, meaning that we only have
    two  calls to  the get_coefficient  method for  each scalar  to be
    predicted. Then the scalar is  predicted by multiplying VI0 by the
    first coeff and  adding the second coeff. This  pass also declares
    the  memory_runtime_verify(i64,  i8*,  i32,  ...)  function.  This
    skeleton is used  by the runtime to predict  outermost loop scalar
    values before calling the optimized version of the code with these
    predicted scalars as parameter.

*** Skeleton 3 - Code Bones

24. *ApolloCloneLoopFunction* - Code Bones - ApolloPassOnSkeleton "" -
    From "" to "skeleton_3" - This passe simply clones apollo_loop_IDl
    functions with  a new name being  apollo_loop_IDl_skeleton_3. This
    function will be  then modified by the following  passes to insert
    profiling code  and save the  results somewhere accessible  by the
    runtime.

25. *ApolloSimpleLoopLatch* - Code Bones -  ApolloPassOnSkeleton
    "skeleton_3" - TODO

26. *StoreBones* TODO

27. *MemVerifNones* TODO

28. *BoundVerifBones* TODO 

29. *ScalarVerifBones* TODO

*** Final passes

30. *JitExportFct*  - Final  - ModulePass  - This  pass creates  a new
    module containing all the  apollo_skeleton_* functions and all the
    apollo_bone.nest_* functions.   The binary IR form  of this module
    is then written in a global variable of type string.

31. *SwitchingCall* -  Final -  ApolloPass  - This  pass declares  the
    apollo_runtime_hook function as external  and replace all calls to
    apollo_loop_IDl function by a call to the runtime hook.  This pass
    also  generate  the global  skeleton  array  variable. Finally  it
    deletes the apollo_loop_IDl functions.

32. *ApolloGlobalCombine* -  Final -  ModulePass -  This pass  merges
    globals having the same initializer. TODO
    
