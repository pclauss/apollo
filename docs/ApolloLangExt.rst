=========================
Apollo Language Extension
=========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Introduction
============

This document describes the Apollo extension to C/C++ and give and overview
of its implementation in Clang..

Language extension
==================

The Apollo extension is a pragma of the form:

  #pragma apollo <*keyword*>
  {
      <*statement*>
  }

where *keyword* is ``dcop``
and   *statement* is any C/C++ statement.

If *statement* has one or more loops then Apollo will try to optimize all
top-level loops (a top-level loop is one that is not contained in another
loop).

If *statement* has no loops then Apollo does not optimize the code.

Output
------

A statement marked with the ``apollo`` pragma is lowered to LLVM IR
instructions annotated with `metadata
<http://llvm.org/docs/LangRef.html#metadata>`_.  There is one metadata
node, ``apollo.pragma`` for each generated instruction. The operands
to the node are metadata strings identifying the *keyword* used in the
Apollo pragma.

That is, the *statement* in::

  #pragma apollo dcop *statement*

is lowered to LLVM instruction each of which is annotated with
metadata of the form::

  "apollo.pragma@ ! *n* 

where number *n* is::

  ! *n* = {! "dcop" }

Implementation
==============

The Apollo language extension is implemented in Clang by modifying the
LLVM ``IRBuilder`` and the Clang ``ASTContext`` class to record Apollo
metadata. When a ``#pragma apollo`` statement is parsed, the Apollo
keywords are added to the ``ASTContext``. This is used to setup an
``IRBuilder`` to add metadata nodes to the LLVM code generated for an
Apollo statement. The metadata and keywords are removed from the
``IRBuilder`` and ``ASTContext`` once the LLVM code has been
generated.

The ``IRBuilder`` class is part of LLVM and an defined in
``llvm/include/llvm/IR/IRBuilder.h``. The Apollo modifications are to
support storing and removing metadata and to add any stored metadata
to instructions inserted into the ``IRBuilder``.

Parsing of a ``#pragma apollo`` construct is by extending the existing
Clang pragma parsing in ``clang/lib/Parse``. A parse node
``PragmaApollo`` is defined as an instance of the Clang
``PragmaHandler`` class and added to the Clang parser. The
``PragmaApollo`` class parses the ``#pragma apollo`` construct and
calls ``Sema::ActOnPragma`` to add the keyword information to the
``ASTContext``.

Code generation for the Apollo construct uses the Clang code
generation for compound statements, modified to support Apollo. These
are implemented in ``clang/lib/CodeGen``.  First, the function
``CodeGenFunction::EmitCompoundStmt`` uses the keywords recorded in
the ``ASTContext`` to construct metadata nodes. These are stored in
the ``IRBuilder`. 

Control then passes to
``CodeGenFunction::EmitCompoundStmtWithoutScope`` which generated the
LLVM instructions for the Apollo statement. As the LLVM instructions
are generated, the ``IRBuilder`` adds the Apollo metadata. Once all
LLVM instructions have been emitted, the Apollo metadata is removed
from the ``IRBuilder``.

