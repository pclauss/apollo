//===--- JitExportFct.cpp -------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "Transformations/FinalPasses/JitExportFct.h"
#include "Utils/Utils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/IRBuilder.h"
#include "llvm/Pass.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/Bitcode/BitcodeReader.h"
#include "llvm/Bitcode/BitcodeWriter.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Regex.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IR/LegacyPassManager.h"

/// IPO.h declares the createGVExtractionPass which needs a modification to LLVM
/// to make sure it is properly linked into the clang binary.
/// See externals/llvm/include/llvm/LinkAllPasses.h.
#include "llvm/Transforms/IPO.h"

#include <cstring>
#include <iostream>
#include <string>

using namespace llvm;
using namespace apollo;

namespace {

  /// \fn std::vector <Function*> collect_fct_name (Module* m)
  /// \brief Function to collect all functions created by apollo.
  /// \param m The original module.
  /// \return A vector of containing all the functions.
  std::vector<Function *> collectFunctionsName(Module *M) {
    std::vector<Function *> VecFunction;
    for (Function &fit : *M) {
      const std::string FunctionctName = fit.getName();
      Regex RegExp("^apollo_loop_[0-9]+_skeleton_[0-9]+$");
      if (RegExp.match(FunctionctName)) {
	const unsigned Id = apollo::function::getSkeletonId(&fit);
	if (skeleton::hasJit(Id))
	  VecFunction.push_back(&fit);
      }
    }
    return VecFunction;
  }

  std::vector<GlobalValue *> collectGlobals(Module *ModClone,
					    std::set<Function *> &Functions) {
    std::vector<GlobalValue *> GVs;
    GVs.insert(GVs.end(), Functions.begin(), Functions.end());
    size_t GVsOldSize = 0;
    do {
      GVsOldSize = GVs.size();
      // collect referenced global variables
      for (GlobalVariable &gv : ModClone->globals()) {
	const bool AlreadyConsidered = std::find(GVs.begin(), GVs.end(),
						 &gv) != GVs.end();
	if (!AlreadyConsidered) {
	  bool IsUsed = false;
	  for (User *user : gv.users()) {
	    Instruction *UserInst = dyn_cast<Instruction>(user);
	    IsUsed =
	      IsUsed
	      || (UserInst
		  && Functions.count(UserInst->getParent()->getParent()));
	  }
	  if (IsUsed)
	    GVs.push_back(&gv);
	}
      }
      // collect referenced functions
      for (Function &F : *ModClone) {
	const bool AlreadyConsidered = std::find(GVs.begin(), GVs.end(), &F)
	  != GVs.end();
	if (!AlreadyConsidered) {
	  bool IsUsed = false;
	  for (User *user : F.users()) {
	    Instruction *UserInst = dyn_cast<Instruction>(user);
	    IsUsed =
	      IsUsed
	      || (UserInst
		  && Functions.count(
				     UserInst->getParent()->getParent()));
	  }
	  if (IsUsed)
	    GVs.push_back(&F);
	}
      }
    } while (GVsOldSize != GVs.size());
    return GVs;
  }

  bool validToExtract(std::vector<GlobalValue *> &GVs,
		      std::set<Function *> &Functions) {

    for (GlobalValue *gv : GVs) {
      // we can't extract this module, because we can't import that global.
      Function *GvAsFun = dyn_cast<Function>(gv);
      const bool IsInFun = ((GvAsFun != 0) && Functions.count(GvAsFun));
      const bool IsPrivate =
	(gv->getLinkage() == GlobalValue::PrivateLinkage
	 || gv->getLinkage() == GlobalValue::InternalLinkage);
      if (!IsInFun && IsPrivate) {
	errs() << "Jit warning: cannot extract function.\n";
	errs() << "\tglobal value " << gv->getName()
	       << " has private or internal linkage.\n";
	return false;
      }
    }
    return true;
  }

/// \brief Function to extract functions and their dependencies to a new module.
/// \param M The original module.
/// \param The functions we want to extract.
/// \return A new module with only the extracted functions and their
/// dependencies.

  std::unique_ptr<Module>
  extractFunctions(Module *M, const std::set<Function *> &Functions) {
    DEBUG_WITH_TYPE("apollo-jit-extract-functions", {
      dbgs() << __FUNCTION__ << " - source module:\n";
      M->print(dbgs(), nullptr, false, true);
    });

    std::unique_ptr<Module> ModClone(std::move(CloneModule(M)));
    std::set<Function *> FunctionsClones;

    for (Function *F : Functions) {
      FunctionsClones.insert(ModClone->getFunction(F->getName()));
    }
    std::vector<GlobalValue *> GVs =
      std::move(collectGlobals(ModClone.get(), FunctionsClones));

    DEBUG_WITH_TYPE("apollo-jit-extract-functions", {
	dbgs() << __FUNCTION__ << " - globals to extract:\n";

	dbgs() << "[";
	for(const auto& gv: GVs) {
	  gv->print(dbgs(), true);
	}
	dbgs() << "]\n";
      });

    if (!validToExtract(GVs, FunctionsClones))
      return nullptr;

    llvm::legacy::PassManager Passes;
    Passes.add(createGVExtractionPass(GVs));
    Passes.add(createGlobalDCEPass());           // Delete unreachable globals
    Passes.add(createStripDeadDebugInfoPass());  // Remove dead debug info
    Passes.add(createStripDeadPrototypesPass()); // Remove dead func decls
    Passes.run(*ModClone);

    for (GlobalValue *gv : GVs) {
      Function *GvAsFun = dyn_cast<Function>(gv);
      GlobalVariable *GvAsGlobal = dyn_cast<GlobalVariable>(gv);
      const bool FunToExtract = GvAsFun && FunctionsClones.count(GvAsFun);
      // if function make only a declaration, if global drop the initializer.
      if (!FunToExtract) {
	if (GvAsFun) {
	  // Delete the function body and set the
	  // declaration to external linkage
	  GvAsFun->deleteBody();
	} else if (GvAsGlobal) {
	  GvAsGlobal->setLinkage(GlobalValue::ExternalLinkage);
	  GvAsGlobal->setInitializer(nullptr);
	  GvAsGlobal->setAlignment(0);
	} else
	  assert(false && "case not handled");
      }
    }

    DEBUG_WITH_TYPE("apollo-jit-extract-functions", {
	dbgs() << __FUNCTION__ << " - extracted module:\n";
	ModClone->print(dbgs(), nullptr, false, true);
      });
    return ModClone;
  }

  /// \brief Write the module m to a global variable named FctionName.
  /// \param M The original module.
  /// \param ModToExtract The module we want to register.
  /// \param FunctionName The name of the new global variable.
  void writeModuleToGivString(Module &M, Module *ModToExtract,
			      const std::string &FunctionName) {
    std::string Str = "";
    raw_string_ostream OS(Str);
    WriteBitcodeToFile(ModToExtract, OS);
    OS.flush();
    int Id = 0;
    int Version = 0;
    sscanf(FunctionName.c_str(), "apollo_loop_%d_skeleton_%d", &Id, &Version);
    const std::string BitcodeStringName =
      utils::concat("apollo_loop_", Id, "_jit_", Version);

    DEBUG_WITH_TYPE("apollo-jit-export", {
	dbgs() << __FUNCTION__ << " - \n"
	       << "storing bitcode for function "
	       << FunctionName << "\n"
	       << "     as Global Value " << BitcodeStringName << "\n";
      });

    GlobalVariable *BitcodeString =
      new GlobalVariable
      (M,
       ArrayType::get(IntegerType::get(ModToExtract->getContext(), 8),
		      Str.size() + 1),
       true,
       GlobalValue::LinkageTypes::PrivateLinkage,
       0,
       BitcodeStringName);

    BitcodeString
      ->setInitializer(ConstantDataArray
		       ::getString(ModToExtract->getContext(),
				   OS.str(),
				   true));
  }

  std::map<int, std::set<Function *>> collectBones(Module *M) {
    std::map<int, std::set<Function *>> NestBones;
    std::vector<std::pair<int, Function *>> AllNestBones;
    for (Function &F : *M) {
      Regex RegExp("^apollo_bone.nest_[0-9]+.*");
      const std::string FunctionName = F.getName();
      if (RegExp.match(FunctionName)) {
	int NestId = -1;
	sscanf(FunctionName.c_str(), "apollo_bone.nest_%d", &NestId);
	std::pair<int, Function *> p(NestId, &F);
	AllNestBones.push_back(p);
	NestBones[NestId] = std::set<Function *>();
      }
    }
    for (auto &p : AllNestBones) {
      NestBones[p.first].insert(p.second);
    }
    return NestBones;
  }

} // end anonymous namespace

namespace apollo {

  JitExportFct::JitExportFct() :
    ModulePass(ID) {
  }

  void JitExportFct::getAnalysisUsage(AnalysisUsage &AU) const {
  }

  bool JitExportFct::runOnModule(Module &M) {

    DEBUG_WITH_TYPE("apollo-jit-export", {
	dbgs() << __FILE__ << ":" << __FUNCTION__
	       << " - source module:\n";
	M.print(dbgs(), nullptr, false, true);
	dbgs() << "--------------------\n";
      });

    DEBUG_WITH_TYPE("apollo-pass-0",
		    dbgs() << "Running pass on module: JitExportFct\n");

    std::vector<Function *> Functions = std::move(collectFunctionsName(&M));
    DEBUG_WITH_TYPE("apollo-jit-export", {
	dbgs() << __FUNCTION__
	       << " - functions to extract:\n";
	dbgs() << "[";
	bool comma = false;
	for(const auto& F: Functions) {
	  dbgs() << F->getName() << (comma ? "," : "");
	  comma = true;
	}
	dbgs() << "]\n";
      });

    for (Function *F : Functions) {
      std::unique_ptr<Module>
	ModToExtract(std::move(extractFunctions(&M, { F })));

      if (ModToExtract) {
	writeModuleToGivString(M, ModToExtract.get(), F->getName());

	DEBUG_WITH_TYPE("apollo-jit-export", {
	    dbgs() << __FUNCTION__
		   << " - module for "
		   << F->getName() << ":\n";
	    ModToExtract.get()->print(dbgs(), nullptr, false, true);
	    dbgs() << "----------------------------------------\n";
	  });
      }
    }

    std::map<int, std::set<Function *>> Bones = std::move(collectBones(&M));
    DEBUG_WITH_TYPE("apollo-jit-export", {
	dbgs() << __FUNCTION__
	       << " - code-bones to extract:\n";
	dbgs() << "[";
	bool comma = false;
	for(const auto& P: Bones) {
	  dbgs() << P.first << (comma ? "," : "");
	  comma = true;
	}
	dbgs() << "]\n";
      });

    for (auto &pair : Bones) {
      std::unique_ptr<Module>
	ModToExtract(std::move(extractFunctions(&M, pair.second)));

      if (ModToExtract) {
	std::string name =
	  utils::concat("apollo_loop_", pair.first, "_skeleton_",
			skeleton::code_bones::id);
	writeModuleToGivString(M, ModToExtract.get(), name);

	DEBUG_WITH_TYPE("apollo-jit-export", {
	    dbgs() << ":" << __FUNCTION__
		   << " - module for " << name << ":\n";
	    ModToExtract.get()->print(dbgs(), nullptr, false, true);
	    dbgs() << "--------------------\n";
	  });

      }
    }
    bool modified = Functions.size() || Bones.size();

    DEBUG_WITH_TYPE("apollo-jit-export", {
	dbgs() << ":" << __FUNCTION__
	       << " - final module:\n";
	M.print(dbgs(), nullptr, false, true);
	dbgs() << "----------------------------------------\n";
      });

    DEBUG_WITH_TYPE("apollo-pass-1",
		    dbgs() << " ==> modified = "
		    << (modified ? "true" : "false")
		    << "\n");
    return modified;
  }

  char JitExportFct::ID = 0;
  DefinePassCreator(JitExportFct)

} // and namespace apollo

namespace llvm {
  class PassRegistry;
  DeclarePassInitializator(JitExportFct);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(JitExportFct, "JitExportFct", "JitExportFct", false,
		      false)
INITIALIZE_PASS_END(JitExportFct, "JitExportFct", "JitExportFct", false,
		    false)
