//===--- ApolloGlobalCombine.cpp ------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/FinalPasses/ApolloGlobalCombine.h"
#include "Utils/Utils.h"

#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/Regex.h"

using namespace llvm;
using namespace apollo;

namespace {

/// \brief return true if both constant intializer are equal.
bool sameInitializer(const Constant *A, const Constant *B) {
	return A == B;
}

/// \brief return true if the value passed as parameter has name "unnamed_%id"
bool isUnnamed(const Value &V) {
	Regex RegExp("^unnamed_[0-9]+$");
	return RegExp.match(V.getName());
}

} // end anonymous namespace

namespace apollo {

ApolloGlobalCombine::ApolloGlobalCombine() :
		ModulePass(ID) {
}

void ApolloGlobalCombine::getAnalysisUsage(AnalysisUsage &AU) const {
}

bool ApolloGlobalCombine::runOnModule(Module &M) {
	DEBUG_WITH_TYPE("apollo-pass-0",
			dbgs() << "Running pass on module: ApolloGlobalCombine\n");
	Module::global_iterator it = M.global_begin();
	bool modified = false;
	while (it != M.global_end()) {
		bool Merge = false;
		if (isUnnamed(*it)) {
			Module::global_iterator merge_with = it;
			merge_with++;
			while (merge_with != M.global_end()) {
				if (isUnnamed(*merge_with)) {
					Merge = sameInitializer(merge_with->getInitializer(),
							it->getInitializer());
					if (Merge) {
						merge_with->replaceAllUsesWith(&*it);
						merge_with->eraseFromParent();
						modified = true;
						break;
					}
				}
				merge_with++;
			}
		}
		if (Merge)
			it = M.global_begin();
		else
			++it;
	}
	DEBUG_WITH_TYPE("apollo-pass-1",
			dbgs() << " ==> modified = " << (modified ? "true" : "false")
					<< "\n");
	return modified;
}

char ApolloGlobalCombine::ID = 0;
DefinePassCreator(ApolloGlobalCombine);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloGlobalCombine);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloGlobalCombine, "APOLLO_GLOBAL_COMBINE",
		"APOLLO_GLOBAL_COMBINE", false, false)
	INITIALIZE_PASS_END(ApolloGlobalCombine, "APOLLO_GLOBAL_COMBINE",
			"APOLLO_GLOBAL_COMBINE", false, false)
