//===--- ApolloCloneLoopFunction.cpp --------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/Preparation/ApolloCloneLoopFunction.h"
#include "Utils/Utils.h"
#include "SkeletonConfig.h"

#include "llvm/Transforms/Utils/Cloning.h"

#include <iterator>

using namespace llvm;
using namespace apollo;

namespace {

/// \brief Clone a function.
/// \param original a function.
/// \return The clone of original
Function *cloneFunctionAndInstructions(Function *F) {
  Function *Clone = Function::Create(F->getFunctionType(), F->getLinkage(), "",
                                     F->getParent());
  ValueToValueMapTy ValueMap;
  ApolloCloneLoopFunction::mapArguments(F, Clone, ValueMap);
  SmallVector<ReturnInst *, 8> Returns;
  CloneFunctionInto(Clone, F, ValueMap, false, Returns, "", 0x0);
  ApolloCloneLoopFunction::fixInstructions(Clone, ValueMap);
  return Clone;
}

} // end anonymous namespace

namespace apollo {

ApolloCloneLoopFunction::ApolloCloneLoopFunction()
  : ApolloPassOnSkeleton(ID, "", "ApolloCloneLoopFunction") {}

ApolloCloneLoopFunction::ApolloCloneLoopFunction(std::string CloneFrom_,
                                                 std::string CloneTo_)
  : ApolloPassOnSkeleton(ID, CloneFrom_, "ApolloCloneLoopFunction") {
  this->CloneTo = CloneTo_;
  this->CloneFrom = CloneFrom_;
}

bool ApolloCloneLoopFunction::runOnApollo(Function *apolloFunction) {
  Function *Clone = cloneFunctionAndInstructions(apolloFunction);
  const std::string apolloFunctionName = apolloFunction->getName();
  // if cloneFrom is not zero substract an extra character for the "_"
  const std::string apolloLoopName = apolloFunctionName.substr(
      0, apolloFunctionName.size() - CloneFrom.size() - (CloneFrom.size() > 0));
  const std::string CloneName = utils::concat(apolloLoopName, "_", CloneTo);
  Clone->setName(CloneName);
  if (CloneTo == skeleton::sk_name<skeleton::instrumentation>()) {
    Clone->removeFnAttr(Attribute::NoInline);
    Clone->addFnAttr(Attribute::AlwaysInline);
  } else {
    Clone->removeFnAttr(Attribute::AlwaysInline);
    Clone->addFnAttr(Attribute::NoInline);
  }
  return true;
}

void ApolloCloneLoopFunction::mapArguments(Function *Original, Function *Clone,
                                           ValueToValueMapTy &ValueMap) {
  // Clone and map the arguments of the function.
  for (auto arg = Original->arg_begin(), arg_clone = Clone->arg_begin(),
       arg_en = Original->arg_end(); arg != arg_en; ++arg, ++arg_clone) {
    arg_clone->setName(arg->getName());
    ValueMap[&*arg] = &*arg_clone;
  }
}

void ApolloCloneLoopFunction::fixInstructions(Function *Clone,
                                              ValueToValueMapTy &ValueMap) {
  for (BasicBlock &block : *Clone) {
    for (Instruction &instruction : block) {
      Instruction::op_iterator operand, op_end = instruction.op_end();
      for (operand = instruction.op_begin(); operand != op_end; ++operand) {
        Value *OperandValue = *operand;
        if (ValueMap.count(OperandValue)) // if it is mapped
          operand->set(ValueMap[OperandValue]);
      }
    }
  }
}

Function *ApolloCloneLoopFunction::addParameters(Function *OldFunction, 
                           std::vector<std::pair<std::string, Type *>> &NewArgs,
                           Type *RetTy) {
  // Create the new definition of the function.
  const std::string FunctionName = OldFunction->getName();
  FunctionType *OldFunctionType = OldFunction->getFunctionType();

  // Make the return type
  if (RetTy == 0x0)
    RetTy = OldFunctionType->getReturnType();

  // Rename the old function
  OldFunction->setName(FunctionName + "_old_to_delete");

  // The name and types for the new parameter list
  std::vector<std::string> ArgNames;
  std::vector<Type *> ArgTypes;

  // Get the existing paramater names and types
  for (auto arg_it = OldFunction->arg_begin(),
	 end_it = OldFunction->arg_end();
       arg_it != end_it;
       ++arg_it) {
    ArgNames.push_back(arg_it->getName());
  }

  ArgTypes.insert(ArgTypes.end(), OldFunctionType->param_begin(),
                  OldFunctionType->param_end());

  // Add the new parameter names and types
  for (auto& name_ty_pair : NewArgs) {
    ArgNames.push_back(name_ty_pair.first);
    ArgTypes.push_back(name_ty_pair.second);
  }

  // Make the new function type
  FunctionType *NewFunctionType =
      FunctionType::get(RetTy, ArgTypes, OldFunctionType->isVarArg());

  // The new function declaration
  Function *NewFunction =
      Function::Create(NewFunctionType, OldFunction->getLinkage(), FunctionName,
                       OldFunction->getParent());

  // Set the parameter names
  auto arg_it = NewFunction->arg_begin();
  for (auto name_it = ArgNames.begin(), end_it = ArgNames.end();
       name_it != end_it;
       ++name_it, ++arg_it) {
    arg_it->setName(*name_it);
  }
  assert(arg_it == NewFunction->arg_end() && "mismatched parameter lists");

  // Map the new and old parameters
  ValueToValueMapTy ValueMap;
  ApolloCloneLoopFunction::mapArguments(OldFunction, NewFunction, ValueMap);

  // Clone the body of the old function into the new one.
  SmallVector<ReturnInst *, 8> Returns;
  CloneFunctionInto(NewFunction, OldFunction, ValueMap, false, Returns, "",
                    0x0);
  ApolloCloneLoopFunction::fixInstructions(NewFunction, ValueMap);

  // Finally, erase the old function.
  OldFunction->eraseFromParent();

  return NewFunction;
}

char ApolloCloneLoopFunction::ID = 0;
DefinePassCreator_2(ApolloCloneLoopFunction, std::string, std::string);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloCloneLoopFunction);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloCloneLoopFunction, "APOLLO_CLONE",
                      "Apollo function cloning", false, false)
INITIALIZE_PASS_END(ApolloCloneLoopFunction, "APOLLO_CLONE",
                    "Apollo function cloning", false, false)
