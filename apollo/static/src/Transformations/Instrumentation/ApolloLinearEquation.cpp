//===--- ApolloLinearEquation.cpp -----------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "InstrumentationConfig.h"
#include "Transformations/Instrumentation/ApolloLinearEquation.h"
#include "Transformations/Instrumentation/InstrumentationUtils.h"
#include "Utils/Utils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/TypeUtils.h"
#include "Utils/SCEVUtils.h"

#include "llvm/Pass.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"
#include "llvm/Analysis/ScalarEvolutionExpander.h"

#include <list>

using namespace llvm;
using namespace apollo;

#define SCEV_Visit_End() assert(false && "SCEVFold: Unknown SCEV type!");

#define SCEV_Visit(_Type, _Scev)                                               \
  if (isa<_Type>(_Scev)) {                                                     \
    return visit##_Type(cast<_Type>(_Scev));                                   \
  }
#define SCEV_Method_void(_Type, _Scev) void visit##_Type(const _Type *_Scev)
#define SCEV_Method_value(_Type, _Scev) Value *visit##_Type(const _Type *_Scev)

namespace {

/// \brief A class for creating vector of SCEV from SCEVAddRecExpr.
class SCEVFold {
public:
  ScalarEvolution *SE;
  /// \brief The result of SCEVAddRecExpr unfolding.
  std::vector<const SCEV *> Result;
  SCEVFold(ScalarEvolution *PSE, const unsigned int Depth) {
    SE = PSE;
    std::vector<const SCEV *> Temp(
        Depth + 1, SE->getConstant(cast<ConstantInt>(
                       constant::getInt64Constant(SE->getContext(), 0))));
    Result = Temp;
  }

  void visit(const SCEV *Scev) {
    SCEV_Visit(SCEVCouldNotCompute, Scev);
    SCEV_Visit(SCEVAddRecExpr, Scev);
    Result.at(Result.size() - 1) = Scev;
  }

  SCEV_Method_void(SCEVAddRecExpr, Scev) {
    Result.at(Scev->getLoop()->getLoopDepth() - 1) =
        Scev->getStepRecurrence(*SE);
    this->visit(Scev->getStart());
  }

  SCEV_Method_void(SCEVCouldNotCompute, Scev) {
    assert(false && "ERROR! SCEVCouldNotCompute");
  }
};

/// \brief A class for change vector of SCEV to vector of Value.
///        It can inserting instructions if SCEV is "complicated".
///        Also creating cast if it is needed. At runtime we work only
///        in 64bits.

class SCEVFoldValuer {
public:
  ir::ApolloIRBuilder Builder;
  ScalarEvolution *SE;
  Type *ETy;
  /// \brief The result of conversion from vector of SCEV to vector of Value.
  std::vector<Value *> Result;
  SCEVFoldValuer(ScalarEvolution *PSE, std::vector<const SCEV *> &VecScev,
                 BasicBlock *InsertAt)
      : Builder(InsertAt->getTerminator()) {
    SE = PSE;
    LLVMContext &Context = InsertAt->getContext();
    ETy = Type::getInt64Ty(Context);
    std::vector<Value *> Temp(VecScev.size(),
                              constant::getInt64Constant(Context, 0));
    Result = Temp;
    for (unsigned int i = 0; i < VecScev.size(); i++) {
      Value *Coef = this->visit(VecScev[i]);
      Result[i] = createEtyCast(Coef);
    }
  }

  Value *createEtyCast(Value *V) {
    Type *ValType = V->getType();
    if (ValType == ETy)
      return V;
    if (ValType->isIntegerTy())
      return Builder.CreateIntCast(V, ETy, true);
    if (ValType->isPointerTy())
      return Builder.CreatePtrToInt(V, ETy);
    assert(false && "unhandled cast case.");
    return NULL;
  }

  Value *visit(const SCEV *Scev) {
    SCEV_Visit(SCEVAddExpr, Scev);
    SCEV_Visit(SCEVCastExpr, Scev);
    SCEV_Visit(SCEVMulExpr, Scev);
    SCEV_Visit(SCEVSMaxExpr, Scev);
    SCEV_Visit(SCEVUMaxExpr, Scev);
    SCEV_Visit(SCEVUnknown, Scev);
    SCEV_Visit(SCEVConstant, Scev);
    SCEV_Visit(SCEVUDivExpr, Scev);
    SCEV_Visit_End();
    return NULL;
  }

  SCEV_Method_value(SCEVCastExpr, Scev) {
    Value *Operand = this->visit(Scev->getOperand());
    if ((isa<SCEVSignExtendExpr>(Scev) ||
         (SE->isKnownNonNegative(Scev) && isa<SCEVZeroExtendExpr>(Scev))))
      return Builder.CreateSExtOrTrunc(Operand, Scev->getType());
    return Builder.CreateZExtOrTrunc(Operand, Scev->getType());
  }

  SCEV_Method_value(SCEVUnknown, Scev) { return Scev->getValue(); }

  SCEV_Method_value(SCEVConstant, Scev) { return Scev->getValue(); }

  SCEV_Method_value(SCEVMulExpr, Scev) {
    Value *Op1 = createEtyCast(this->visit(Scev->getOperand(0)));
    Value *Op2 = createEtyCast(this->visit(Scev->getOperand(1)));
    Value *Res = Builder.CreateMul(Op1, Op2);
    for (unsigned i = 2; i < Scev->getNumOperands(); i++) {
      Res = Builder.CreateMul(Res,
                              createEtyCast(this->visit(Scev->getOperand(i))));
    }
    return Res;
  }

  SCEV_Method_value(SCEVAddExpr, Scev) {
    Value *Op1 = createEtyCast(this->visit(Scev->getOperand(0)));
    Value *Op2 = createEtyCast(this->visit(Scev->getOperand(1)));
    Value *Res = Builder.CreateAdd(Op1, Op2);
    for (unsigned i = 2; i < Scev->getNumOperands(); i++) {
      Res = Builder.CreateAdd(Res,
                              createEtyCast(this->visit(Scev->getOperand(i)))); 
    }
    return Res;
  }

  SCEV_Method_value(SCEVUDivExpr, Scev) {
    Value *Op1 = createEtyCast(this->visit(Scev->getLHS()));
    Value *Op2 = createEtyCast(this->visit(Scev->getRHS()));
    Value *Res = Builder.CreateUDiv(Op1, Op2);
    return Res;
  }

  SCEV_Method_value(SCEVSMaxExpr, Scev) {
    Value *Op1 = createEtyCast(this->visit(Scev->getOperand(0)));
    Value *Op2 = createEtyCast(this->visit(Scev->getOperand(1)));
    Value *Res = Builder.createMax(Op1, Op2);
    for (unsigned i = 2; i < Scev->getNumOperands(); i++) {
      Value *Op = this->visit(Scev->getOperand(i));
      Res = Builder.createMax(Res, createEtyCast(Op));
    }
    return Res;
  }

  SCEV_Method_value(SCEVUMaxExpr, Scev) {
    Value *Op1 = createEtyCast(this->visit(Scev->getOperand(0)));
    Value *Op2 = createEtyCast(this->visit(Scev->getOperand(1)));
    Value *Res = Builder.createMin(Op1, Op2);
    for (unsigned i = 2; i < Scev->getNumOperands(); i++) {
      Value *Op = this->visit(Scev->getOperand(i));
      Res = Builder.createMin(Res, createEtyCast(Op));
    }
    return Res;
  }
};

/// \brief getAffineStatements collects all the statements whose pointer
///        is affine
/// \param F the function
/// \param SE scalar evolution analysis
/// \param Outermost outermost loop
/// \return vector of statements
std::vector<Instruction *> getAffineStatements(Function *F, ScalarEvolution &SE,
                                               Loop *Outermost) {
  std::vector<Instruction *> InstrCollected;
  for (BasicBlock &bit : *F) {
    for (Instruction &iit : bit) {
      if (apollo::statement::isStatement(&iit)) {
        Value *PtrOp = apollo::statement::getPointerOperand(&iit);
        const SCEV *Scev = SE.getSCEV(PtrOp);
        if (apollo::SCEVUtils::isAffine(&SE, Scev, Outermost))
          InstrCollected.push_back(&iit);
      }
    }
  }
  return InstrCollected;
}

/// \brief collectBasicScalars collects the phi nodes that are linear
/// \param L the outermost loop.
/// \param SE scalar evolution analysis
/// \return a vector with the basic scalars
std::vector<PHINode *> collectBasicScalars(Loop *L, ScalarEvolution *SE) {
  std::vector<PHINode *> BasicScalars;
  for (BasicBlock::iterator inst = L->getHeader()->begin(); isa<PHINode>(*inst);
       ++inst) {
    PHINode *Scalar = cast<PHINode>(&*inst);
    if (!Scalar->getType()->isFloatingPointTy()) {
      const SCEV *Scev = SE->getSCEV(Scalar);
      if (apollo::SCEVUtils::isAffine(SE, Scev, loop::getOutermostLoop(L)))
        BasicScalars.push_back(Scalar);
    }
  }
  for (Loop *subloop : *L) {
    std::vector<PHINode *> SubloopBasicScalars =
        collectBasicScalars(subloop, SE);
    BasicScalars.insert(BasicScalars.end(), SubloopBasicScalars.begin(),
                        SubloopBasicScalars.end());
  }
  return BasicScalars;
}

/// \brief Compute linear equation for loop bound.
/// \param SE The scalar evolution analysis.
/// \param Outermost The loop nest considered.
/// \param L The loop currently checked.
/// \return
std::list<std::pair<unsigned int, const SCEV *>>
computeLoopsBoundScevRecursively(ScalarEvolution *SE, Loop *Outermost,
                                 Loop *L) {
  std::list<std::pair<unsigned int, const SCEV *>> Res;
  if (SE->hasLoopInvariantBackedgeTakenCount(L)) {
    const SCEV *Scev = SE->getBackedgeTakenCount(L);
    if (apollo::SCEVUtils::isAffine(SE, Scev, Outermost))
      Res.push_back(std::make_pair(loop::getLoopId(L), Scev));
  }
  for (Loop *subloop : *L) {
    Res.splice(Res.end(),
               computeLoopsBoundScevRecursively(SE, Outermost, subloop));
  }
  return Res;
}

/// \brief Create all register function call for basicscalars where we can
///        compute linear equation statically.
/// \param F The function considered.
/// \param SE The scalar evolution analysis.
/// \param L The loop considered. It should be the outermost loop.
/// \param LI The LoopInfo considered.
/// \\param InsertAt block in which we are going to insert the code.
void createCoefsForBasicScalars(Function *F, ScalarEvolution *SE,
                                Loop *Outermost, LoopInfo *LI,
                                BasicBlock *InsertAt) {

  LLVMContext &Context = F->getContext();
  Function *RegisterEventFunction = getApolloRegisterEvent(F->getParent());
  std::vector<PHINode *> LinearScalars =
      std::move(collectBasicScalars(Outermost, SE));
  for (PHINode *scalar : LinearScalars) {
    Value *ScalarId = metadata::getMetadataOperand(scalar, mdkind::ApolloPhiId);
    const SCEV *Scev = SE->getSCEV(scalar);
    SCEVFold SF(SE, LI->getLoopFor(scalar->getParent())->getLoopDepth());
    SF.visit(Scev);
    SCEVFoldValuer SV(SE, SF.Result, InsertAt);
    std::vector<Value *> RRes = SV.Result;
    if (RRes.size() > 0) {
      Value *Num = constant::getInt64Constant(Context, RRes.size());
      Value *Zero = constant::getInt64Constant(Context, 0);
      Value *EventId = constant::getInt64Constant(Context, EventType::ScaSta);
      Value *InstrumentationResult =
          function::getArgumentByName(F, "instrumentation_result");
      std::vector<Value *> LineqParams = {InstrumentationResult, EventId,
                                          ScalarId, Zero, Num};
      LineqParams.insert(LineqParams.end(), RRes.begin(), RRes.end());
      CallInst::Create(RegisterEventFunction, LineqParams, "",
                       InsertAt->getTerminator());
      metadata::attachMetadata(scalar, mdkind::LinEq, {LineqParams[1]});
    }
  }
}

/// \brief Create all register function call for loop bounds where we can
///        compute linear equation statically.
/// \param F The function considered.
/// \param SE The scalar evolution analysis.
/// \param L The loop considered. It should be the outermost loop.
/// \param LI The LoopInfo considered.
/// \param InsertAt block in which we are going to insert the registration
void createCoefsForLoopBounds(Function *F, ScalarEvolution *SE, Loop *Outermost,
                             BasicBlock *InsertAt) {
  Function *RegisterEventFun = getApolloRegisterEvent(F->getParent());
  LLVMContext &Context = F->getContext();
  std::list<std::pair<unsigned, const SCEV *>> ListCoefs =
      std::move(computeLoopsBoundScevRecursively(SE, Outermost, Outermost));
  for (auto &lid_scev_pair : ListCoefs) {
    const unsigned LoopId = lid_scev_pair.first;
    const SCEV *Scev = lid_scev_pair.second;
    Loop *L = loop::getLoopWithId(Outermost, LoopId);
    const unsigned depth = L->getLoopDepth();
    SCEVFold SF(SE, depth - 1);
    SF.visit(Scev);
    SCEVFoldValuer SV(SE, SF.Result, InsertAt);
    std::vector<Value *> RRes = SV.Result;
    if (RRes.size() > 0) {
      // increase by one the constant coeficient
      RRes[RRes.size() - 1] = BinaryOperator::CreateAdd(
          RRes[RRes.size() - 1], constant::one64(Context), "",
          InsertAt->getTerminator());
      Value *LoopIdVal = constant::getInt64Constant(Context, LoopId);
      Value *Num = constant::getInt64Constant(Context, RRes.size());
      Value *EventId = constant::getInt64Constant(Context, EventType::BoundSta);
      Value *Zero = constant::getInt64Constant(Context, 0);
      Value *InstrumentationResult =
          function::getArgumentByName(F, "instrumentation_result");
      std::vector<Value *> LoopBoundParams = {InstrumentationResult, EventId,
                                              LoopIdVal, Zero, Num};
      LoopBoundParams.insert(LoopBoundParams.end(), RRes.begin(), RRes.end());
      CallInst::Create(RegisterEventFun, LoopBoundParams, "",
                       InsertAt->getTerminator());
      CastInst *FakeCast = CastInst::CreateSExtOrBitCast(
          LoopIdVal, Type::getInt64Ty(F->getContext()),
          "fake_lineq_for_iteration_count_pass",
          F->getEntryBlock().getTerminator());
      metadata::attachMetadata(FakeCast, mdkind::LinEq, {LoopIdVal});
    }
  }
}

/// \brief Create all register function call for memory access where we can
///        compute linear equation statically.
/// \param F The function considered.
/// \param SE The scalar evolution analysis.
/// \param L The loop considered. It should be the outermost loop.
/// \param LI The LoopInfo considered.
/// \param InsertAt block in which the registration is going to be inserted.
void createCoefsForMemAccess(Function *F, ScalarEvolution *SE, Loop *Outermost,
                             LoopInfo *LI, BasicBlock *InsertAt) {

  LLVMContext &Context = F->getContext();
  Function *RegisterEventFunction = getApolloRegisterEvent(F->getParent());
  std::vector<Instruction *> InstrCollected =
      std::move(getAffineStatements(F, *SE, Outermost));
  for (auto &iit : InstrCollected) {
    Value *InstDo = apollo::statement::getPointerOperand(iit);
    Value *VInstrId =
        metadata::getMetadataOperand(iit, mdkind::ApolloStatementId, 0);
    const SCEV *Scev = SE->getSCEV(InstDo);
    SCEVFold SF(SE, LI->getLoopFor(iit->getParent())->getLoopDepth());
    SF.visit(Scev);
    SCEVFoldValuer SV(SE, SF.Result, InsertAt);

    std::vector<Value *> RRes = SV.Result;
    if (RRes.size() > 0) {
      Value *EventId = constant::getInt64Constant(Context, EventType::MemSta);
      Value *NumCoefs = constant::getInt64Constant(Context, RRes.size());
      Value *Zero = constant::getInt64Constant(Context, 0);
      Value *InstrumentationResult =
          function::getArgumentByName(F, "instrumentation_result");

      // All arguments must be present
      assert(InstrumentationResult && "Require the instrumentation result");
      assert(VInstrId && "Require the Apollo statment Id");
      assert(Zero && "Require a zero value");
      assert(NumCoefs && "Require the number of coefficients");
      std::vector<Value *> MemLineqParams =
	{ InstrumentationResult, EventId, VInstrId, Zero, NumCoefs };
      MemLineqParams.insert(MemLineqParams.end(), RRes.begin(), RRes.end());
      CallInst::Create(RegisterEventFunction, MemLineqParams, "",
		       InsertAt->getTerminator());
      metadata::attachMetadata(iit, mdkind::LinEq, {MemLineqParams[1]});
    }
  }
}

} // end anonymous namespace

namespace apollo {

/// \brief Check if we have statically computed a linear equation for
///        loop bounds for loop id.
/// \param F The function considered.
/// \param LoopId The loop id.
/// \return true if loop bound of the loop have linear eqution statically
///         computed.
bool ApolloInstrumentLinearEquation::hasLineq(Function *F, unsigned LoopId) {
  for (BasicBlock &block : *F) {
    for (Instruction &inst : block) {
      CastInst *FunCast = dyn_cast<CastInst>(&inst);
      if (FunCast != nullptr) {
        if (metadata::hasMetadataKind(&inst, mdkind::LinEq)) {
          const std::string StrCmp("fake_lineq_for_iteration_count_pass");
          const std::size_t Found = FunCast->getName().str().find(StrCmp);
          if (Found != std::string::npos) {
            ConstantInt *LoopConstId = cast<ConstantInt>(
                metadata::getMetadataOperand(&inst, mdkind::LinEq, 0));
            if (LoopConstId->getValue().getSExtValue() == LoopId)
              return true;
          }
        }
      }
    }
  }
  return false;
}

ApolloInstrumentLinearEquation::ApolloInstrumentLinearEquation()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::instrumentation>(), "ApolloInstrumentLinearEquation") {
}

void ApolloInstrumentLinearEquation::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
  AU.addRequired<ScalarEvolutionWrapperPass>();
}

bool ApolloInstrumentLinearEquation::runOnApollo(Function *F) {
  LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
  ScalarEvolutionWrapperPass &SeWrapper =
      getAnalysis<ScalarEvolutionWrapperPass>(*F);
  ScalarEvolution &SE = SeWrapper.getSE();
  Loop *L = apollo::function::getOutermostLoopInFunction(F, &LI);
  assert(L && "ERROR! Loop not found in ApolloLinearEq");

  // create a basic block in which we are going to register the static linear
  // functions. And branch to it if we are not running sequential version
  // i.e. if apollo_outer_instrument_bound != 0
  BasicBlock *Preheader = L->getLoopPreheader();
  BasicBlock *NewPreheader = Preheader->splitBasicBlock(
      Preheader->getTerminator(), Preheader->getName() + ".cont");
  BasicBlock *RegistrationBlock =
      BasicBlock::Create(F->getContext(), "register_static_lf", F);
  Preheader->getTerminator()->eraseFromParent();
  Value *ShouldInstrument = new ICmpInst(
      *Preheader, ICmpInst::ICMP_NE,
      function::getArgumentByName(F, "apollo_outer_instrument_bound"),
      constant::zero64(F->getContext()), "register_lf_cond");
  BranchInst::Create(RegistrationBlock, NewPreheader, ShouldInstrument,
                     Preheader);
  BranchInst::Create(NewPreheader, RegistrationBlock);

  // Register static functions' coeffs
  createCoefsForMemAccess(F, &SE, L, &LI, RegistrationBlock);
  createCoefsForBasicScalars(F, &SE, L, &LI, RegistrationBlock);
  createCoefsForLoopBounds(F, &SE, L, RegistrationBlock);

  SeWrapper.releaseMemory();
  return true;
}

char ApolloInstrumentLinearEquation::ID = 0;
DefinePassCreator(ApolloInstrumentLinearEquation);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloInstrumentLinearEquation);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloInstrumentLinearEquation,
                      "APOLLO_INSTRUMENT_LINEAR_EQUATION",
                      "APOLLO_INSTRUMENT_LINEAR_EQUATION", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_DEPENDENCY(ScalarEvolutionWrapperPass)
INITIALIZE_PASS_END(ApolloInstrumentLinearEquation,
                    "APOLLO_INSTRUMENT_LINEAR_EQUATION",
                    "APOLLO_INSTRUMENT_LINEAR_EQUATION", false, false)
