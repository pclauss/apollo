//===--- InstrumentationFunctionCall.cpp ----------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "InstrumentationConfig.h"
#include "Transformations/Instrumentation/InstrumentFunctionCall.h"
#include "Transformations/Instrumentation/ApolloOnCondition.h"
#include "Transformations/Instrumentation/InstrumentationUtils.h"
#include "Utils/FunctionUtils.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"

using namespace apollo;
using namespace llvm;

namespace apollo {

InstrumentFunctionCall::InstrumentFunctionCall()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::instrumentation>(), "InstrumentFunctionCall") {
}

void InstrumentFunctionCall::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool InstrumentFunctionCall::runOnLoopLI(Loop *L, LoopInfo *LI) {
  std::vector<CallInst *> CallPoints;
  for (BasicBlock *block : L->getBlocks()) {
    for (Instruction &inst : *block) {
      CallInst *CI = dyn_cast<CallInst>(&inst);
      if (CI) {
        const bool IsSafe = CI->doesNotAccessMemory() && CI->doesNotThrow();
        Function *Called = CI->getCalledFunction();
        const bool IsExc = Called && function::allowedInNest(Called);
        if (!IsSafe && !IsExc)
          CallPoints.push_back(CI);
      }
    }
  }

  if (CallPoints.empty())
    return false;
  Module *M = loop::getLoopParentModule(L);
  Function *RegFun = getApolloRegisterEvent(M);
  for (CallInst *call : CallPoints) {
    Loop *ParentLoop = LI->getLoopFor(call->getParent());
    Value *InstrumentationCondition =
        ApolloInstrumentOnCondition::getInstrumentationCondition(ParentLoop);
    BasicBlock *Parent = call->getParent();
    BasicBlock *Cont =
        Parent->splitBasicBlock(call, Parent->getName() + ".cont");
    BasicBlock *RegBlock =
        BasicBlock::Create(Parent->getContext(), "register_function_call",
                           Parent->getParent(), Cont);
    Type *i64Ty = Type::getInt64Ty(call->getContext());
    Constant *EventId = ConstantInt::get(i64Ty, EventType::FuncCall);
    Constant *Zero = ConstantInt::get(i64Ty, 0);
    Value *InstrumentationResult = function::getArgumentByName(
        Parent->getParent(), "instrumentation_result");
    std::vector<Value *> Args = {InstrumentationResult, EventId, Zero, Zero,
                                 Zero};
    CallInst::Create(RegFun, Args, "", RegBlock);
    BranchInst::Create(Cont, RegBlock);
    Parent->getTerminator()->eraseFromParent();
    BranchInst::Create(RegBlock, Cont, InstrumentationCondition, Parent);
    ParentLoop->addBasicBlockToLoop(Cont, *LI);
    ParentLoop->addBasicBlockToLoop(RegBlock, *LI);
  }
  return true;
}

char InstrumentFunctionCall::ID = 0;
DefinePassCreator(InstrumentFunctionCall);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(InstrumentFunctionCall);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(InstrumentFunctionCall, "APOLLO_INSTRUMENT_FUNCTION_CALL",
                      "APOLLO_INSTRUMENT_FUNCTION_CALL", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(InstrumentFunctionCall, "APOLLO_INSTRUMENT_FUNCTION_CALL",
                    "APOLLO_INSTRUMENT_FUNCTION_CALL", false, false)
