//===--- Stores.cpp -------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "Transformations/CodeBones/Stores.h"
#include "Transformations/Parallel/BasicScalarPredict.h"
#include "Utils/Utils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/Debug.h"

using namespace apollo;
using namespace llvm;

// Enable debugging information for code-bones-extraction with
// '-debug' or '-debug=codebones-stores'
#define DEBUG_TYPE "codebones-stores"

namespace {

std::vector<StoreInst *> collectStoreStatements(Function *F) {
  std::vector<StoreInst *> Stores;
  for (BasicBlock &block : *F) {
    for (Instruction &inst : block) {
      if (isa<StoreInst>(inst) && statement::isStatement(&inst))
        Stores.push_back(cast<StoreInst>(&inst));
    }
  }
  return Stores;
}

std::vector<std::set<Instruction *>>
collectStoreComputations(std::vector<StoreInst *> &Stores,
                         cfghelper::cfg &PostDom, cfghelper::cfg &Dominator) {

  std::vector<std::set<Instruction *>> Computations;
  for (StoreInst *store : Stores) {
    std::set<Value *> Initial = {store->getValueOperand(),
                                 store->getPointerOperand(),
                                 store->getParent()};
    Computations.push_back(collectComputations(Initial, PostDom, Dominator));
  }
  return Computations;
}

Function *createBoneDefinition(Function *F, Loop *Outermost, int ParentLoopId,
                               StoreInst *Store) {
  // create function signature.
  const int NestId = function::getNestId(F);
  const int StmtId = statement::getStatementId(Store);
  Loop *ParentLoop = loop::getLoopWithId(Outermost, ParentLoopId);
  const int LoopDepth = ParentLoop->getLoopDepth();

  const std::string BoneName =
      utils::concat("apollo_bone.nest_", NestId, ".stmt_", StmtId);
  LLVMContext &Context = F->getContext();

  Type *voidTy = Type::getVoidTy(Context);
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *GlobalParamsTy = F->getFunctionType()->getParamType(0);
  Type *PhiStateTy = F->getFunctionType()->getParamType(1);
  Type *CoefTy = Type::getInt8PtrTy(Context, 0);

  std::vector<Type *> ParamTypes = std::vector<Type *>(LoopDepth + 3, i64Ty);
  ParamTypes[0] = GlobalParamsTy;
  ParamTypes[1] = PhiStateTy;
  ParamTypes[2] = CoefTy;
  FunctionType *BoneType = FunctionType::get(voidTy, ParamTypes, false);
  Function *Bone = Function::Create(BoneType, Function::ExternalLinkage,
                                    BoneName, F->getParent());
  // name the virtual iterators
  auto argIt = Bone->arg_begin();
  for (int i = 0; argIt != Bone->arg_end(); ++argIt, ++i) {
    if (i == 0)
      argIt->setName("param");
    else if (i == 1)
      argIt->setName("phi_state");
    else if (i == 2)
      argIt->setName("coefs");
    else if (i > 2)
      argIt->setName(utils::concat("vi.", i - 3));
  }
  return Bone;
}

void extractComputations(Function *Bone, Function *F, Loop *Outermost,
                         StoreInst *Store,
                         std::set<Instruction *> &Computations) {
  // These maps map original to cloned.
  std::map<BasicBlock *, BasicBlock *> bMap;
  std::map<Value *, Value *> iMap;

  // Map the global params
  auto F_arg_itr = F->arg_begin();
  auto Bone_arg_itr = Bone->arg_begin();

  iMap[&*(F_arg_itr)] = &*(Bone_arg_itr);
  ++F_arg_itr;
  ++Bone_arg_itr;
  iMap[&*(F_arg_itr)] = &*(Bone_arg_itr);

  // Clone the function
  extractCodeBone(Bone, F, Outermost, Computations, bMap, iMap);

  // clone the store!
  assert(Store->getParent() && "Required basic block not present");
  assert(bMap[Store->getParent()] && "Required basic block not preserved");

  Instruction *StoreClone = Store->clone();
  IRBuilder<> Builder(bMap[Store->getParent()]->getTerminator());
  Builder.Insert(StoreClone);

  if (isa<Instruction>(Store->getValueOperand()) ||
      isa<Argument>(Store->getValueOperand())) {
    StoreClone->setOperand(0, iMap[Store->getValueOperand()]);
  }
  if (isa<Instruction>(Store->getPointerOperand()) ||
      isa<Argument>(Store->getPointerOperand())) {
    StoreClone->setOperand(1, iMap[Store->getPointerOperand()]);
  }
}

// \brief Create a single store code-bone
// \returns nullptr iff a useable code-bone couldn't be constructed
Function *createSingleStoreBone(Function *F, Loop *Outermost, StoreInst *Store,
                                std::set<Instruction *> &Computations) {

  DEBUG({
      dbgs() << __FUNCTION__ << ":\n"
	     << "Creating stores code-bone for store instruction:\n";
      Store->print(dbgs());
      dbgs() << "\n";

      dbgs() << "From Function " << F->getName() << ":\n";
      F->print(dbgs(), nullptr, false, true);
      dbgs() << "------------------------------------------\n";
    });

  const int StmtLoopId =
      cast<ConstantInt>(metadata::getMetadataOperand(Store, mdkind::ParentLoop))
      ->getSExtValue();

  Function *Bone = createBoneDefinition(F, Outermost, StmtLoopId, Store);
  extractComputations(Bone, F, Outermost, Store, Computations);
  auto AllViValues = std::move(getAllViValues(Bone, Outermost, StmtLoopId));

  // Use a dummy. It is not used after, so it's not required to update it.
  auto Dummy = std::map<Value *, Value *>();

  // Predict basic scalars
  if (!basicScalarLinearFunction(Bone, Outermost, AllViValues, Dummy)
      || !statementLinearFunctions(Bone, Outermost, AllViValues)) {
    eraseCodeBone(Bone);

    DEBUG({
      dbgs() << __FUNCTION__ << ": "
	     << "Failed to extract a stores code-bone\n"
	     << "------------------------------------------\n";
      });

    return nullptr;
  }

  DEBUG({
      dbgs() << __FUNCTION__ << ": "
	<< "Extracted stores code-bone " << Bone->getName();
	Bone->print(dbgs(), nullptr, false, true);
	dbgs() << "------------------------------------------\n";
    });

#ifndef NDEBUG
  assert(!verifyFunction(*Bone, &errs()) &&
	 "Constructed an invalid code-bone for stores");
#endif // NDEBUG

  return Bone;
}

bool
createStoreBones(Function *F, Loop *Outermost,
		 std::vector<StoreInst *> &Stores,
                 std::vector<std::set<Instruction *>> &StoresComputations,
		 std::vector<Function *> &Bones) {
  //  std::vector<Function *> Bones(Stores.size(), nullptr);
  for (unsigned i = 0; i < Stores.size(); ++i)
    {
      auto Bone = createSingleStoreBone(F, Outermost, Stores[i],
					StoresComputations[i]);
      if (Bone == nullptr) {
	// Failed to create a valid code-bone
	return false;
      }

      Bones.push_back(Bone);
    }
  return true;
}

} // end anonymous namespace

namespace apollo {

DefinePassCreator(StoreBones);
char StoreBones::ID = 0;

StoreBones::StoreBones()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::code_bones>(), "StoreBones") {}

void StoreBones::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool StoreBones::runOnApollo(Function *F) {
  const DataLayout &DL = F->getParent()->getDataLayout();
  LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
  Loop *Outermost = function::getOutermostLoopInFunction(F, &LI);
  cfghelper::cfg CFG(*F, LI, true);
  cfghelper::cfg Dom = CFG.dominator();
  cfghelper::cfg PostDom = CFG.postDominator();
  std::vector<StoreInst *> Stores = std::move(collectStoreStatements(F));
  std::vector<std::set<Instruction *>> StoresComputations =
      std::move(collectStoreComputations(Stores, PostDom, Dom));

  std::vector<Function *> StoreBones;
  if (!createStoreBones(F, Outermost, Stores, StoresComputations, StoreBones)) {
    // Failed to create a valid code-bone
    for (Function* Bone: StoreBones) {
      eraseCodeBone(Bone);
    }
    return true;  // Bail out early
  }

  for (Function *bone : StoreBones) {
    apollo::optimize(bone, DL);
  }

#ifndef NDEBUG
  {
    bool debug_errors = false;
    assert(!verifyModule(*(F->getParent()), &errs(), &debug_errors) &&
	   "Malformed module created by apollo::StoreBones");
  }
#endif // NDEBUG

  return true;
}

} // end namespace apollo

INITIALIZE_PASS_BEGIN(StoreBones, "APOLLO_STORE_BONES", "APOLLO_STORE_BONES",
                      false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(StoreBones, "APOLLO_STORE_BONES", "APOLLO_STORE_BONES",
                    false, false)
