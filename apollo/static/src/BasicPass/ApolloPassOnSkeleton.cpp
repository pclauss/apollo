//===--- ApolloPassOnSkeleton.cpp -----------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "llvm/IR/Module.h"
#include "llvm/Support/Debug.h"

#include "BasicPass/ApolloPassOnSkeleton.h"
#include "Utils/FunctionUtils.h"

using namespace llvm;

namespace apollo {
namespace pass {

ApolloPassOnSkeleton::ApolloPassOnSkeleton(char &ID, const std::string &SN,
		const std::string &_name) :
		name(_name), ModulePass(ID) {
	SkeletonNames = {SN};
}

ApolloPassOnSkeleton::~ApolloPassOnSkeleton() {
}

void ApolloPassOnSkeleton::getAnalysisUsage(AnalysisUsage &AU) const {
	AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloPassOnSkeleton::runOnModule(Module &M) {
	DEBUG_WITH_TYPE("apollo-pass-0",
			dbgs() << "Running pass on apollo_loop_SKELETON functions: " << name
					<< "\n");
	std::vector<Function *> ModuleFunctions;
	for (Function &F : M) {
		for (std::string &suffix : SkeletonNames) {
			if (function::isApolloSkeletonFunction(&F, suffix))
				ModuleFunctions.push_back(&F);
		}
	}
	bool Modify = false;
	for (Function *F : ModuleFunctions) {
		DEBUG_WITH_TYPE("apollo-pass-1",
				dbgs() << "   function:" << F->getName().data());
		if (runOnApollo(F)) {
			Modify = true;
		}
	}
	DEBUG_WITH_TYPE("apollo-pass-1",
			dbgs() << " ==> modified = " << (Modify ? "true" : "false")
					<< "\n");
	return Modify;
}

bool ApolloPassOnSkeleton::runOnApollo(Function *apolloFunction) {
	LoopInfo &LI =
			getAnalysis<LoopInfoWrapperPass>(*apolloFunction).getLoopInfo();
	Loop *L = function::getOutermostLoopInFunction(apolloFunction, &LI);
	assert(L && "runOnApollo: Loop not found!");
	return runOnLoopLI(L, &LI);
}

bool ApolloPassOnSkeleton::runOnLoopLI(Loop *L, LoopInfo *LI) {
	return runOnLoop(L);
}

bool ApolloPassOnSkeleton::runOnLoop(Loop *L) {
	return false;
}

} // end namespace pass
} // end namecpace apollo
