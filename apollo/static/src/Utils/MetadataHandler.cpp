//===--- MetadataHandler.cpp ----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Utils/MetadataHandler.h"
#include "llvm/IR/LLVMContext.h"

using namespace llvm;

namespace apollo {
namespace metadata {

void attachMetadata(Instruction *I, 
                    const std::string &mKind,
                    const std::string &mNode) {
  MDNode *MD = MDNode::get(I->getContext(), 
                           MDString::get(I->getContext(), mNode));
  I->setMetadata(mKind, MD);
}

void attachMetadata(Instruction *I, 
                    std::string mKind,
                    const std::vector<Value *> &mNode) {
  std::vector<Metadata *> MData;
  for (Value *v : mNode) {
    MData.push_back(ValueAsMetadata::get(v));
  }
  MDNode *MD = MDNode::get(I->getContext(), MData);
  I->setMetadata(mKind, MD);
}

Value *getMetadataOperand(Instruction *I, 
                          const std::string &mKind,
                          unsigned Op) {
  MDNode *MD = I->getMetadata(mKind);
  assert(Op < getMetadataNumOperands(I, mKind));
  if (MD) {
    Metadata *Operand = MD->getOperand(Op);
    ValueAsMetadata *AsValue = dyn_cast<ValueAsMetadata>(Operand);
    if (AsValue) {
      Value *Val = AsValue->getValue();
      return Val;
    }
  }
  assert(false && "getMetadataOperand: No metadata with that kind");
  return 0x0;
}

unsigned getMetadataNumOperands(Instruction *I, const std::string &mKind) {
  MDNode *MD = I->getMetadata(mKind);
  if (MD)
    return MD->getNumOperands();
  assert(false && "getMetadataOperand: No metadata with that kind");
  return 42;
}

std::string getMetadataString(Instruction *I, const std::string &mKind) {
  Value *Val = getMetadataOperand(I, mKind, 0);
  MDNode *MD = I->getMetadata(mKind);
  if (MD) {
    Metadata *MData = MD->getOperand(0);
    if (MData && MDString::classof(MData))
      return (cast<MDString>(*MData)).getString();
  }
  assert(false && "getMetadataNode: Metadata not transformable to string");
  return 0x0;
}

bool hasMetadataKind(Instruction *I, const std::string &mKind) {
  const unsigned MK = I->getContext().getMDKindID(mKind);
  return MK && I->getMetadata(MK);
}

} // end namespace metadata
} // end namsepace apollo
