//===--- TypeUtils.cpp ----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Utils/Utils.h"
#include "Utils/GlobalUtils.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Constant.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Type.h"

using namespace llvm;

namespace apollo {
namespace type {

StructType *getVecType(Module *M, Type *ContainedTy) {
  const std::string Name =
      utils::concat<const char *, Type &>("apollo.vec.", *ContainedTy);
  LLVMContext &Context = M->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *Ty = M->getTypeByName(Name);
  StructType *StrVecTy;
  if (!Ty) {
    PointerType *PtrContained = PointerType::get(ContainedTy, 0);
    std::vector<Type *> StrFields = {i64Ty, PtrContained};
    StrVecTy = StructType::create(Context, StrFields, Name);
  } else {
    StrVecTy = cast<StructType>(Ty);
    assert(StrVecTy->getElementType(0) == i64Ty && "Wrong contained type.");
    PointerType *ptrTy = dyn_cast<PointerType>(StrVecTy->getElementType(1));
    assert(ptrTy != 0x0 && ptrTy->getContainedType(0) == ContainedTy &&
           "Wrong contained type.");
  }
  return StrVecTy;
}

StructType *getVecType(Module *M) {
  LLVMContext &Context = M->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  return getVecType(M, i64Ty);
}

template <typename T>
Constant *getVecIntConstant(Module *M, std::vector<T> &Values, Type *intTy) {
  LLVMContext &Context = M->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  ArrayType *iArrayTy = ArrayType::get(intTy, Values.size());
  StructType *vecTy = getVecType(M, intTy);
  Constant *ConstSize = ConstantInt::get(i64Ty, Values.size());
  Constant *Zero = ConstantInt::get(i64Ty, 0);
  std::vector<Constant *> GExpIdx = {Zero, Zero};
  std::vector<Constant *> ConstValues;

  for (unsigned value : Values) {
    ConstValues.push_back(ConstantInt::get(intTy, value));
  }
  Constant *ConstData = ConstantArray::get(iArrayTy, ConstValues);
  GlobalVariable *GvData = cast<GlobalVariable>(
      M->getOrInsertGlobal(global::getUnnamedGlobal(M), iArrayTy));
  GvData->setConstant(true);
  GvData->setInitializer(ConstData);
  GvData->setLinkage(GlobalVariable::PrivateLinkage);
  Constant *GExp = ConstantExpr::getGetElementPtr(
      GvData->getType()->getContainedType(0), GvData, GExpIdx);
  std::vector<Constant *> Fields = {ConstSize, GExp};
  return ConstantStruct::get(vecTy, Fields);
}

StructType *getPairType(Module *M, Type *TyA, Type *TyB) {
  if (TyA == 0x0)
    TyA = Type::getInt64Ty(M->getContext());
  if (TyB == 0x0)
    TyB = Type::getInt64Ty(M->getContext());
  const std::string Name = 
       utils::concat<const char *, Type &, const char *, Type &>(
                                               "apollo.pair.", *TyA, "_", *TyB);
  Type *Ty = M->getTypeByName(Name);
  if (!Ty) {
    auto StrFields = {TyA, TyB};
    Ty = StructType::create(M->getContext(), StrFields, Name);
  }
  return cast<StructType>(Ty);
}

} // end namespace type
} // end namespace apollo
