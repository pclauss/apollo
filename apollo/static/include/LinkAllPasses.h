//===--- LinkAllPasses.h --------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef APOLLO_LINK_ALL_PASSES_H
#define APOLLO_LINK_ALL_PASSES_H

#include "CreatePasses.h"

namespace llvm {

class Pass;
class PassInfo;
class PassRegistry;
class RegionPass;

} // end namespace llvm

namespace {

struct ApolloForceLinking {
  ApolloForceLinking() {
    // We must reference the passes in such a way that compilers will not
    // delete it all as dead code, even with whole program optimization,
    // yet is effectively a NO-OP. As the compiler isn't smart enough
    // to know that getenv() never returns -1, this will do the job.
    if (std::getenv("bar") != (char *)-1)
      return;
    apollo::createApolloLoopExtractPass();
    apollo::createApolloIgnoreEmptyPass();
    apollo::createApolloSignatureNormalizePass();
    apollo::createApolloPhiHandlingPass();
    apollo::createApolloInstrumentLinearEquationPass();
    apollo::createApolloViInsertPass("");
    apollo::createApolloSimpleLoopLatchPass("");
    apollo::createApolloStatementMetadataAdderPass();
    apollo::createStatementVerificationAvoidancePass();
    apollo::createRegisterStaticInfoPass();
    apollo::createRegisterStmtAliasPass();
    apollo::createApolloCloneLoopFunctionPass("", "");
    apollo::createApolloReg2MemPass("");
    apollo::createInstrumentFixFunctionSignaturePass();
    apollo::createApolloInstrumentViBoundsPass();
    apollo::createApolloInstrumentPhiStatePass();
    apollo::createFunctionCallParentPass();
    apollo::createApolloInstrumentOnConditionPass();
    apollo::createApolloInstrumentMemAccessPass();
    apollo::createApolloInstrumentIterationCountPass();
    apollo::createApolloInstrumentPhiPass();
    apollo::createInstrumentFunctionCallPass();
    apollo::createBasicScalarPredictPass();
    apollo::createJitExportFctPass();
    apollo::createSwitchingCallPass();
    apollo::createApolloGlobalCombinePass();
  }
} ApolloForceLinking;

} // end anonymous namespace

#endif
