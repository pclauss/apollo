//===--- CodeBonesUtils.h -------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef CODE_BONES_UTILS_H
#define CODE_BONES_UTILS_H

#include "Utils/ViUtils.h"

#include "llvm/Analysis/LoopInfo.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Value.h"

#include <set>
#include <vector>

namespace cfghelper {

struct cfg {
  std::vector<llvm::BasicBlock *> Nodes;
  std::vector<std::set<int>> Neighbours;
  std::map<llvm::BasicBlock *, int> Block2Idx;

  cfg();
  cfg(llvm::Function &F, llvm::LoopInfo &LI, bool BreakLatch);

  cfg reverse() const;
  cfg dominator() const;
  cfg postDominator() const;
  bool hasNegihbour(llvm::BasicBlock *BBa, llvm::BasicBlock *BBb) const;

  std::ostream &dump(std::ostream &OS) const;
};

} // end namespace cfg

namespace apollo {

std::set<llvm::Instruction *>
collectComputations(std::set<llvm::Value *> &Initial, cfghelper::cfg &PD,
                    cfghelper::cfg &DT);

std::vector<llvm::Value *> getBoneVi(llvm::Function *F);

/// \brief Extract a code-bone for a set of computations in a function
///
/// Constructs a function body for code-bone \p Bone from copies of the
/// instructions and basic-blocks in function \p Orig. The code-bone is
/// constructed so that it includes anything needed by the values (or
/// instructions) in \p Computations. Sets the linkage for \p Bone to Private
///
/// \p Bone [in,out] The code-bone function to populate
/// \p Orig The original function
/// \p Outermost The outermost loop in \p Orig
/// \p Computations The values that the code-bone will be used to compute
/// \p bMap [in,out] The mapping between basic-blocks in \p Orig and \p Bone
/// \p iMap [in,out] The mapping between instructions in \p Orig and \p Bone
void extractCodeBone(llvm::Function *Bone,
		     llvm::Function *Orig,
		     llvm::Loop *Outermost,
		     std::set<llvm::Instruction *> &Computations,
		     std::map<llvm::BasicBlock *, llvm::BasicBlock *> &bMap,
		     std::map<llvm::Value *, llvm::Value *> &iMap);

/// \brief Erase a code-bone from its containing module and remove all
/// references to it.
/// \p Bone The code-bone to erase
void eraseCodeBone(llvm::Function* Bone);

std::vector<llvm::Value *>
loadScalarCoefficients(llvm::PHINode *Scalar, llvm::Loop *Outermost,
                       apollo::ir::ApolloIRBuilder &Builder);

std::vector<llvm::Value *>
loadStatementCoefficients(llvm::Instruction *Stmt, llvm::Loop *Outermost,
                          apollo::ir::ApolloIRBuilder &Builder,
                          std::vector<llvm::Value *> &TubeCallValue,
                          const std::string &Str);

std::vector<llvm::Value *>
loadBoundCoefficients(const unsigned LoopId, llvm::Loop *Outermost,
                      apollo::ir::ApolloIRBuilder &Builder);

int getMaximumLoopId(std::set<llvm::Instruction *> &Computations);

bool basicScalarLinearFunction(llvm::Function *Bone, llvm::Loop *Outermost,
                               std::map<int, llvm::Value *> &AllViValues,
                               std::map<llvm::Value *, llvm::Value *> &iMap);
bool statementLinearFunctions(llvm::Function *Bone, llvm::Loop *Outermost,
                              std::map<int, llvm::Value *> &AllViValues);

std::map<int, llvm::Value *>
getAllViValues(llvm::Function *F, llvm::Loop *Outermost, const int ParentLoopId);

void optimize(llvm::Function *F, const llvm::DataLayout &DL);

} // end namespace apollo

#endif
