//===--- ApolloCloneLoopFunction.h ----------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef APOLLO_CLONE_LOOP_FUN_H
#define APOLLO_CLONE_LOOP_FUN_H

#include "BasicPass/ApolloPass.h"
#include "BasicPass/ApolloPassOnSkeleton.h"

#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/IR/Instructions.h"

#include <utility>

namespace apollo {

struct ApolloCloneLoopFunction : public pass::ApolloPassOnSkeleton {
  static char ID;
  std::string CloneTo;
  std::string CloneFrom;

  ApolloCloneLoopFunction();
  ApolloCloneLoopFunction(std::string CTo, std::string CFrom);
  virtual bool runOnApollo(llvm::Function *F);
  /// \brief Map the arguments of the original function with the arguments
  //         of the new clone.
  static void mapArguments(llvm::Function *F, llvm::Function *Clone,
                           llvm::ValueToValueMapTy &ValueMap);

  /// \brief Fix the operands of the instructions after cloning.
  ///        The clone does not fix the uses of the instructions,
  ///        and they still reference the original ones.
  static void fixInstructions(llvm::Function *F,
                              llvm::ValueToValueMapTy &ValueMap);

  /// \brief Add arguments to a function definition.
  /// \param The orginal function
  /// \param A vector of tuples (argument name, argument type).
  /// \param If specified, the new return type of the function.
  /// \param RetTy the new return type, by default keep the old one.
  /// \return The new function with the new definitions
  static llvm::Function *
  addParameters(llvm::Function *OldFunction,
                std::vector<std::pair<std::string, llvm::Type *>> &NewArgs,
                llvm::Type *RetTy = 0x0);
};

} // end namespace apollo

#endif
