//===--- Skeletons.cpp ----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include <JitCompiler/CodeGenerator.h>
#include "Backdoor/Backdoor.h"
#include "SkeletonConfig.h"
#include "JitCompiler/PassUtils.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Constant.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Value.h"

#include <cstdlib>

extern APOLLO_PROG *apolloProg;

using namespace apollo;

CodeGenerator::CodeGenerator(StaticSkeletonInfo *StatSkeletons,
                             unsigned NumSkeletons) {

  traceEnter("CodeGenerator::CodeGenerator");
  assert(NumSkeletons == 4);

  RegisterStaticInfo =
      (StaticInfoInitFunTy)StatSkeletons[apollo::skeleton::static_info::id]
          .FunctionPtr;
  Instrument =
      (InstrumentationFunTy)StatSkeletons[apollo::skeleton::instrumentation::id]
          .FunctionPtr;
  BasicScalarPredict =
      (BasicScalarPredictFunTy)
          StatSkeletons[apollo::skeleton::basic_scalar_predict::id]
              .FunctionPtr;

  assert(RegisterStaticInfo);
  assert(Instrument);
  assert(BasicScalarPredict);

  CodeBonesBytecode = StatSkeletons[apollo::skeleton::code_bones::id].Bytecode;
  Optimized = nullptr;
  Verification = nullptr;

  assert(CodeBonesBytecode.Size > 0 && CodeBonesBytecode.Ptr != nullptr);

  llvm::InitializeNativeTarget();
  llvm::InitializeNativeTargetAsmPrinter();

  traceExit("CodeGenerator::CodeGenerator");
}

CodeGenerator::~CodeGenerator() {
  traceEnter("CodeGenerator::~CodeGenerator");
  traceExit("CodeGenerator::~CodeGenerator");
}

void CodeGenerator::callRegisterStaticInfo(struct StaticNestInfo *StaticInfo_) {
  traceEnter("CodeGenerator::register_static_info()");
  RegisterStaticInfo(StaticInfo_);
  traceExit("CodeGenerator::register_static_info()");
}

long CodeGenerator::callInstrumentation(NestParamsStruc &Param, PhiState &State,
                                        InstrumentationResult *Results,
                                        const long ChunkLower, 
                                        const long ChunkUpper,
                                        const long InstOuter, 
                                        const long InstInner) {

  traceEnter(backdoorutils::concat("CodeGenerator::call_instrumentation(",
                                   ChunkLower, ", ", ChunkUpper, ", ",
                                   InstOuter, ",", InstInner, ")"));

  const long SkeletonResult = Instrument(Param.Param, State, (void *)Results, 
                                         ChunkLower, ChunkUpper,
                                         InstOuter, InstInner);

  traceExit(backdoorutils::concat("CodeGenerator::call_instrumentation = ",
                                  SkeletonResult));
  return SkeletonResult;
}

long CodeGenerator::callOriginal(NestParamsStruc &Param, PhiState &State,
                                 const long ChunkLower, 
                                 const long ChunkUpper) {

  traceEnter(backdoorutils::concat("BaseSkeletons::call_original(", ChunkLower,
                                   ", ", ChunkUpper, ")"));

  const long SkeletonResult = callInstrumentation(Param, State, nullptr, 
                                                  ChunkLower, ChunkUpper, 0, 0);

  traceExit(backdoorutils::concat("BaseSkeletons::call_original = ", 
                                  SkeletonResult));

  return SkeletonResult;
}

PhiState CodeGenerator::predictPhiState(PhiState &State,
                                        std::shared_ptr<PredictionModel> &PM,
                                        const long Iteration) {

  traceEnter(backdoorutils::concat("CodeGenerator::predict_phi_state(",
                                   Iteration, ")"));
  PhiState PredictedState = State.clone();
  BasicScalarPredict(PredictedState, (void *)PM.get(), Iteration);
  traceExit("CodeGenerator::predict_phi_state");
  return PredictedState;
}


/// \brief Load the code bones module from the string variable
/// containing its bytecode
llvm::Module *CodeGenerator::loadCodeBonesModule() {
  if (!OriginalCodeBonesModule) {
    OriginalCodeBonesModule =
        jit::loadModule(CodeBonesBytecode.Ptr, CodeBonesBytecode.Size);
  }
  return OriginalCodeBonesModule.get();
}

void CodeGenerator::callVerificationOnly(NestParamsStruc &Params,
                                         PhiState &PhiStateObj,
                                         std::shared_ptr<PredictionModel> &PM,
                                         const long ChunkLower, 
                                         const long ChunkUpper) {

  traceEnter(backdoorutils::concat("CodeGenerator::call_verification_only(",
                                   ChunkLower, ", ", ChunkUpper, ")"));
  assert(Verification);

  void *ParamsPtr = Params.Param;
  void *PMPtr = (void *)PM.get();

  Verification(ParamsPtr, PhiStateObj, PMPtr, ChunkLower, ChunkUpper);

  traceExit("CodeGenerator::call_verification_only)");
}

void CodeGenerator::callOptimized(NestParamsStruc &Params,
                                  PhiState &PhiStateObj,
                                  std::shared_ptr<PredictionModel> &PM,
                                  const long ChunkLower, 
                                  const long ChunkUpper) {

  traceEnter(backdoorutils::concat("CodeGenerator::call_optimized(", ChunkLower,
                                   ", ", ChunkUpper, ")"));
  assert(Optimized);

  void *ParamsPtr = Params.Param;
  void *PMPtr = (void *)PM.get();

  if (!apolloProg->ShouldRollback) {
    Optimized(ParamsPtr, PhiStateObj, PMPtr, ChunkLower, ChunkUpper);
    PhiStateObj = predictPhiState(PhiStateObj, PM, ChunkUpper);
  }

  traceExit("CodeGenerator::call_optimized)");
}

void CodeGenerator::patchModules(NestParamsStruc &Params,
                                 std::shared_ptr<PredictionModel> &PM) {

  traceEnter("CodeGenerator::patch_modules()");
  eventStart("jit");

  loadCodeBonesModule();
  JitStateObj = std::move(std::unique_ptr<JitState>(new JitState()));
  JitStateObj->PatchedModule =
      std::move(jit::getPatchedModule(OriginalCodeBonesModule, Params, PM));

  eventEnd("jit");
  traceExit("CodeGenerator::patch_modules()");
}

void CodeGenerator::generateOptimizedCode(
    std::shared_ptr<ScanTypeStruct> &Scan) {
  traceEnter("CodeGenerator::generate_optimized_code()");
  eventStart("jit");

  assert(JitStateObj);
  assert(JitStateObj->PatchedModule);
  JitStateObj->ExecutionEngine =
      std::move(jit::getExecutionEngine(JitStateObj->PatchedModule, Scan));

  auto BinaryFuncionPtrs = jit::getFunctionPtr(JitStateObj->ExecutionEngine);

  Verification = JitStateObj->VerificationFunction =
      (CodeBonesFunTy)BinaryFuncionPtrs.first;
  Optimized = JitStateObj->OptimizedFunction =
      (CodeBonesFunTy)BinaryFuncionPtrs.second;

  eventEnd("jit");
  traceExit("CodeGenerator::generate_optimized_code()");
}

static CodeGenerator::CodeBonesDescriptor
buildCodeBoneDescriptor(llvm::Function &F) {
  CodeGenerator::CodeBonesDescriptor ADescriptor;
  ADescriptor.Name = F.getName();

  // Search and save the IDs for all the loads, stores
  // and scalars instructions in the bone's function
  for (llvm::BasicBlock &block : F) {
    for (llvm::Instruction &instruction : block) {
      if (isStatement(&instruction)) {
        llvm::Value *StatementId =
            getMetadataOperand(&instruction, "ApolloStatementId");
        llvm::ConstantInt *StatementIdConst =
            llvm::cast<llvm::ConstantInt>(StatementId);
        const int StmtId = StatementIdConst->getSExtValue();
        if (llvm::isa<llvm::LoadInst>(instruction))
          ADescriptor.Loads.push_back(StmtId);
        else
          ADescriptor.Stores.push_back(StmtId);

      } else if (isBasicScalar(&instruction)) {
        llvm::Value *ScalarId = getMetadataOperand(&instruction, "ApolloPhiId");
        llvm::ConstantInt *ScalarIdConst =
            llvm::cast<llvm::ConstantInt>(ScalarId);
        const int ScId = ScalarIdConst->getSExtValue();
        ADescriptor.Scalars.push_back(ScId);
      }
    }
  }

  // If the bone is a verification bone, search and save the id of
  // the statement associated to it
  int Dummy = 0;
  if (ADescriptor.Name.find("verif_stmt_") != std::string::npos) {
    int MemVerifId = 0;
    sscanf(ADescriptor.Name.c_str(), "apollo_bone.nest_%d.verif_stmt_%d",
           &Dummy, &MemVerifId);
    ADescriptor.MemVerifIds.push_back(MemVerifId);
  } else if (ADescriptor.Name.find("verif_scalar_init_") != std::string::npos) {
    int ScalarVerifId = 0;
    sscanf(ADescriptor.Name.c_str(), "apollo_bone.nest_%d.verif_scalar_init_%d",
           &Dummy, &ScalarVerifId);
    ADescriptor.ScalarVerifIds.push_back(ScalarVerifId);
  } else if (ADescriptor.Name.find("verif_scalar_final_") !=
             std::string::npos) {
    int ScalarVerifId = 0;
    sscanf(ADescriptor.Name.c_str(),
           "apollo_bone.nest_%d.verif_scalar_final_%d", &Dummy, &ScalarVerifId);
    ADescriptor.ScalarVerifIds.push_back(ScalarVerifId);
  } else if (ADescriptor.Name.find("verif_bound_") != std::string::npos) {
    int BoundVerifId = 0;
    sscanf(ADescriptor.Name.c_str(), "apollo_bone.nest_%d.verif_bound_%d",
           &Dummy, &BoundVerifId);
    ADescriptor.BoundVerifIds.push_back(BoundVerifId);
  }
  return ADescriptor;
}


/// \brief Returns the code bones descriptors for all
/// code bones included in this code generator patched
/// module object
std::vector<CodeGenerator::CodeBonesDescriptor>
CodeGenerator::getCodeBonesDescriptors() {

  traceEnter("CodeGenerator::get_code_bones_descriptors()");
  std::vector<CodeGenerator::CodeBonesDescriptor> Descriptors;
  if (this->JitStateObj) {
    llvm::Module *M = this->JitStateObj->PatchedModule.get();
    if (M) {
      for (llvm::Function &code_bone : *M) {
        const bool IsCodeBone = 
                  code_bone.getName().startswith("apollo_bone.nest_");
        if (IsCodeBone) {
          assert(!code_bone.getBasicBlockList().empty());
          Descriptors.emplace_back(buildCodeBoneDescriptor(code_bone));
        }
      }
    }
  }
  traceExit("CodeGenerator::get_code_bones_descriptors()");
  return Descriptors;
}
