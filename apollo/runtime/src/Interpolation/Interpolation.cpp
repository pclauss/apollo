//===--- Interpolation.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Interpolation/Interpolation.h"
#include "Regression/Regression.h"
#include "Backdoor/Backdoor.h"
#include "Backup/AllocatedMemArrange.h"
#include "RuntimeConfig.h"
#include "APOLLO_PROG.h"
#include <cstdarg>

extern APOLLO_PROG *apolloProg;
extern RuntimeConfig apolloRuntimeConfig;

using EventTy = InstrumentationResult::InstrumentationEvent;

/// Print prediction model (debug mode)
void PredictionModel::dump(std::ostream &OS) {
	OS << "PredictionModel: dump() [\n";
	for (size_t i = 0; i < Memory.size(); ++i) {
		OS << "\tmem[" << i << "]: "
				<< predictionTypeToString(Memory[i].PreType) << " ";
		if (Memory[i].isLinear() || Memory[i].isLinearAfterReIndexing()) {
			OS << "< ";
			for (size_t j = 0; j < Memory[i].LF.size(); ++j)
				OS << Memory[i].LF[j] << " ";
			OS << ">";
		}
		if (Memory[i].isTube() || Memory[i].isTubeAfterReIndexing()) {
			OS << "< ";
			for (size_t j = 0; j < Memory[i].LF.size(); ++j) {
				OS << Memory[i].LF[j] << " ";
			}
			OS << "> Tube Width: " << Memory[i].TubeWidth
					<< " Regression correlation: " << Memory[i].RC;
		}
		OS << "\n";
	}
	for (size_t i = 0; i < Scalars.size(); ++i) {
		OS << "\tscalar[" << i << "]: "
				<< predictionTypeToString(Scalars[i].PreType) << " ";
		if (Scalars[i].isLinear()) {
			OS << "< ";
			for (size_t j = 0; j < Scalars[i].LF.size(); ++j) {
				OS << Scalars[i].LF[j] << " ";
			}
			OS << ">";
		} else if (Scalars[i].PreType == ScalarSemiLinear) {
			OS << "< ";
			for (size_t j = 0; j < Scalars[i].LF.size() - 2; ++j) {
				OS << "* ";
			}
			OS << Scalars[i].LF[Scalars[i].LF.size() - 2] << " @base_value";
			OS << ">";
		}

		OS << "\n";
	}
	for (size_t i = 0; i < Bounds.size(); ++i) {
		OS << "\tbound[" << i << "]: "
				<< predictionTypeToString(Bounds[i].PreType) << " ";
		if (Bounds[i].isPredicted()) {
			OS << "< ";
			for (size_t j = 0; j < Bounds[i].LF.size(); ++j) {
				OS << Bounds[i].LF[j] << " ";
			}
			OS << ">";
		}

		if (Bounds[i].isTube()) {
			OS << " Max = " << Bounds[i].Max;
		}
		OS << "\n";
	}
	OS << "]";
}

template<typename VecTy>
static bool validateLF(const std::vector<EventTy> &Events, const int NumParents,
		const long MinValue, const VecTy &Coefs) {

	for (size_t event = 0; event < Events.size(); ++event) {
		long Validate = (Events[event].Value - MinValue) - Coefs[NumParents];
		for (int j = 0; j < NumParents; ++j) {
			// multiply the iterator by the prediction
			Validate -= Coefs[j] * Events[event].Coefs[j];
		}
		if (Validate != 0) {
			return false;
		}
	}
	return true;
}

static std::vector<PredictionModel::CoefTy> solveLinearFunction(
		const std::vector<EventTy> &Events, const int NumParents) {

	// Interpolate linear functions. FIXME: do this with rationals!
	arma::Col<double> Coefs(NumParents + 1);

	// Use the minimum value to reduce the values of the coefficients to
	// win precision.
	const long MinValue = std::accumulate(Events.begin(), Events.end(),
			Events[0].Value, [](long Val, const EventTy &B) {
				const long BVal = B.Value;
				if (std::abs<long>(Val) < std::abs<long>(BVal))
				return Val;
				return BVal;
			});

	// We try to solve the system using different number of events
	// for efficiency concerns. We consider using either
	// NumParents + 1 events to have a square matrix first and
	// then all of them
	std::vector<long> NbEventsToConsider = { NumParents + 1,
			(long) Events.size() };
	long upperBound = 2; // == (long) EventsToConsider.size()
	bool InterpolationSucceed = false;
	for (long i = 0; i < upperBound && !InterpolationSucceed; ++i) {

		try {
			long n_rows = NbEventsToConsider[i];
			long n_cols = NumParents + 1;
			arma::Mat<double> Iterators(n_rows, n_cols);
			arma::Col<double> Values(n_rows);

			// Fill the matrix
			for (long idx = 0; idx < n_rows; ++idx) {
				int Event = idx;

				// If we are only using NumParents + 1 events,
				// select them over all the events
				if (i == 0) {
					Event = ((long) std::pow(
							apolloRuntimeConfig.InstrumentationSample, idx) - 1)
							% Events.size();
				}

				// Fill the row number 'idx' with coefficients
				// of event number 'Event'
				for (int col = 0; col < NumParents; ++col) {
					Iterators(idx, col) = Events[Event].Coefs[col];
				}
				Iterators(idx, NumParents) = 1;
				Values(idx) = Events[Event].Value - MinValue;
			}

			// Try to solve and validate the result
			InterpolationSucceed = arma::solve(Coefs, Iterators, Values);
			if (InterpolationSucceed) {
				Coefs = arma::round(Coefs);
				InterpolationSucceed = validateLF(Events, NumParents, MinValue,
						Coefs);
			}
		} catch (...) {
			InterpolationSucceed = false;
		}
	}
	if (!InterpolationSucceed)
		return std::vector<long>();
	std::vector<PredictionModel::CoefTy> Solution(NumParents + 1, 0);
	std::copy(Coefs.begin(), Coefs.end(), Solution.begin());
	Solution.back() += MinValue;
	return Solution;
}

static std::vector<long> tryBuildSemiLinearScalar(
		const std::vector<EventTy> &Events, const int NumParents) {

	using EventsMapTy = std::map<std::vector<long>,
	std::vector<std::pair<long, long>>>;

	// the key vector contains the non-linear n-1 parents. the value pair contain
	// the parent loop iterator and the instrumented value.
	EventsMapTy EventsByParents;
	EventsMapTy::key_type ParentIterators(NumParents - 1, 0);
	EventsMapTy::mapped_type *LonguestEventSeq = nullptr;
	for (auto &event : Events) {
		for (int i = 0; i < NumParents - 1; ++i) {
			ParentIterators[i] = event.Coefs[i];
		}
		EventsMapTy::mapped_type IteratorValue = { std::pair<long, long>(
				event.Coefs[NumParents - 1], event.Value) };
		EventsMapTy::value_type ToInsert(ParentIterators, IteratorValue);
		std::pair<EventsMapTy::iterator, bool> Ret = EventsByParents.insert(
				ToInsert);
		if (Ret.second == false) {
			Ret.first->second.push_back(IteratorValue[0]);
		}
		if (LonguestEventSeq == nullptr
				|| LonguestEventSeq->size() < Ret.first->second.size()) {
			LonguestEventSeq = &Ret.first->second;
		}
	}
	// not enough events to interpolate a linear function.
	if (LonguestEventSeq == nullptr || LonguestEventSeq->size() == 1)
		return std::vector<long>();
	// in a nest like this:
	// for (i... )
	//   for (a = b[i]... a++)
	// we try to interpolate a function with this shape:
	//   f(vi,vj) = b*vj + base_addr(vi) = val_vi_vj
	// so:
	//   b = (val_vi_vj - val_vi_vj') / (vj' - vj)
	long Vj = LonguestEventSeq->at(0).first;
	long VjP = LonguestEventSeq->at(1).first;
	long ValViVj = LonguestEventSeq->at(0).second;
	long ValViVjP = LonguestEventSeq->at(1).second;
	long K = VjP - Vj;
	long Diff = ValViVjP - ValViVj;

	if (Diff % K != 0)
		return std::vector<long>();
	long B = Diff / K;
	// now validate what we interpolated.
	for (auto &pair : EventsByParents) {
		std::vector<std::pair<long, long>> &IteratorValueSamples = pair.second;
		std::pair<long, long> &ASample = IteratorValueSamples[0];
		long Vj = ASample.first;
		long ValViVj = ASample.second;
		for (size_t other = 1; other < IteratorValueSamples.size(); ++other) {
			std::pair<long, long> &OtherSample = IteratorValueSamples[other];
			long VjP = OtherSample.first;
			long ValViVjP = OtherSample.second;
			long K = VjP - Vj;
			long Diff = ValViVjP - ValViVj;
			if (B != (Diff / K))
				return std::vector<long>();
		}
	}
	// if we reach this point, we succedded.
	std::vector<long> F(NumParents + 1, 0);
	F[NumParents - 1] = B;
	return F;
}

PredictionModel::LinearFunctionTy PredictionModel::tryBuildLinearFunction(
		const std::vector<EventTy> &Events, const int NumParents,
		PredictionModel::PredictionType &Ty, bool IsScalar) {

	LinearFunctionTy F(NumParents + 1, 0);
	const bool NotExecuted = Events.size() == 0;
	if (NotExecuted) {
		Ty = PredictionType::NotExecuted;
		return F;
	}

	// Static events are always registered before dynamic ones
	// so we can check the type of first event
	const bool StaticallyRegistered = Events[0].InstrType
			== EventTy::InstrumentationType::StaticEvent;
	if (StaticallyRegistered) {
		std::copy(Events[0].Coefs, Events[0].Coefs + NumParents + 1, F.begin());
		Ty = PredictionType::StaticLinear;
		return F;
	}

	// Check if dynamic events all fit on a line using armadillo
	// If we are here, all events in Events are dynamic
	const auto Solution = std::move(solveLinearFunction(Events, NumParents));
	if (Solution.size() > 0) {
		for (int i = 0; i < NumParents; ++i) {
			F[i] = Solution[i];
		}
		F[NumParents] = Solution[NumParents];
		Ty = PredictionType::DynamicLinear;
		return F;
	}

	// for scalars it is possible to handle a case where the initial value is not
	// linear, but the increase is linear.
	if (IsScalar) {
		const auto Solution1 = std::move(
				tryBuildSemiLinearScalar(Events, NumParents));
		if (Solution1.size() > 0) {
			for (int i = 0; i < NumParents; ++i) {
				F[i] = Solution1[i];
			}
			F[NumParents] = Solution1[NumParents];
			Ty = PredictionType::ScalarSemiLinear;
			return F;
		}
	}
	Ty = PredictionType::NonLinear;
	return F;
}

std::shared_ptr<PredictionModel> PredictionModel::buildFromInstrumentation(
		const InstrumentationResult &In, bool EarlyStop) {

	traceEnter("PredictionModel::build_from_instrumentation");
	eventStart("interpolation");

	std::shared_ptr<PredictionModel> PM = std::make_shared<PredictionModel>();
	PM->UnpredictedDetected = false;
	PM->UnsafeCallDetected = !In.getFunCallsEvents().empty();

	StaticNestInfo *NestInfo = apolloProg->StaticInfo.get();
	const int NumLoops = NestInfo->Loops.size();
	const int NumStmts = NestInfo->Mem.size();
	const int NumScalars = NestInfo->Scalars.size();
	PM->Memory = std::vector<MemoryPrediction>(NumStmts, MemoryPrediction());
	PM->Scalars = std::vector<ScalarPrediction>(NumScalars, ScalarPrediction());
	PM->Bounds = std::vector<BoundPrediction>(NumLoops, BoundPrediction());

	std::ostream NullStream(0);
	std::ostream &Err1 = arma::get_cerr_stream(),
	  &err2 = arma::get_cerr_stream();
	arma::set_cerr_stream(NullStream);
	arma::set_cerr_stream(NullStream);

	// Memory accesses prediction
	for (int memID = 0; memID < NumStmts; ++memID) {

		if (EarlyStop && PM->hasNonPredictedEntities()) {
			break;
		}

		// Try to get a linear function for the access memID
		// This can be the case if
		//  - a static linear function has been extracted at compile time
		//  - the dynamic memory profiling indicates accesses are linear
		const int NumParentLoops = NestInfo->Mem[memID].Parents.size();
		PM->Memory[memID].TubeWidth = 0;
		PM->Memory[memID].LF = std::move(
				PredictionModel::tryBuildLinearFunction(In.getMemEvents(memID),
						NumParentLoops, PM->Memory[memID].PreType));

		// Try to build a tube if not linear
		if (PM->Memory[memID].PreType == PredictionType::NonLinear) {
			// "NonLinear Memory Code Bones (Tube)" starts here!!!
			bool IsTube = true;
			Regression::nonAffineMemoryAnalysis(PM->Memory[memID].LF,
					PM->Memory[memID].TubeWidth, PM->Memory[memID].RC,
					In.getMemEvents(memID), memID, IsTube);

			if (IsTube) {
				traceStep("TUBE WIDTH:--> ", PM->Memory[memID].TubeWidth, -2);
				PM->Memory[memID].PreType = PredictionType::Tube;
			}
		}

		// If not linear nor a tube, then we can't do anything
		if (PM->Memory[memID].PreType == PredictionType::NonLinear
				|| PM->Memory[memID].PreType == PredictionType::NotExecuted) {
			PM->UnpredictedDetected = true;
		}
	}

	// Scalars prediction
	for (int scalarID = 0; scalarID < NumScalars; ++scalarID) {
		if (EarlyStop && PM->hasNonPredictedEntities())
			break;

		// Try to get a linear function
		const int NumParentLoops = NestInfo->Scalars[scalarID].Parents.size();
		PM->Scalars[scalarID].LF = std::move(
				PredictionModel::tryBuildLinearFunction(
						In.getScalarEvents(scalarID), NumParentLoops,
						PM->Scalars[scalarID].PreType, true));

		// If not linear, then we can't do anything for now
		if (PM->Scalars[scalarID].PreType == PredictionType::NonLinear
				|| PM->Scalars[scalarID].PreType
						== PredictionType::ScalarSemiLinear || //Not yet supported by the code generation mechanism
				PM->Scalars[scalarID].PreType == PredictionType::NotExecuted) {
			PM->UnpredictedDetected = true;
		}
	}

	// Bounds prediction
	for (int loopID = 0; loopID < NumLoops; ++loopID) {
		if (EarlyStop && PM->hasNonPredictedEntities())
			break;

		// Try to get a linear function
		const int NumParentLoops = NestInfo->Loops[loopID].depth() - 1;
		PM->Bounds[loopID].LF = std::move(
				PredictionModel::tryBuildLinearFunction(
						In.getBoundEvents(loopID), NumParentLoops,
						PM->Bounds[loopID].PreType));

		// Try to build a tube if not linear
		if (PM->Bounds[loopID].PreType == PredictionType::NonLinear) {
			bool IsTube = false;
			Regression::nonAffineBoundAnalysis(PM->Bounds[loopID].LF,
					PM->Bounds[loopID].Max, PM->Bounds[loopID].RC, In, loopID,
					IsTube);

			if (IsTube) {
				PM->Bounds[loopID].PreType = PredictionType::Tube;
			}
		}

		// If not linear (even if we detected tube, then we can't do anything
		if (PM->Bounds[loopID].PreType == PredictionType::NonLinear
				|| PM->Bounds[loopID].PreType == PredictionType::Tube || //Not yet supported by the code generation mechanism
				PM->Bounds[loopID].PreType == PredictionType::NotExecuted)
			PM->UnpredictedDetected = true;
	}

	arma::set_cerr_stream(Err1);
	arma::set_cerr_stream(err2);
	eventEnd("interpolation");

	const auto &OutermostBoundPrediction = PM->getBoundPrediction(0);
	if (OutermostBoundPrediction.PreType == PredictionType::StaticLinear) {
		long LFConstantCoefficient = OutermostBoundPrediction.LF.back();
		apolloProg->State->registerMaxUpperBound(LFConstantCoefficient);
	}

	// Dump recorded events
	if (backdoorEnabled("info_all")) {
		std::stringstream DumpIn;
		In.dump(DumpIn);
		Info(
				backdoorutils::concat(
						"PredictionModel::build_from_instrumentation ",
						DumpIn.str()));
	}

	// Dump prediction model
	if (backdoorEnabled("info_all") || backdoorEnabled("info")) {
		std::stringstream DumpPM;
		PM->dump(DumpPM);
		Info(
				backdoorutils::concat(
						"PredictionModel::build_from_instrumentation ",
						DumpPM.str()));
	}
	traceExit("PredictionModel::build_from_instrumentation");
	return PM;
}

void PredictionModel::reIndexMemAccesses(const InstrumentationResult &IR,
		ReIndexMap& idxMap) {

	traceEnter(
			backdoorutils::concat("PredictionModel::reIndexMemAccesses() for ",
					IR.getMemEventsCount(), " memory accesses"));
	size_t i = 0;
	StaticNestInfo *NestInfo = apolloProg->StaticInfo.get();

	// Iterates over memory statements
	for (auto MemPred : Memory) {

		// If the statement is nor linear nor a tube
		if (!MemPred.isLinearOrTube()) {

			std::vector<InstrumentationResult::InstrumentationEvent> Events(
					IR.getMemEvents(i).size());

			// Create a new events list with the new indexes
			for (auto event : IR.getMemEvents(i)) {
				long index = idxMap.GetIndex(event.Value);
				InstrumentationResult::InstrumentationEvent ReIndexedEvent =
						event;
				ReIndexedEvent.Value = index;
				Events.push_back(ReIndexedEvent);
//				std::cerr << "Event vis = " << ReIndexedEvent.Coefs[0] << " "
//						<< ReIndexedEvent.Coefs[1] << " "
//						<< ReIndexedEvent.Coefs[2] << " -> " << event.Value
//						<< ":" << ReIndexedEvent.Value << std::endl;
			}

			if (Events.size() == 0) {
				this->Memory[i].TubeWidth = 0;
				this->Memory[i].PreType = PredictionType::NotExecuted;
				this->Memory[i].LF = LinearFunctionTy();
				continue;
			}

			// Check if re indexed events are affine using armadillo
			const int NumParents = NestInfo->Mem[i].Parents.size();
			const auto Solution = std::move(
					solveLinearFunction(Events, NumParents));
			if (Solution.size() > 0) {
				LinearFunctionTy F(NumParents + 1, 0);
				for (int i = 0; i < NumParents; ++i) {
					F[i] = Solution[i];
				}
				F[NumParents] = Solution[NumParents];
				this->Memory[i].TubeWidth = 0;
				this->Memory[i].PreType = PredictionType::DynamicReIndexed;
				this->Memory[i].LF = F;
			}

			// Try to build a tube if not affine
			else {
				bool IsTube = true;
				Regression::nonAffineMemoryAnalysis(this->Memory[i].LF,
						this->Memory[i].TubeWidth, this->Memory[i].RC, Events,
						i, IsTube);
				if (IsTube) {
					this->Memory[i].PreType = PredictionType::TubeReIndexed;
				}
			}
		}
		i++;
	}

	// Dump recorded events
	if (backdoorEnabled("info_all")) {
		std::stringstream DumpIn;
		IR.dump(DumpIn);
		Info(
				backdoorutils::concat(
						"PredictionModel::build_from_instrumentation ",
						DumpIn.str()));
	}

	// Dump prediction model
	if (backdoorEnabled("info_all") || backdoorEnabled("info")) {
		std::stringstream DumpPM;
		this->dump(DumpPM);
		Info(
				backdoorutils::concat(
						"PredictionModel::build_from_instrumentation ",
						DumpPM.str()));
	}

	traceExit("PredictionModel::reIndexMemAccesses()");
}

bool PredictionModel::isStillValid(const InstrumentationResult &IR) {

	StaticNestInfo *NestInfo = apolloProg->StaticInfo.get();
	const int NumLoops = NestInfo->Loops.size();
	const int NumStmts = NestInfo->Mem.size();
	const int NumScalars = NestInfo->Scalars.size();

	// Check memory accesses
	for (int memID = 0; memID < NumStmts; ++memID) {

		// No need to check accesses statically linear
		MemoryPrediction MemPred = Memory[memID];
		if (MemPred.PreType == PredictionType::StaticLinear) {
			continue;
		}

		// Check every event for the current memory access
		for (auto event : IR.getMemEvents(memID)) {

			if (MemPred.isLinear()) {
				long pred = MemPred.LF.back();
				for (size_t ci = 0; ci < MemPred.LF.size() - 1; ci++) {
					pred += MemPred.LF[ci] * event.Coefs[ci];
				}
				if (pred != event.Value) {
					event.dump(std::cerr);
					std::cerr
							<< " MEMORY does not match with previous linear model:\n\t";
					for (size_t ci = 0; ci < MemPred.LF.size(); ci++) {
						std::cerr << MemPred.LF[ci] << " ";
					}
					std::cerr << std::endl;
					return false;
				}
			} else if (MemPred.isTube()) {
				long pred = MemPred.LF.back();
				for (size_t ci = 0; ci < MemPred.LF.size() - 1; ci++) {
					pred += MemPred.LF[ci] * event.Coefs[ci];
				}
				if (event.Value > pred + MemPred.TubeWidth
						|| event.Value < pred - MemPred.TubeWidth) {
					event.dump(std::cerr);
					std::cerr
							<< " MEMORY does not match with previous tube model:\n\t";
					for (size_t ci = 0; ci < MemPred.LF.size(); ci++) {
						std::cerr << MemPred.LF[ci] << " ";
					}
					std::cerr << std::endl;
					return false;
				}
			} else {
				std::cerr << "We should not be there" << std::endl;
				exit(-1);
			}
		}
	}

	// Check scalars
	for (int scalarID = 0; scalarID < NumScalars; ++scalarID) {

		// No need to check scalars statically linear
		ScalarPrediction ScalarPred = Scalars[scalarID];
		if (ScalarPred.PreType == PredictionType::StaticLinear) {
			continue;
		}

		// Check every event for the current scalar
		for (auto event : IR.getScalarEvents(scalarID)) {
			if (ScalarPred.isLinear()) {
				long pred = ScalarPred.LF.back();
				for (size_t ci = 0; ci < ScalarPred.LF.size() - 1; ci++) {
					pred += ScalarPred.LF[ci] * event.Coefs[ci];
				}
				if (pred != event.Value) {
					event.dump(std::cerr);
					std::cerr
							<< " SCALAR does not match with previous linear model:\n\t";
					for (size_t ci = 0; ci < ScalarPred.LF.size(); ci++) {
						std::cerr << ScalarPred.LF[ci] << " ";
					}
					std::cerr << std::endl;
					return false;
				}
			} else {
				std::cerr << "We should not be there" << std::endl;
				exit(-1);
			}
		}
	}

	// Check bounds
	for (int loopID = 0; loopID < NumLoops; ++loopID) {

		// No need to check bounds statically linear
		BoundPrediction BoundsPred = Bounds[loopID];
		if (BoundsPred.PreType == PredictionType::StaticLinear) {
			continue;
		}

		// Check every event for the current bound
		for (auto event : IR.getBoundEvents(loopID)) {
			if (BoundsPred.isLinear()) {
				long pred = BoundsPred.LF.back();
				for (size_t ci = 0; ci < BoundsPred.LF.size() - 1; ci++) {
					pred += BoundsPred.LF[ci] * event.Coefs[ci];
				}
				if (pred != event.Value) {
					event.dump(std::cerr);
					std::cerr
							<< " BOUND does not match with previous linear model:\n\t";
					for (size_t ci = 0; ci < BoundsPred.LF.size(); ci++) {
						std::cerr << BoundsPred.LF[ci] << " ";
					}
					std::cerr << std::endl;
					return false;
				}
			} else if (BoundsPred.isTube()) {
				long pred = BoundsPred.LF.back();
				for (size_t ci = 0; ci < BoundsPred.LF.size() - 1; ci++) {
					pred += BoundsPred.LF[ci] * event.Coefs[ci];
				}
				if (event.Value > pred + BoundsPred.Max
						|| event.Value < pred - BoundsPred.Max) {
					event.dump(std::cerr);
					std::cerr
							<< " does not match with previous linear model:\n\t";
					for (size_t ci = 0; ci < BoundsPred.LF.size(); ci++) {
						std::cerr << BoundsPred.LF[ci] << " ";
					}
					std::cerr << std::endl;
					return false;
				}
			} else {
				std::cerr << "We should not be there" << std::endl;
				exit(-1);
			}
		}
	}

	return true;
}

bool PredictionModel::isStillValid(const InstrumentationResult &IR,
		ReIndexMap& idxMap) {

	StaticNestInfo *NestInfo = apolloProg->StaticInfo.get();
	for (size_t i = 0; i < NestInfo->Mem.size(); i++) {
		MemoryPrediction MemPred = Memory[i];
		if (MemPred.PreType == PredictionType::StaticLinear) {
			continue;
		}
		for (auto event : IR.getMemEvents(i)) {
			if (MemPred.isLinear() || MemPred.isLinearAfterReIndexing()) {
				long pred = MemPred.LF.back();
				for (size_t ci = 0; ci < MemPred.LF.size() - 1; ci++) {
					pred += MemPred.LF[ci] * event.Coefs[ci];
				}
				long value;
				if (MemPred.isLinearAfterReIndexing()) {
					value = idxMap.GetIndex(event.Value);
				} else {
					value = event.Value;
				}
				if (pred != value) {
					event.dump(std::cerr);
					std::cerr
							<< " does not match with previous linear model:\n\t";
					for (size_t ci = 0; ci < MemPred.LF.size(); ci++) {
						std::cerr << MemPred.LF[ci] << " ";
					}
					std::cerr << std::endl;
					return false;
				}
			} else if (MemPred.isTube() || MemPred.isTubeAfterReIndexing()) {
				long pred = MemPred.LF.back();
				for (size_t ci = 0; ci < MemPred.LF.size() - 1; ci++) {
					pred += MemPred.LF[ci] * event.Coefs[ci];
				}
				long value;
				if (MemPred.isTubeAfterReIndexing()) {
					value = idxMap.GetIndex(event.Value);
				} else {
					value = event.Value;
				}
				if (pred > value + MemPred.TubeWidth
						|| pred < value - MemPred.TubeWidth) {
					event.dump(std::cerr);
					std::cerr
							<< " does not match with previous tube model:\n\t";
					for (size_t ci = 0; ci < MemPred.LF.size(); ci++) {
						std::cerr << MemPred.LF[ci] << " ";
					}
					std::cerr << std::endl;
					return false;
				}
			} else {
				std::cerr << "We should not be there" << std::endl;
				exit(-1);
			}
		}
	}
	return true;
}

// used to retrieve the coefficients by the skeletons
extern "C" long get_coefficient(PredictionModel *PM, const long Ty,
		const long Id, const long Coef) {

	switch (Ty) {
	case 0: {
		const auto &MemPred = PM->getMemPrediction(Id);
		// return 'tube' type
		if (Coef == -1)
			return MemPred.PreType;
		// return tube width
		if (Coef == -2)
			return MemPred.TubeWidth;
		const PredictionModel::LinearFunctionTy &LF = MemPred.LF;
		if (Coef < (int) LF.size())
			return LF[Coef];
		return 0;
	}
	case 1:
		return PM->getScalarPrediction(Id).LF[Coef];
	case 2:
		return PM->getBoundPrediction(Id).LF[Coef];
	default:
		return 0;
	}
}

// Memory runtime verification
extern "C" bool memory_runtime_verify(const long StmtId, void *MemAddress,
		const int NumberOfViIt, ...) {
	/*
	 if (!apolloProg->AllocatedMemory.isAllocated(((uintptr_t)MemAddress))) {
	 return false;
	 }
	 */

	const StaticNestInfo *Info = apolloProg->StaticInfo.get();

	if (Info->Mem[StmtId].IsWrite)
		return false;

	const uintptr_t Address = (uintptr_t) MemAddress;
	const auto &MemRanges = apolloProg->ExtremeMemRanges;
	const std::vector<StaticMemInfo *> &WriteInfoList = Info->WriteMem;

	for (StaticMemInfo* WriteInfo : WriteInfoList) {
		const auto &WriteRange = MemRanges[WriteInfo->Id];
		const bool InsideWriteRange = (WriteRange.first <= Address
				&& WriteRange.second >= Address);
		if (InsideWriteRange)
			return false;
	}
	return true;

	/*
	 const std::vector<long> &Vec =
	 apolloProg->PredictionModel->getMemPrediction(StmtId).LF;
	 long rLine = 0;

	 va_list ViArgList;
	 va_start(ViArgList, NumberOfViIt);

	 int i = 0;
	 for (i = 0; i < NumberOfViIt; i++) {
	 const long ViIterator = va_arg(ViArgList, long);
	 rLine += Vec[i] * ViIterator;
	 }
	 rLine += Vec[i];
	 va_end(ViArgList);
	 long Address = (long) MemAddress;

	 return true;
	 */
}
