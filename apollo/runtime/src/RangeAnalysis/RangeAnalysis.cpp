//===--- RangeAnalysis.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "RangeAnalysis/RangeAnalysis.h"
#include "Interpolation/Interpolation.h"
#include "SwitchingState/SwitchingState.h"
#include "APOLLO_PROG.h"
#include "Regression/Regression.h"
#include "Backdoor/Backdoor.h"

#include <complex>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <limits>
#include <ctime>

extern "C" {
#include "polylib/polylib64.h"
#include "polylib/polyparam.h"
}

/// \brief Returns the iteration domain of all the inner most loops in the nest
/// The returned domains are represented using polylib doamins
static std::map<int, Polyhedron *>
getInnermostPolylibDomains(const APOLLO_PROG *apolloProg) {
  const int Lower = 0;
  const int Upper = 1;
  std::map<int, Polyhedron *> Domains;
  for (StaticLoopInfo *loop : apolloProg->StaticInfo->InnermostLoops) {
    const int NumParentLoops = loop->depth();
    // domain: the column arrangement is [1][vi][constant]
    Matrix *DomainMatrix =
        Matrix_Alloc(2 * NumParentLoops, 1 + NumParentLoops + 1);
    for (int loopIdx = 0; loopIdx < NumParentLoops; ++loopIdx) {
      const int LoopId = loop->Parents[loopIdx]->Id;
      DomainMatrix->p[2 * loopIdx + Lower][0] =
          DomainMatrix->p[2 * loopIdx + Upper][0] = 1;
      auto IterLower = &DomainMatrix->p[2 * loopIdx + Lower][1];
      auto IterUpper = &DomainMatrix->p[2 * loopIdx + Upper][1];
      auto ConstLower =
          &DomainMatrix->p[2 * loopIdx + Lower][1 + NumParentLoops];
      auto ConstUpper =
          &DomainMatrix->p[2 * loopIdx + Upper][1 + NumParentLoops];
      IterLower[loopIdx] = 1;
      IterUpper[loopIdx] = -1;

      if (LoopId == 0) {
        *ConstLower = -apolloProg->State->chunkLower();
        *ConstUpper = apolloProg->State->chunkUpper() - 1;
      } else {
        const auto &Prediction =
            apolloProg->PredictionModel->getBoundPrediction(LoopId);
        const bool BoundSolved = Prediction.isLinear();
        if (BoundSolved) {
          const int NumParents = apolloProg->StaticInfo->Loops[LoopId].depth();
          const auto &LinearFunction = Prediction.LF;
          for (int parentIdx = 0; parentIdx < NumParents - 1; ++parentIdx) {
            IterUpper[parentIdx] = LinearFunction[parentIdx];
          }
          *ConstUpper = LinearFunction[NumParents - 1] - 1;
        }
      }
    }
    Polyhedron *Domain =
        Constraints2Polyhedron(DomainMatrix, DomainMatrix->NbRows);
    Domains[loop->Id] = Domain;
    Matrix_Free(DomainMatrix);
  }
  return Domains;
}

static void computeExtremeBounds(APOLLO_PROG *apolloProg,
                                 std::map<int, Polyhedron *> &Domains) {
  traceEnter("compute_extreme_bounds");
  const int NumLoops = apolloProg->StaticInfo->Loops.size();
  long UpperBound[NumLoops];
  long LowerBound[NumLoops];

  // init
  for (int i = 0; i < NumLoops; ++i) {
    LowerBound[i] = std::numeric_limits<long>::max();
    UpperBound[i] = std::numeric_limits<long>::min();
  }

  for (int loop_id = 0; loop_id < NumLoops; ++loop_id) {
    const int InnermostStmtId = 
             apolloProg->StaticInfo->Loops[loop_id].InnermostMemId;
    const auto &InnermostStmtParents =
              apolloProg->StaticInfo->Mem[InnermostStmtId].Parents;
    Polyhedron *OneDomain = Domains[InnermostStmtParents.back()->Id];
    int LoopIdxForDomain = -1;
    for (size_t i = 0; i < InnermostStmtParents.size(); ++i)
      if (InnermostStmtParents[i]->Id == loop_id)
        LoopIdxForDomain = i;
    assert(LoopIdxForDomain != -1);
    for (unsigned vertex = 0; vertex < OneDomain->NbRays; ++vertex) {
      const bool IsVertex = OneDomain->Ray[vertex][0] == 1;
      if (IsVertex) {
        LowerBound[loop_id] = std::min<long>(
            LowerBound[loop_id], OneDomain->Ray[vertex][LoopIdxForDomain + 1]);
        UpperBound[loop_id] = std::max<long>(
            UpperBound[loop_id], OneDomain->Ray[vertex][LoopIdxForDomain + 1]);
      }
    }
  }
  // save in apollo prog
  std::vector<BoundRangeTy> &ExtremeBounds = apolloProg->ExtremeBounds;
  ExtremeBounds = std::vector<BoundRangeTy>(NumLoops, BoundRangeTy(0,0));
  for (int i = 0; i < NumLoops; ++i) {
    ExtremeBounds[i] = BoundRangeTy(LowerBound[i], UpperBound[i]);
  }

  traceExit("compute_extreme_bounds");
}

void computeMaxMinOfMemoryRanges(unsigned int StmtId, 
                                 APOLLO_PROG *apolloProg,
                                 std::map<int, Polyhedron *> &Domains,
                                 const std::vector<long> &ReggresionLine,
                                 long TubeWidth = 0) {

  const StaticNestInfo *NestInfo = apolloProg->StaticInfo.get();
  const auto &StmtInfoObj = NestInfo->Mem[StmtId];
  const int InnermostStmtId = StmtInfoObj.Parents.back()->InnermostMemId;

  const auto &InnermostStmtInfo = NestInfo->Mem[InnermostStmtId];
  Polyhedron *OneDomain = Domains[InnermostStmtInfo.getParentId()];

  int64_t Min = std::numeric_limits<int64_t>::max();
  int64_t Max = std::numeric_limits<int64_t>::min();

  for (unsigned vertexId = 0; vertexId < OneDomain->NbRays; ++vertexId) {
    const bool IsVertex = OneDomain->Ray[vertexId][0] == 1;
    if (IsVertex) {
      const auto Vertex = OneDomain->Ray[vertexId];
      int64_t ThisVertexValue = 0;
      for (size_t loopIdx = 0; 
           loopIdx < StmtInfoObj.Parents.size(); ++loopIdx) {
        ThisVertexValue += ReggresionLine[loopIdx] * Vertex[1 + loopIdx];
      }
      Min = std::min<int64_t>(Min, ThisVertexValue);
      Max = std::max<int64_t>(Max, ThisVertexValue);
    }
  }
  const int64_t ConstantCoef = ReggresionLine.back();

  Min += ConstantCoef - TubeWidth;
  Max += ConstantCoef + (StmtInfoObj.SizeInBytes - 1) + TubeWidth;

  apolloProg->ExtremeMemRanges[StmtId] = MemRangeTy(Min, Max);
}

static void computeExtremeMemoryRanges(APOLLO_PROG *apolloProg,
                                       std::map<int, Polyhedron *> &Domains) {
  traceEnter("compute_extreme_memory_ranges");

  const StaticNestInfo *NestInfo = apolloProg->StaticInfo.get();
  const size_t NumMem = NestInfo->Mem.size();
  apolloProg->ExtremeMemRanges = std::vector<MemRangeTy>(NumMem,MemRangeTy(0,0));

  for (unsigned int memId = 0; memId < NumMem; ++memId) {
    const auto &MemPred = apolloProg->PredictionModel->getMemPrediction(memId);
    if (MemPred.isLinearOrTube()) {
      const auto &LinearFunction = MemPred.LF;
      computeMaxMinOfMemoryRanges(memId, apolloProg, Domains, LinearFunction,
                                  MemPred.TubeWidth);
    }
  }
  traceExit("compute_extreme_memory_ranges");
}

/// \brief Indicates whether or not at least one loop bound of the given
/// program performs linear accesses depending on the value of the outer most
/// iterator
static bool boundDependingOnChunk(const APOLLO_PROG *apolloProg) {
  const int NumLoops = apolloProg->StaticInfo->Loops.size();
  for (int loopId = 1; loopId < NumLoops; ++loopId) {
    const auto &BoundPrediction =
        apolloProg->PredictionModel->getBoundPrediction(loopId);
    const bool IsLinearAndCoefIsNotZero =
        BoundPrediction.isLinear() && BoundPrediction.LF[0] != 0;
    if (IsLinearAndCoefIsNotZero)
      return true;
  }
  return false;
}


/// \brief Indicates whether or not at least one memory statement of the given
/// program performs linear accesses depending on the value of the outer most
/// iterator
static bool memDependingOnChunk(const APOLLO_PROG *apolloProg) {
  const int NumMem = apolloProg->StaticInfo->Mem.size();
  for (int stmtId = 0; stmtId < NumMem; ++stmtId) {
    const auto &MemPred = apolloProg->PredictionModel->getMemPrediction(stmtId);
    const bool IsLinearAndCoefIsNotZero = MemPred.isLinearOrTube() && MemPred.LF[0] != 0;
    if (IsLinearAndCoefIsNotZero)
      return true;
  }
  return false;
}

static bool hasSquareDomain(const int InnerLoopId, 
                            const APOLLO_PROG *apolloProg) {
  const StaticLoopInfo &InnerLoopInfo = apolloProg->StaticInfo->Loops[InnerLoopId];
  for (int loopIdx = 1; loopIdx < InnerLoopInfo.depth(); ++loopIdx) {
    const int LoopId = InnerLoopInfo.Parents[loopIdx]->Id;
    const auto &Prediction =
        apolloProg->PredictionModel->getBoundPrediction(LoopId);
    if (!Prediction.isLinear())
      return false;
    const auto SearchResult =
        std::find_if(++Prediction.LF.rbegin(), Prediction.LF.rend(),
                     [](long c) { return c != 0; });
    const bool NonZeroCoef = SearchResult != Prediction.LF.rend();
    if (NonZeroCoef)
      return false;
  }
  return true;
}

// random number between low and high
static float uniformRandom(const long Low, const long High) {
  const long Size = High - Low;
  const float R = (float)rand() / (float)RAND_MAX;
  return R * Size + Low;
}

static double monteCarloApproximation(const int LoopId, 
                                      const APOLLO_PROG *apolloProg) {
  const StaticLoopInfo &LoopInfo = apolloProg->StaticInfo->Loops[LoopId];
  const int LoopDepth = LoopInfo.depth();
  long Low[LoopDepth];
  long High[LoopDepth];

  for (int loopIdx = 0; loopIdx < LoopDepth; ++loopIdx) {
    Low[loopIdx] =
        apolloProg->ExtremeBounds[LoopInfo.Parents[loopIdx]->Id].first;
    High[loopIdx] =
        apolloProg->ExtremeBounds[LoopInfo.Parents[loopIdx]->Id].second;
  }

  const int MaxAttempts = 20 * LoopDepth;
  const float TimeOutInSec = 0.0005;
  size_t TotalPoints = 0;
  size_t PointsInside = 0;
  std::clock_t Start = std::clock();
  do {
    for (int i = 0; i < MaxAttempts; ++i) {
      // the position at 0 is dummy, this value is always in the bounds
      float Point[LoopDepth];
      // get a point
      for (int j = 0; j < LoopDepth; ++j) {
        Point[j] = uniformRandom(Low[j], High[j]);
      }
      // check if it is inside the domain
      bool Inside = true;
      for (int loopIdx = 1; loopIdx < LoopDepth && Inside; ++loopIdx) {
        const auto &BoundPrediction =
            apolloProg->PredictionModel->getBoundPrediction(
                LoopInfo.Parents[loopIdx]->Id);
        float UpperBound = BoundPrediction.LF[loopIdx];
        for (int i = 0; i < loopIdx; ++i) {
          UpperBound += BoundPrediction.LF[i] * Point[i];
        }
        Inside = Inside && Point[loopIdx] <= UpperBound;
      }
      PointsInside += Inside ? 1 : 0;
      TotalPoints += 1;
    }
  } while (((std::clock() - Start) / (double)CLOCKS_PER_SEC) < TimeOutInSec);

  const double Probability = ((double)PointsInside / (double)TotalPoints);
  return Probability;
}

void approximateNumberOfIterations(APOLLO_PROG *apolloProg,
                                   std::map<int, Polyhedron *> &Domains) {

  apolloProg->ChunkNumberOfIterations = 0.0;
  // the extreme bounds set a bounding box for the iteration domain.
  const auto &BoxBounds = apolloProg->ExtremeBounds;
  double TotalPoints = 0.0;
  for (const std::pair<int, Polyhedron *> &dom : Domains) {
    const int LoopId = dom.first;
    const StaticLoopInfo &LoopInfo = apolloProg->StaticInfo->Loops[LoopId];
    double BoundingBoxArea = 1.0;
    for (size_t loopIdx = 0; loopIdx < LoopInfo.Parents.size(); ++loopIdx) {
      const auto &Bound = BoxBounds.at(LoopInfo.Parents[loopIdx]->Id);
      BoundingBoxArea *= (Bound.second - Bound.first);
    }
    const bool DomIsSquare = hasSquareDomain(LoopId, apolloProg);
    double Probability = 0.0;
    if (DomIsSquare) {
      // if the domain is square the bounding box is equal to the domain
      Probability = 1.0;
    } else {
      // obtain a montecarlo approximation
      Probability = monteCarloApproximation(LoopId, apolloProg);
    }
    TotalPoints += BoundingBoxArea * Probability;
  }
  apolloProg->ChunkNumberOfIterations = TotalPoints;
}

static void logExtremes(const APOLLO_PROG *apolloProg) {
  if (backdoorEnabled("info")) {
    const int NumMem = apolloProg->StaticInfo->Mem.size();
    const int NumLoops = apolloProg->StaticInfo->Loops.size();
    std::stringstream Bounds;
    std::stringstream Mem;
    Bounds << "ExtremeBounds [\n";
    for (int loopId = 0; loopId < NumLoops; ++loopId) {
      Bounds << "\tloop[" << loopId << "] extremes = <"
             << apolloProg->ExtremeBounds[loopId].first << " to "
             << apolloProg->ExtremeBounds[loopId].second << ">\n";
    }
    Bounds << "]";
    Info(backdoorutils::concat("compute_extreme_bounds_and_memory_ranges ",
                               Bounds.str()));
    Mem << "ExtremeMemory [\n";
    for (int mem_id = 0; mem_id < NumMem; ++mem_id) {
      std::string Type(apolloProg->StaticInfo->Mem[mem_id].IsWrite ? "write"
                                                                   : "read");
      Mem << "\t" << Type << "[" << mem_id << "] extremes = <"
          << apolloProg->ExtremeMemRanges[mem_id].first << " to "
          << apolloProg->ExtremeMemRanges[mem_id].second << ">\n";
    }
    Mem << "]";
    Info(backdoorutils::concat("compute_extreme_bounds_and_memory_ranges ",
                               Mem.str()));
  }
}

bool computeExtremeBoundsAndMemoryRanges(APOLLO_PROG *apolloProg) {

  traceEnter("compute_extreme_bounds_and_memory_ranges");
  eventStart("extremes_computation");
  std::map<int, Polyhedron *> Domains =
      std::move(getInnermostPolylibDomains(apolloProg));
  const bool AlreadyComputed = !apolloProg->ExtremeBounds.empty() &&
                               !apolloProg->ExtremeMemRanges.empty();
  const bool ReuseExtremes = AlreadyComputed && !memDependingOnChunk(apolloProg) &&
                             !boundDependingOnChunk(apolloProg);
  if (ReuseExtremes) {
    // only update the outermost loop bounds, the rest remain the same.
    apolloProg->ExtremeBounds[0].first = apolloProg->State->chunkLower();
    apolloProg->ExtremeBounds[0].second = apolloProg->State->chunkUpper() - 1;
  } else {
    apolloProg->ExtremeBounds.clear();
    apolloProg->ExtremeMemRanges.clear();
    computeExtremeBounds(apolloProg, Domains);
    computeExtremeMemoryRanges(apolloProg, Domains);
  }

  approximateNumberOfIterations(apolloProg, Domains);
  for (auto &id_domain : Domains) {
    Polyhedron_Free(id_domain.second);
  }
  bool ValidExtremes = true;
  if (!ReuseExtremes) {
    auto InvalidRange = [](
      const BoundRangeTy &p) -> bool {
      return !(p.first <= p.second);
    };
    ValidExtremes =
        ValidExtremes &&
        std::find_if(apolloProg->ExtremeBounds.begin(),
                     apolloProg->ExtremeBounds.end(),
                     InvalidRange) == apolloProg->ExtremeBounds.end();
    auto InvalidMemRange = [](const MemRangeTy &p) -> bool {
      return !(p.first < p.second);
    };
    ValidExtremes =
        ValidExtremes &&
        std::find_if(apolloProg->ExtremeMemRanges.begin(),
                     apolloProg->ExtremeMemRanges.end(),
                     InvalidMemRange) == apolloProg->ExtremeMemRanges.end();
  }
  if (!ReuseExtremes)
    logExtremes(apolloProg);

  eventEnd("extremes_computation");
  traceExit(backdoorutils::concat("computeExtremeBoundsAndMemoryRanges = ",
                                  (ValidExtremes) ? "true" : "false"));
  return ValidExtremes;
}

/// \brief Populate the minimum and maximum value of each iterator
/// \param apolloProg : The apollo program
std::vector<BoundRangeTy> getMinMaxIterator(const APOLLO_PROG *apolloProg) {
  // for the outermost iterator the bounds are the bounds of the chuck
  const auto ExtMap = apolloProg->ExtremeBounds;
  return ExtMap;
}

/// \brief Populate the minimum and maximum value of each iterator
/// \param apolloProg : The apollo program
/// \param OuterIteratorMin : the lower bound of the outermost iterator
/// \param OuterIteratorMax : the upper bound of the outermost iterator
std::vector<BoundRangeTy> getMinMaxIterator(const APOLLO_PROG *apolloProg,
                                            const long OuterIteratorMin,
                                            const long OuterIteratorMax) {

  const size_t NumLoops = apolloProg->StaticInfo->Loops.size();
  std::vector<BoundRangeTy> IterBounds(NumLoops, BoundRangeTy(0,0));
  IterBounds[0] = std::make_pair(OuterIteratorMin, OuterIteratorMax);

  // for each iterator
  for (unsigned int loopId = 1; loopId < NumLoops; loopId++) {
    const int64_t Max = getMaxIterator(apolloProg, IterBounds, loopId);

    // for all iterators other than the outer most the lower bound is zero
    IterBounds[loopId] = BoundRangeTy(0, std::max<int64_t>(Max - 1, 0));
  }
  return IterBounds;
}

/// \brief Calculate the upper bound of a given iterator
/// \param apolloProg : The apollo program
/// \param iterbounds : Map containing the upper and lower bounds of the parents
///        iterators (at minimum)
/// \param LoopId : The id of the iterator
int64_t getMaxIterator(const APOLLO_PROG *apolloProg,
                       const std::vector<BoundRangeTy> &IterBounds, 
                       const int LoopId) {
  int64_t Max = 0;
  const auto &BoundPred =
      apolloProg->PredictionModel->getBoundPrediction(LoopId);
  const bool BoundSolved = BoundPred.isLinear();

  if (BoundSolved) {
    const auto &LinearFunForIterI = BoundPred.LF;
    // now go over the linear function of the iterator and solve
    const auto &ParentLoops = apolloProg->StaticInfo->Loops[LoopId].Parents;
    const int NumParents = ParentLoops.size() - 1;
    for (int parentIdx = 0; parentIdx < NumParents; parentIdx++) {
      const int64_t Parent = ParentLoops[parentIdx]->Id;

      const auto &ParentBounds = IterBounds[Parent];
      const auto ParentCoef = LinearFunForIterI[parentIdx];
      // if the coefficient is positive, multiply by the upper bounds,
      // if negative by the lower bound.
      if (ParentCoef >= 0)
        Max += ParentBounds.second * ParentCoef;
      else
        Max += ParentBounds.first * ParentCoef;
    }
    // for the constant
    Max += LinearFunForIterI[NumParents];
  }
  return Max;
}
