//===--- BoneLoop.cpp -----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "CodeBones/BonesInfo/BoneAst.h"
#include "CodeBones/BonesInfo/BoneComposedStmt.h"
#include "CodeBones/BonesInfo/BoneLoop.h"
#include "CodeBones/BonesInfo/BoneStmt.h"

using namespace std::placeholders;

namespace apollo {
namespace codebones {

BoneLoop::BoneLoop(int IdVal) {
  Id = IdVal;
}

BoneLoop::~BoneLoop() {
  for (auto child : Children) {
    delete child;
  }
}

BoneLoop &BoneLoop::addChild(BoneAst *Child, BoneAst *InsertBefore) {
  auto InsertAt = std::find(Children.begin(), Children.end(), InsertBefore);
  Children.insert(InsertAt, Child);
  Child->setParent(this);
  return *this;
}

BoneLoop &BoneLoop::removeChild(BoneAst *Child) {
  auto ChildIterator = std::find(Children.begin(), Children.end(), Child);
  assert(ChildIterator != Children.end());
  Children.erase(ChildIterator);
  Child->setParent(nullptr);
  return *this;
}

bool BoneLoop::isLoop() const {
  return true;
}

int BoneLoop::getId() const {
  return Id;
}

std::vector<BoneAst *> BoneLoop::children() const {
  return Children;
}

std::vector<BoneLoop *> BoneLoop::loops() {
  std::vector<BoneLoop *> Result = {this};
  for (auto child : this->children()) {
    if (child->isLoop()) {
      auto ChildLoops = child->loops();
      Result.insert(Result.end(), ChildLoops.begin(), ChildLoops.end());
    }
  }
  return Result;
}

std::vector<BoneStmt *> BoneLoop::stmts() {
  std::vector<BoneStmt *> Result;
  for (auto child : this->children()) {
    std::vector<BoneStmt *> ChildStmts = child->stmts();
    Result.insert(Result.end(), ChildStmts.begin(), ChildStmts.end());
  }
  return Result;
}

/// \brief Creates a BoneAst representing the loop-nest as
/// initially specified in the code.
/// The returned BoneLoop is the outer most loop of the nest
/// in the given APOLLO_PROG. The returned AST contains
/// only loops and not yet any statement
std::unique_ptr<BoneLoop> BoneLoop::createLoopTree(APOLLO_PROG *apolloProg) {
  const int NumLoops = apolloProg->StaticInfo->Loops.size();
  std::vector<BoneLoop *> Loops(NumLoops, nullptr);
  for (int id = NumLoops - 1; id >= 0; --id) {
    Loops[id] = new BoneLoop(id);
    for (StaticLoopInfo *child_loop :
         apolloProg->StaticInfo->Loops[id].Children) {
      Loops[id]->addChild(Loops[child_loop->Id]);
    }
  }
  return std::unique_ptr<BoneLoop>(Loops.front());
}

static int getBoneParentLoopId(BoneStmt *Stmt, APOLLO_PROG *apolloProg) {
  int Loop = -1;
  const StaticNestInfo *NestInfo = apolloProg->StaticInfo.get();
  if (Stmt->accessMemory()) {
    const int LastMemId = Stmt->lastMemoryAccess();
    Loop = std::max<int>(NestInfo->Mem[LastMemId].getParentId(), Loop);
    if (!Stmt->isVerification())
      return Loop;
  }

  const auto &MemoryVerif = Stmt->memVerif();
  if (!MemoryVerif.empty())
    Loop = std::max<int>(NestInfo->Mem[MemoryVerif.back()].getParentId(), Loop);
  const auto &BoundVerif = Stmt->boundVerif();
  if (!BoundVerif.empty())
    Loop = std::max<int>(Stmt->boundVerif().front(), Loop);
  const auto &ScalarVerif = Stmt->scalarVerif();
  if (!ScalarVerif.empty()) {
    const auto &Parents = NestInfo->Scalars[ScalarVerif.back()].Parents;
    int ParentLoop = 0;
    if (Stmt->verifiesScalarInitialziation())
      ParentLoop = Parents[Parents.size() - 2]->Id;
    else
      ParentLoop = Parents[Parents.size() - 1]->Id;
    Loop = std::max<int>(ParentLoop, Loop);
  }
  return Loop;
}

static BoneLoop *getLoopWithId(BoneLoop *Loop, int LoopId) {
  if (Loop->getId() == LoopId)
    return Loop;
  auto Children = Loop->children();
  // find the first loop with id lower than the one it's being searched.
  // (it will be a parent loop)
  auto ChildLoop = std::find_if(
      Children.rbegin(), Children.rend(), [LoopId](BoneAst *child) -> bool {
        if (!child->isLoop())
          return false;
        return static_cast<BoneLoop *>(child)->getId() <= LoopId;
      });
  assert(ChildLoop != Children.rend());
  return getLoopWithId(static_cast<BoneLoop *>(*ChildLoop), LoopId);
}

static int getLoopFirstMemAccessId(int LoopId, APOLLO_PROG *apolloProg) {
  const StaticLoopInfo &LoopInfo = apolloProg->StaticInfo->Loops[LoopId];
  const int LoopDepth = LoopInfo.depth();
  for (StaticMemInfo &mem_info : apolloProg->StaticInfo->Mem) {
    const int MemParentLoopId = mem_info.getParentId();
    const int MemDepth = mem_info.Parents.size();
    if (LoopDepth <= MemDepth && LoopId <= MemParentLoopId) {
      if (mem_info.Parents[LoopDepth - 1]->Id == LoopId)
        return mem_info.Id;
    }
  }
  std::cerr << "error, statement not found for loop_id = " << LoopId << ".\n";
  assert(false);
  return -1;
}

/// \brief Returns true if Stmt executes before Node
static bool boneExecutesBefore(BoneAst *Node, BoneStmt *Stmt,
                               APOLLO_PROG *apolloProg) {

  // returns true if stmt doesn't access memory at all
  // i.e, it can be executed before any other statements
  if (!Stmt->accessMemory())
    return true;

  // If Node is a loop, get the first statement of the loop,
  // and check if it is smaller.
  if (Node->isLoop()) {
    BoneLoop *OtherLoop = static_cast<BoneLoop *>(Node);
    int FirstLoopMemAccess =
        getLoopFirstMemAccessId(OtherLoop->getId(), apolloProg);
    return Stmt->lastMemoryAccess() < FirstLoopMemAccess;
  }

  else {
    BoneStmt *OtherStmt = static_cast<BoneStmt *>(Node);
    if (!OtherStmt->accessMemory())
      return false;
    if (Stmt->lastMemoryAccess() == OtherStmt->lastMemoryAccess()) {
      const bool BothVerifyMemory = !Stmt->memVerif().empty() &&
                                    !OtherStmt->memVerif().empty();
      if (BothVerifyMemory)
        return Stmt->memVerif().front() < Stmt->memVerif().front();
      const bool BothVerifyBounds = !Stmt->boundVerif().empty() &&
                                    !OtherStmt->boundVerif().empty();
      if (BothVerifyBounds)
        return Stmt->boundVerif().front() <
               OtherStmt->boundVerif().front();
      const bool BothVerifyScalars = !Stmt->scalarVerif().empty() &&
                                     !OtherStmt->scalarVerif().empty();
      if (BothVerifyScalars)
        return Stmt->boundVerif().front() < OtherStmt->boundVerif().front();

      // the one that verifies scalars is first
      if (!Stmt->scalarVerif().empty())
        return true;
      else if (!OtherStmt->scalarVerif().empty())
        return false;
      // then the one that verifies bounds
      return !Stmt->boundVerif().empty();
    }
    return Stmt->lastMemoryAccess() < OtherStmt->lastMemoryAccess();
  }
}

/// \brief Insert the given bone statement at the right place
/// in this loop nest
void BoneLoop::insertStmtBone(std::unique_ptr<BoneStmt> StmtPtr,
                     APOLLO_PROG *apolloProg) {
  assert(StmtPtr);
  assert(!StmtPtr->isComposed());

  // Get the loop containing the statament
  BoneStmt *Stmt = StmtPtr.release();
  const int ParentLoopId = getBoneParentLoopId(Stmt, apolloProg);
  BoneLoop *ParentLoop = getLoopWithId(this, ParentLoopId);

  // Insert the statement in the loop at the right place
  // according to parent loop and id in parent loop
  if (ParentLoop->children().empty()) {
    ParentLoop->addChild(Stmt);
  }
  else {
    auto Children = ParentLoop->children();
    auto InsertAt =
        std::find_if(Children.begin(), Children.end(),
                     std::bind(boneExecutesBefore, _1, Stmt, apolloProg));
    BoneAst *InsertBefore = nullptr;
    if (InsertAt != Children.end()) {
      InsertBefore = *InsertAt;
    }
    ParentLoop->addChild(Stmt, InsertBefore);
  }
}

/// \brief Insert the given tube memory verification bone
/// statement at the right place in this loop nest
void BoneLoop::insertStmtBoneTube(std::unique_ptr<BoneStmt> StmtPtr,
                                  APOLLO_PROG *apolloProg) {

  std::vector<BoneStmt *> Statements = std::move(this->stmts());
  for (auto access_it = Statements.begin(), E = Statements.end();
       access_it != E; ++access_it) {
    const std::vector<int> MemAccessVector =
                          std::move((*access_it)->allAccesses());
    const int MemVerif = StmtPtr->memVerif().front();
    const bool FindVal = std::binary_search(MemAccessVector.begin(),
                                      MemAccessVector.end(), MemVerif);
    if (FindVal) {
      BoneStmt *Ptr = new BoneStmt(*StmtPtr);
      BoneComposedStmt *Comp = new BoneComposedStmt(
          Ptr, (*access_it), BoneComposedStmt::CompositionType::Guarded);
      BoneLoop *Parent = (*access_it)->getParent();
      Parent->addChild(Comp, (*access_it));
      Parent->removeChild((*access_it));
    }
  }
}

} // end namespace codebones
} // end namespace apollo
