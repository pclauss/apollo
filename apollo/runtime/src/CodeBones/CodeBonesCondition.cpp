//===--- CodeBonesCondition.cpp -------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "APOLLO_PROG.h"
#include "CodeBones/BonesInfo/BoneAst.h"
#include "CodeBones/BonesInfo/BoneComposedStmt.h"
#include "CodeBones/BonesInfo/BoneLoop.h"
#include "CodeBones/BonesInfo/BoneStmt.h"
#include "CodeBones/CodeBones.h"
#include "CodeBones/BonesToScop/BoneToAccess.h"
#include "CodeBones/BonesToScop/BoneToBody.h"
#include "CodeBones/BonesToScop/BoneToDomain.h"
#include "CodeBones/BonesToScop/BoneToScattering.h"
#include "CodeBones/ArrayInfo.h"
#include "StaticInfo/StaticInfo.h"
#include "Backdoor/Backdoor.h"

#define OSL_PRECISION OSL_PRECISION_DP

#include "osl/scop.h"
#include "osl/statement.h"
#include "osl/body.h"
#include "osl/relation.h"
#include "osl/macros.h"
#include "osl/int.h"

#include "osl/extensions/arrays.h"
#include "osl/extensions/scatnames.h"

extern "C" {
#include "candl/candl.h"
#include "candl/scop.h"
#include "candl/dependence.h"
#include "candl/options.h"
}

#include <cstring>

using apollo::codebones::BoneComposedStmt;
using apollo::codebones::BoneStmt;
using apollo::codebones::BoneLoop;
using apollo::codebones::BoneAst;

static BoneStmt *buildOrignalMem(StaticMemInfo *StaticStmt) {
  apollo::codebones::IdVector Loads;
  apollo::codebones::IdVector Stores;
  const int Id = StaticStmt->Id;
  std::string StmtName = "";
  if (StaticStmt->IsWrite) {
    Stores.push_back(Id);
    StmtName = backdoorutils::concat("store_", Id);
  } else {
    Loads.push_back(Id);
    StmtName = backdoorutils::concat("load_", Id);
  }
  BoneStmt *Stmt = new BoneStmt(StmtName, Loads, Stores);
  return Stmt;
}

/// \brief Builds a BoneLoop AST representing the original
/// loop nest such as described by the two parameters.
static BoneLoop *buildOrignalLoop(StaticLoopInfo *StaticLoop,
                                  StaticNestInfo *NestInfo) {

  BoneLoop *Loop = new BoneLoop(StaticLoop->Id);
  // add the child loops
  for (StaticLoopInfo *static_child_loop : StaticLoop->Children) {
    BoneLoop *ChildLoop = buildOrignalLoop(static_child_loop, NestInfo);
    Loop->addChild(ChildLoop);
  }
  // add all the immediate child mem accesses.
  for (StaticMemInfo *mem : StaticLoop->Stmts) {
    BoneStmt *Stmt = buildOrignalMem(mem);
    std::vector<BoneAst *> Children = Loop->children();
    BoneAst *InsertBefore = nullptr;
    auto SearchForPosition =
        std::find_if(Children.begin(), Children.end(),
                     [mem, NestInfo](BoneAst *node) -> bool {
                       if (node->isLoop()) {
                         BoneLoop *AsLoop = (BoneLoop *)node;
                         const int LoopId = AsLoop->getId();
                         const int MemInside = 
                                   NestInfo->Loops[LoopId].InnermostMemId;
                         return mem->Id < MemInside;
                       } else {
                         BoneStmt *AsStmt = (BoneStmt *)node;
                         return mem->Id < AsStmt->allAccesses()[0];
                       }
                     });

    if (SearchForPosition != Children.end()) {
      InsertBefore = *SearchForPosition;
    }
    Loop->addChild(Stmt, InsertBefore);
  }
  return Loop;
}

static osl_scop_p buildScopForOriginalLoop(BoneLoop *Loop,
                                           ArrayAnalysis &MemToArray,
                                           APOLLO_PROG *apolloProg) {

  auto AllStmtsVector = Loop->stmts();
  std::set<BoneStmt *> AllStmts;
  AllStmts.insert(AllStmtsVector.begin(), AllStmtsVector.end());

  osl_scop_p Scop = osl_scop_malloc();

  char *Language = (char *)malloc(sizeof(char) * 2);
  Language[0] = 'C';
  Language[1] = 0;

  Scop->language = Language;
  Scop->version = 1;
  Scop->context = buildContext(apolloProg);

  BoneToDomain DomainBuilder(apolloProg, MemToArray, AllStmts, false);
  BoneToDomain::RetTy Domains = DomainBuilder.visit(Loop, 0);
  BoneToScattering ScatterBuilder(apolloProg, AllStmts);
  BoneToScattering::RetTy Scatterings = ScatterBuilder.visit(Loop, {0});
  BoneToAccess AccessBuilder(apolloProg, MemToArray, AllStmts);
  BoneToAccess::RetTy Accesses = AccessBuilder.visit(Loop, 0);

  osl_generic_p Arrays = AccessBuilder.buildArrays();
  osl_generic_add(&Scop->extension, Arrays);

  BoneToBody BodyBuilder(apolloProg, AllStmts);
  BoneToBody::RetTy Body = BodyBuilder.visit(Loop, 0);

  for (BoneStmt *a_stmt : AllStmtsVector) {
    osl_statement_p Stmt = osl_statement_malloc();
    Stmt->domain = Domains[a_stmt];
    Stmt->scattering = Scatterings[a_stmt];
    Stmt->access = Accesses[a_stmt];
    Stmt->extension = Body[a_stmt];
    osl_statement_add(&Scop->statement, Stmt);
  }

  // names for the parameters.
  osl_strings_p ParametersNames = osl_strings_malloc();
  free(ParametersNames->string);

  ParametersNames->string = (char **)malloc(sizeof(char *) * 3);
  ParametersNames->string[0] = strdup("chunk_lower");
  ParametersNames->string[1] = strdup("chunk_upper");
  ParametersNames->string[2] = 0;

  osl_generic_p Parameters = osl_generic_malloc();
  Parameters->interface = osl_strings_interface();
  Parameters->data = ParametersNames;

  osl_generic_add(&Scop->parameters, Parameters);
  return Scop;
}

static bool memUsedInBone(const int MemId, BoneStmt *Stmt) {
  const auto BoneMem = std::move(Stmt->allAccesses());
  const bool InBone = std::find(BoneMem.begin(), BoneMem.end(), MemId) != 
                                BoneMem.end();
  return InBone;
}

static bool sourceDomainChange(BoneStmt *Source, BoneLoop *CodeBonesNest) {
  assert(Source->loads().size() == 1 && Source->allAccesses().size() == 1);
  const int SourceId = Source->loads().front();
  const int SourceParentId = Source->getParent()->getId();

  for (BoneStmt *stmt : CodeBonesNest->stmts()) {
    if (memUsedInBone(SourceId, stmt)) {
      if (stmt->getParent()->getId() != SourceParentId)
        return true;
    }
  }
  return false;
}

static bool isIntraIterationDependency(osl_dependence_p &Dep) {
  // This is a shortcut. If the 'store' is executed before the 'load',
  // trivially the WAR dependency is intra-iteration.
  if (Dep->label_target < Dep->label_source)
    return false;
  // TODO: To implement a rigorous check. Still for most of the cases the
  // current check is more than enough.
  return true;
}

static bool executionOrderViolation(BoneStmt *Source, BoneStmt *Target,
                                    BoneLoop *CodeBonesNest) {
  assert(Source->loads().size() == 1 && Source->allAccesses().size() == 1);
  assert(Target->stores().size() == 1 && Target->allAccesses().size() == 1);

  const int SourceId = Source->loads().front();
  const int TargetId = Target->stores().front();
  bool BoneNestChanged = false;
  do {
    BoneNestChanged = false;
    auto AllStmts = std::move(CodeBonesNest->stmts());
    const int AllStmtsSize = AllStmts.size();
    int LastSourceUseIdx, FirstTargetUseIdx;

    for (LastSourceUseIdx = AllStmtsSize - 1; LastSourceUseIdx >= 0;
         --LastSourceUseIdx) {
      if (memUsedInBone(SourceId, AllStmts[LastSourceUseIdx]))
        break;
    }
    for (FirstTargetUseIdx = 0; FirstTargetUseIdx < AllStmtsSize;
         ++FirstTargetUseIdx) {
      if (memUsedInBone(TargetId, AllStmts[FirstTargetUseIdx]))
        break;
    }

    if (FirstTargetUseIdx == AllStmtsSize || LastSourceUseIdx == -1)
      return false;
    if (LastSourceUseIdx <= FirstTargetUseIdx)
      return false;
    // if they are contiguous, I can compose them to save the dependency!
    bool CanCompose = BoneComposedStmt::canCompose(AllStmts[LastSourceUseIdx],
                                                   AllStmts[FirstTargetUseIdx]);
    if (CanCompose) {
      Info(backdoorutils::concat("execution_order_violation() bones ",
                                 AllStmts[FirstTargetUseIdx]->name(), " and ",
                                 AllStmts[LastSourceUseIdx]->name(),
                                 " composed."));
      BoneComposedStmt::compose(AllStmts[LastSourceUseIdx],
                                AllStmts[FirstTargetUseIdx],
                                BoneComposedStmt::CompositionType::Concurrent);
      BoneNestChanged = true;
    }

  } while (BoneNestChanged);
  return true;
}

static bool validDependency(BoneStmt *Source, BoneStmt *Target,
                            BoneLoop *CodeBonesNest, osl_dependence_p &Dep) {
  traceEnter(backdoorutils::concat("valid_dependency(", Source->name(), " -> ",
                                   Target->name(), ")"));

  if (isIntraIterationDependency(Dep)) {
    if (sourceDomainChange(Source, CodeBonesNest)) {
      traceExit("valid_dependency() = violation(source domain changed)");
      return false;
    }
    if (executionOrderViolation(Source, Target, CodeBonesNest)) {
      traceExit("valid_dependency() = violation(execution order)");
      return false;
    }
  }
  traceExit("valid_dependency() = success");
  return true;
}

/// \brief Checks whether or not the reconstructed loop nest (this->OutermostLoop)
/// is valid according to the original loop nest. In other words, it checks
/// if dependencies in the new loop nest are not broken compare to the ones
/// in the initial loop
bool apollo::CodeBoneManager::codeBonesNestValidDependencies(
                                                       ArrayAnalysis &MemInfo) {
  traceEnter("codeBoneManager::codeBonesNestValidDependencies()");
  // get the scop
  std::unique_ptr<BoneLoop> OriginalLoop(buildOrignalLoop(
      &apolloProg->StaticInfo->Loops.front(), apolloProg->StaticInfo.get()));

  if (backdoorEnabled("info")) {
    std::stringstream OS;
    codebones::BoneAstDump V;
    OS << "codeBoneManager::codeBonesNestValidDependencies initial nests [\n";
    V.visit(OriginalLoop.get(), OS);
    V.visit(this->OutermostLoop.get(), OS);
    OS << "]\n";
    Info(OS.str());
  }

  osl_scop_p Scop =
      buildScopForOriginalLoop(OriginalLoop.get(), MemInfo, apolloProg);

  // candl to compute only write-after-read
  candl_options_p CandlOpt = candl_options_malloc();
  CandlOpt->verbose = CandlOpt->waw = CandlOpt->raw = CandlOpt->rar = false;
  CandlOpt->war = true;

  // dependence analysis.
  candl_scop_usr_init(Scop);
  osl_dependence_p CandlDeps = candl_dependence(Scop, CandlOpt);

  auto OriginalAccesses = std::move(OriginalLoop->stmts());

  // iterate over the antidependencies and see if any is invalidated
  for (osl_dependence_p dep = CandlDeps; dep != nullptr; dep = dep->next) {
    BoneStmt *Source = OriginalAccesses[dep->label_source];
    BoneStmt *target = OriginalAccesses[dep->label_target];
    if (!validDependency(Source, target, this->OutermostLoop.get(), dep)) {
      candl_options_free(CandlOpt);
      osl_dependence_free(CandlDeps);
      candl_scop_usr_cleanup(Scop);
      osl_scop_free(Scop);
      traceExit("codeBonesNestValidDependencies() = violation");
      return false;
    }
  }
  candl_options_free(CandlOpt);
  osl_dependence_free(CandlDeps);
  candl_scop_usr_cleanup(Scop);
  osl_scop_free(Scop);
  traceExit("codeBonesNestValidDependencies() = success");
  return true;
}
