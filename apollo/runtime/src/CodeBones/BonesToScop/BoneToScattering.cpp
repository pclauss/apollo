//===--- BoneToScattering.h -----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "CodeBones/BonesToScop/BoneToScattering.h"
#include "CodeBones/BonesToScop/BuildContext.h"
#include "Interpolation/Interpolation.h"

using apollo::codebones::BoneAst;
using apollo::codebones::BoneLoop;
using apollo::codebones::BoneStmt;

BoneToScattering::BoneToScattering(APOLLO_PROG *apolloProg_,
                                   std::set<BoneStmt *> &RestrictTo_)
  : RestrictTo(RestrictTo_) {
  apolloProg = apolloProg_;
}

BoneToScattering::RetTy BoneToScattering::visitStmt(BoneStmt *Stmt,
                                                    std::vector<int> Path) {

  if (!RestrictTo.count(Stmt))
    return RetTy();
  const int ParentLoopId = Stmt->getParent()->getId();
  const StaticLoopInfo &LoopInfo = apolloProg->StaticInfo->Loops[ParentLoopId];
  const int NumIterators = LoopInfo.depth();
  assert((int)Path.size() == NumIterators + 1);
  osl_relation_p Scattering =
      osl_relation_pmalloc(OSL_PRECISION, 2 * NumIterators + 1,
                           2 + 3 * NumIterators + 1 + NumParams);
  osl_relation_set_attributes(Scattering, 2 * NumIterators + 1, NumIterators, 0,
                              2);
  osl_relation_set_type(Scattering, OSL_TYPE_SCATTERING);

  for (int i = 0; i < Scattering->nb_output_dims; ++i) {
    osl_int_set_si(OSL_PRECISION, &Scattering->m[i][1 + i], -1);
  }
  // loop dim
  for (int i = 0; i < NumIterators; ++i) {
    osl_int_set_si(
        OSL_PRECISION,
        &Scattering->m[2 * i + 1][1 + Scattering->nb_output_dims + i], 1);
  }
  // constant dim
  for (int i = 0; i < NumIterators + 1; ++i) {
    osl_int_set_si(OSL_PRECISION,
                   &Scattering->m[2 * i][Scattering->nb_columns - 1], Path[i]);
  }
  RetTy StmtSca;
  StmtSca[Stmt] = Scattering;
  return StmtSca;
}

BoneToScattering::RetTy BoneToScattering::visitLoop(BoneLoop *Loop,
                                                    std::vector<int> Path) {
  auto Children = Loop->children();
  RetTy StmtsToScattering;
  for (unsigned child_position = 0; child_position < Children.size();
       ++child_position) {
    BoneAst *Child = Children[child_position];
    auto ChildPath = Path;
    ChildPath.push_back(child_position);
    const RetTy ChildStmts = this->visit(Child, ChildPath);
    for (auto kv : ChildStmts) {
      StmtsToScattering[kv.first] = kv.second;
    }
  }
  return StmtsToScattering;
}

