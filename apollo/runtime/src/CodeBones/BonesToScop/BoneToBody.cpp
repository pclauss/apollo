//===--- BoneToBody.cpp ---------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "CodeBones/BonesToScop/BoneToBody.h"
#include "CodeBones/BonesToScop/BuildContext.h"
#include "Interpolation/Interpolation.h"
#include "RuntimeConfig.h"

#include <string>

using apollo::codebones::BoneLoop;
using apollo::codebones::BoneStmt;

BoneToBody::BoneToBody(APOLLO_PROG *apolloProg_,
                       std::set<BoneStmt *> &RestrictTo_)
  : RestrictTo(RestrictTo_) {
  apolloProg = apolloProg_;
}

std::map<BoneStmt *, osl_generic_p> BoneToBody::visitStmt(BoneStmt *Stmt, int) {
  if (!this->RestrictTo.count(Stmt))
    return RetTy();
  const std::string Name = Stmt->name();
  osl_body_p Body = osl_body_malloc();
  // create the name.
  char *StmtName = strdup(Name.c_str());
  Body->expression = osl_strings_encapsulate(StmtName);
  // put the iterator names
  const StaticLoopInfo &LoopInfo =
      apolloProg->StaticInfo->Loops[Stmt->getParent()->getId()];
  const int NumParents = LoopInfo.depth();
  char **IteratorNames = (char **)malloc(sizeof(char *) * (NumParents + 1));
  IteratorNames[NumParents] = nullptr;
  for (int i = 0; i < NumParents; ++i) {
    char *ItName = (char *)malloc(sizeof(char) * 8);
    std::sprintf(ItName, "vi_%d", LoopInfo.Parents[i]->Id);
    IteratorNames[i] = ItName;
  }

  osl_strings_p Iterators = osl_strings_malloc();
  free(Iterators->string);
  Iterators->string = IteratorNames;
  Body->iterators = Iterators;
  osl_generic_p BodyGeneric = osl_generic_malloc();
  BodyGeneric->data = Body;
  BodyGeneric->interface = osl_body_interface();
  RetTy StmtSca;
  StmtSca[Stmt] = BodyGeneric;
  return StmtSca;
}

std::map<BoneStmt *, osl_generic_p> BoneToBody::visitLoop(BoneLoop *Loop, int) {
  RetTy StmtsToBody;
  for (auto child : Loop->children()) {
    const RetTy ChildStmts = this->visit(child, 0);
    for (auto &kv : ChildStmts) {
      StmtsToBody[kv.first] = kv.second;
    }
  }
  return StmtsToBody;
}
