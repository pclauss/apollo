﻿//===--- Switching.cpp ----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include <JitCompiler/CodeGenerator.h>
#include "Switching.h"
#include "StaticInfo/StaticInfo.h"
#include "CodeBones/CodeBones.h"
#include "CodeBones/ArrayInfo.h"
#include "Interpolation/Interpolation.h"
#include "RangeAnalysis/RangeAnalysis.h"
#include "Backup/Backup.h"
#include "Backup/MemProtection.h"
#include "Backdoor/Backdoor.h"
#include "RuntimeConfig.h"

#include <iostream>
#include <cstdlib>
#include <cassert>
#include <set>
#include <string>

APOLLO_PROG *apolloProg;
RuntimeConfig apolloRuntimeConfig;

using namespace apollo;

extern "C" {
long apollo_runtime_hook(const int64_t PragmaId,
		struct CodeGenerator::StaticSkeletonInfo *RawSkeletons,
		const long NumberOfSkeletons, ParamTy Params, const long PhiStateSize) {

	traceEnter("apollo_runtime_hook");
	eventStart("apollo");

	MemoryProtection MemPro; // this must be the first thing created!
	CodeGenerator CodeGen(RawSkeletons, NumberOfSkeletons);
	PhiState PhiStateVal(PhiStateSize);

	SwitchingMechanism Switching(Params, PhiStateVal, CodeGen);
	const long SkeletonResult = Switching.run();

	for (auto plug : apolloRuntimeConfig.ProfilingPlugins) {
		plug->LoopCompleted();
	}

	eventEnd("apollo");
	traceExit("apollo_runtime_hook");
	return SkeletonResult;
}
} // end of extern "C"

static void cleanLastSession() {
	apolloProg->InstrumentationResults = nullptr;
	apolloProg->PredictionModel = nullptr;
	apolloProg->ExtremeBounds.clear();
	apolloProg->ExtremeMemRanges.clear();
	apolloProg->CodeBonesTransformation = nullptr;
}

SwitchingMechanism::SwitchingMechanism(const ParamTy &Params_,
		apollo::PhiState &Phi_, apollo::CodeGenerator &CG_):
		State(true), CodeGenerator(CG_), PhiState(Phi_) {

	apolloProg = new APOLLO_PROG;
	apolloProg->State = &State;

	// register the static info
	apolloProg->StaticInfo.reset(new StaticNestInfo());
	CodeGenerator.callRegisterStaticInfo(apolloProg->StaticInfo.get());

	if (backdoorEnabled("info")) {
		std::stringstream OS;
		apolloProg->StaticInfo->dump(OS);
		Info(
				backdoorutils::concat(
						"SwitchingMechanism::SwitchingMechanism() ", OS.str()));
	}

	Params.Param = Params_;
	Params.SizeInBytes = apolloProg->StaticInfo->ParamsSize;

	apolloProg->Param = Params;
}

SwitchingMechanism::~SwitchingMechanism() {
	delete apolloProg;
}

long SwitchingMechanism::runOriginalChunk(const long Lower, const long Upper) {
	traceStep("Original", Lower, Upper);
	traceEnter(
			backdoorutils::concat("SwitchingMechanism::runOriginalChunk(",
					Lower, ",", Upper, ")"));

	// SkeletonResult value is non zero if the original exit condition was reached
	eventStart("original_skeleton");
	const long SkeletonResult = CodeGenerator.callOriginal(Params, PhiState,
			Lower, Upper);
	eventEnd("original_skeleton");

	traceExit(
			backdoorutils::concat("SwitchingMechanism::runOriginalChunk = ",
					SkeletonResult));
	return SkeletonResult;
}

long SwitchingMechanism::runInstrumentationChunk(const long Lower,
		const long Upper) {
	traceStep("Instrument", Lower, Upper);
	traceEnter(
			backdoorutils::concat("runInstrumentationChunk(", Lower, ",", Upper,
					")"));
	const long InstInner =
			apolloRuntimeConfig.InstrumentationSample == -1 ?
					std::numeric_limits<int>::max() :
					apolloRuntimeConfig.InstrumentationSample;
	const long InstOuter = InstInner;

	// initialize an instrumentation structure and clean the previous
	// instrumentation results
	auto Results = apolloProg->InstrumentationResults = std::make_shared<
			InstrumentationResult>(apolloProg->StaticInfo.get(),
			apolloRuntimeConfig.InstrumentationChunkSize);

	// SkeletonResult value is non zero if the original exit condition was reached
	eventStart("instrumentation_skeleton");
	const long SkeletonResult = CodeGenerator.callInstrumentation(Params,
			PhiState, Results.get(), Lower, Upper, InstOuter, InstInner);

	// Let profiling plugins do something with results
	if ((SkeletonResult == 0 || Upper == State.maxChunkUpper()) && apolloRuntimeConfig.profilingMode) {
		for (auto plug : apolloRuntimeConfig.ProfilingPlugins) {
			plug->InstrumentationChunkCompleted(Lower, Upper, *Results.get());
		}
	}

	eventEnd("instrumentation_skeleton");
	traceExit(
			backdoorutils::concat("runInstrumentationChunk = ",
					SkeletonResult));
	return SkeletonResult;
}

SwitchingMechanism::TxSelectionResult SwitchingMechanism::runTxSelectionChunk(
		const long Lower) {

	traceStep("Transform", Lower);
	traceEnter(
			backdoorutils::concat("SwitchingMechanism::runTxSelectionChunk(",
					Lower, ", ... )"));

	bool MergeWithMainThread = false;
	TxSelectionResult Result;
	Result.ExecutedUntil = Lower;
	Result.SkeletonResult = 0;
	Result.State = ChunkResult::TransformationNotFound;

	// Launch original skeleton in parallel of transformation in normal mode
	// This allows to hide part of the cost of the transformation
	auto originalSkeletonFunction =
			[&]() -> long {
				eventStart("original_skeleton");
				const long Step = 1;
				while (Result.SkeletonResult == 0 && !MergeWithMainThread) {
					const long L = Result.ExecutedUntil;
					const long H = L + Step;
					Result.SkeletonResult = CodeGenerator.callOriginal(Params, PhiState, L, H);
					Result.ExecutedUntil = H;
				}
				eventEnd("original_skeleton");
				return Result.ExecutedUntil;
			};
	std::launch Mode;
	if (apolloRuntimeConfig.profilingMode) {
		Mode = std::launch::deferred;
	} else if (apolloRuntimeConfig.NumberOfThreads == 1) {
		Mode = std::launch::deferred;
	} else {
		Mode = std::launch::async;
	}
	auto OriginalSkeletonThread = std::async(Mode, originalSkeletonFunction);

	// Don't run transformation if instrumentation failed
	// or if original skeleton launched in parallel failed
	// Stop original thread skeleton above before returning
	if (!apolloProg->InstrumentationResults || Result.SkeletonResult != 0) {
		cleanLastSession();
		MergeWithMainThread = true;
		OriginalSkeletonThread.get();
		traceExit("runTxSelectionChunk early exit(after instrumentation)");
		return Result;
	}

	// Create prediction models
	auto PM = PredictionModel::buildFromInstrumentation(
			*apolloProg->InstrumentationResults, !apolloRuntimeConfig.predictAll);
	if (!PM || PM->hasNonPredictedEntities() || Result.SkeletonResult != 0) {
		cleanLastSession();
		MergeWithMainThread = true;
		OriginalSkeletonThread.get();
		traceExit("runTxSelectionChunk early exit(after interpolation)");
		return Result;
	}
	apolloProg->PredictionModel = PM;

	// Computes memory ranges for all statements
	computeExtremeBoundsAndMemoryRanges(apolloProg);

	// Patch the code bones IR module saved as a string in the binary code
	CodeGenerator.patchModules(Params, PM);

	// Break if original skeleton launched in parallel failed
	if (Result.SkeletonResult != 0) {
		cleanLastSession();
		MergeWithMainThread = true;
		OriginalSkeletonThread.get();
		traceExit("runTxSelectionChunk early exit(params patching)");
		return Result;
	}

	// Recover information about arrays to simplify coefficients
	apolloProg->AA = nullptr;
	apolloProg->AA = std::make_shared<ArrayAnalysis>(apolloProg);

	// Get scop
	apolloProg->CodeBonesTransformation = nullptr;
	apollo::CodeBoneManager CodeBoneMgr(apolloProg, CodeGenerator);
	apollo::ScopPtrTy OrigScop = CodeBoneMgr.getScop();

	// Run pluto
	apollo::ScopPtrTy TxScop = CodeBoneMgr.getOptimized(OrigScop);

	// Break if original skeleton launched in parallel failed
	if (Result.SkeletonResult != 0) {
		cleanLastSession();
		MergeWithMainThread = true;
		OriginalSkeletonThread.get();
		traceExit("runTxSelectionChunk early exit(tx selection)");
		return Result;
	}

	// Run Cloog Scan
	const apollo::ScanPtrTy Scan = CodeBoneMgr.getScan(TxScop);
	apolloProg->CodeBonesTransformation = Scan;

	// Break if scan failed or if original skeleton launched in parallel failed
	if (!Scan || Result.SkeletonResult != 0) {
		cleanLastSession();
		MergeWithMainThread = true;
		OriginalSkeletonThread.get();
		traceExit("runTxSelectionChunk early exit(scan)");
		return Result;
	}

	// Run LLVM JIT
	std::shared_ptr<ScanTypeStruct> AdvancedTx =
			apolloProg->CodeBonesTransformation;
	CodeGenerator.generateOptimizedCode(AdvancedTx);

	// Stop original thread and return success
	MergeWithMainThread = true;
	OriginalSkeletonThread.get();
	Result.State = ChunkResult::Success;
	traceExit("runTxSelectionChunk success");

	// Let profiling plugins do something with results
	if (apolloRuntimeConfig.profilingMode) {
		for (auto plug : apolloRuntimeConfig.ProfilingPlugins) {
			plug->PlutoCompleted(OrigScop, TxScop);
		}
	}

	return Result;
}

ChunkResult SwitchingMechanism::runVerificationCode(const long Lower,
		const long Upper) {

	std::shared_ptr<PredictionModel> PM = apolloProg->PredictionModel;
	apollo::PhiState NewPhiState = PhiState.clone();

	eventStart("parallel_skeleton_verif");
	CodeGenerator.callVerificationOnly(Params, NewPhiState, PM, Lower, Upper);
	eventEnd("parallel_skeleton_verif");

	const bool EarlyRollback = apolloProg->ShouldRollback;
	if (EarlyRollback) {
		return ChunkResult::EarlyMissprediction;
	}
	return ChunkResult::Success;
}

ChunkResult SwitchingMechanism::runOptimizedCode(const long Lower,
		const long Upper) {
	const bool SupportsInspectorExecutor =
			apolloProg->CodeBonesTransformation->SupportInspectorExecutor;

	// If inspector-executor is supported there is no need to backup
	if (!SupportsInspectorExecutor) {
		const bool BackupSuceed = doBackup(apolloProg);
		if (!BackupSuceed)
			return ChunkResult::RangeAnalysisFailed;
	}

	std::shared_ptr<PredictionModel> PM = apolloProg->PredictionModel;
	apollo::PhiState NewPhiState = PhiState.clone();

	eventStart("parallel_skeleton");
	CodeGenerator.callOptimized(Params, NewPhiState, PM, Lower, Upper);
	eventEnd("parallel_skeleton");

	const bool Succed = !apolloProg->ShouldRollback;

	if (Succed)
		PhiState = NewPhiState;
	else
		traceStep("RollBack", Lower, Upper);

	clearBackup(apolloProg);

	return Succed ? ChunkResult::Success : ChunkResult::Missprediction;
}

ChunkResult SwitchingMechanism::runOptimizedChunk(const long Lower,
		const long Upper) {
	traceStep("Optimized", Lower, Upper);
	traceEnter(
			backdoorutils::concat("runOptimizedChunk(", Lower, ",", Upper,
					")"));

	// Check the bounds read and write
	const bool RangeAnalysis = computeExtremeBoundsAndMemoryRanges(apolloProg)
			&& checkMemoryExtremes(apolloProg);
	if (!RangeAnalysis) {
		traceExit("runOptimizedChunk = RangeAnalysisFailed");
		return ChunkResult::RangeAnalysisFailed;
	}

	const ChunkResult VerificationResult = runVerificationCode(Lower, Upper);

	if (VerificationResult != ChunkResult::Success) {
		traceExit("runOptimizedChunk = EarlyMisprediction");
		return VerificationResult;
	}

	const ChunkResult ComputationResult = runOptimizedCode(Lower, Upper);

	traceExit("runOptimizedChunk");
	return ComputationResult;
}

long SwitchingMechanism::run() {
	traceEnter("SwitchingMechanism::run");
	State.nextState();
	while (true) {
		const SwitchingState::ApolloStateTypes Decision = State.getCurState();
		const long Lower = State.chunkLower();
		const long Upper = State.chunkUpper();
		long SkeletonResult = 0;
		long ExecutedUntil = -1;

		switch (Decision) {
		case SwitchingState::ApolloStateTypes::Instrument:
			SkeletonResult = this->runInstrumentationChunk(Lower, Upper);
			apolloProg->LastChunkResult = ChunkResult::Success;
			break;
		case SwitchingState::ApolloStateTypes::Original: {
			SkeletonResult = this->runOriginalChunk(Lower, Upper);
			apolloProg->LastChunkResult = ChunkResult::Success;
			break;
		}
		case SwitchingState::ApolloStateTypes::TxSelection: {
			const TxSelectionResult TxSelection = this->runTxSelectionChunk(
					Lower);
			apolloProg->LastChunkResult = TxSelection.State;
			ExecutedUntil = TxSelection.ExecutedUntil;
			SkeletonResult = TxSelection.SkeletonResult;
			break;
		}
		case SwitchingState::ApolloStateTypes::Optimized: {
			apolloProg->LastChunkResult = this->runOptimizedChunk(Lower, Upper);
			break;
		}
		default:
			assert(false && "switching: invalid case.");
		}

		if (SkeletonResult != 0) {
			traceExit("SwitchingMechanism::run");
			return SkeletonResult;
		}
		State.nextState(apolloProg->LastChunkResult, ExecutedUntil);
	}
	assert(false && "switching: unreachable.");
}

