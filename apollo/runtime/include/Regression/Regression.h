//===--- Regression.h -----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef REGRESSIONTUBE_H
#define REGRESSIONTUBE_H

#include "Instrumentation/Instrumentation.h"
#include "Interpolation/Interpolation.h"
#include "armadillo"

#include <vector>
#include <map>

namespace Regression {

const double RegressionThreshold = 0.9;

struct SystemOfEquations {
  // constructor
  SystemOfEquations(const int NumIterators, const int NumEvents) {
    Values = std::vector<double>(NumEvents, 0);
    VirtualIterators = std::vector<std::vector<long>>(
        NumEvents, std::vector<long>(NumIterators, 0));
  }
  std::vector<double> Values;
  std::vector<std::vector<long>> VirtualIterators;
};

// Solve the linear equation using regression
std::vector<double> computeRegressionLine(const SystemOfEquations &AllValues);

// Finds the maximum and minimum distance of the points from the plane
void populateRegressionQuality(const std::vector<double> &Plane,
                               const SystemOfEquations &SystemOfEq,
                               long &MaxPosError, long &MaxNegError, double &RC);

void nonAffineBoundAnalysis(std::vector<long> &Plane, long &Max,
                            double &RC, const InstrumentationResult &IR,
                            const int NonLinearLoopID, bool &IsTube);


// Construct regression line and tube width for a single stmt
void nonAffineMemoryAnalysis(std::vector<long> &Plane, long &TubeWidth,
		double &RC,
		const std::vector<InstrumentationResult::InstrumentationEvent> &EventList,
		const int nonLinearMemoryID, bool &IsTube);

double computeCorrelationCoefficient(const SystemOfEquations &SystemOfEq,
                                     const std::vector<double> &PredictedValues);
}; 

#endif
