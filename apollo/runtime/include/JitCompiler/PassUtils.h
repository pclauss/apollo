//===--- PassUtils.h ------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef UTILS_H
#define UTILS_H

#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"

#include <string>
#include <set>
#include <map>

// This mimic functions defined in the static part.
bool isStatement(llvm::Instruction *I);
bool isBasicScalar(llvm::Instruction *I);
bool hasMetadataKind(llvm::Instruction *I, const std::string &Str);
int getMetadataNumOperands(llvm::Instruction *I, const std::string &Str);
llvm::Value *getMetadataOperand(llvm::Instruction *I, const std::string &Str,
                                int op = 0);

// defined down
template <typename T> 
std::string toString(T Input);

/// \brief for printing vectors.
template <typename T>
llvm::raw_ostream &operator<<(llvm::raw_ostream &OS, const std::vector<T> &V) {
  OS << "[ ";
  for (auto v : V) {
    OS << toString<T>(v) << " ";
  }
  OS << "]";
  return OS;
}

/// \brief for printing sets.
template <typename T>
llvm::raw_ostream &operator<<(llvm::raw_ostream &OS, const std::set<T> &S) {
  OS << "{ ";
  for (auto s : S) {
    OS << toString<T>(s) << " ";
  }
  OS << "}";
  return OS;
}

/// \brief for printing maps.
template <typename T, typename K>
llvm::raw_ostream &operator<<(llvm::raw_ostream &OS, const std::map<T, K> &M) {
  OS << "{ ";
  for (auto m : M) {
    OS << toString<T>(m.first) << " : " << toString<K>(m.second) << " ";
  }
  OS << "}";
  return OS;
}

/// \brief converts its parameter to std::string format.
template <typename T> 
std::string toString(T Str) {
  std::string Empty = "";
  llvm::raw_string_ostream OS(Empty);
  OS << Str;
  return OS.str();
}

/// \brief it takes a list of parameters (see utils::to_string to extend the
///        supported types), converts them to std::string, and concatenates them
template <typename T> 
std::string concat(T Str) { 
  return toString<T>(Str); 
}

/// \brief it takes a list of parameters (see utils::to_string to extend the
///        supported types), converts them to std::string, and concatenates them
template <typename T1, typename T2, typename... Args>
std::string concat(T1 A, T2 B, Args... As) {
  return concat<T1>(A) + concat<T2, Args...>(B, As...);
}

#endif
