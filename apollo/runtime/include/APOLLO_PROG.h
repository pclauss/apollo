//===--- APOLLO_PROG.h ----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef APOLLO_PROG_H
#define APOLLO_PROG_H

#include "StaticInfo/StaticInfo.h"
#include "Backup/BackupContainer.h"
#include "Backup/MemProtection.h"
#include "Backup/AllocatedMemArrange.h"
#include "SwitchingState/SwitchingState.h"
#include "Params.h"
#include "armadillo"

#include <map>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <cstdint>
#include <climits>

struct ScanTypeStruct;
class ArrayAnalysis;

/// The structure containing a loop nest. Groups everything here
/// so that multiple loop nest can be handled easy in FUTURE
struct APOLLO_PROG {
  /// Tells if the last chunk was successful
  ChunkResult LastChunkResult;

  /// This flag is set when a rollback call is received
  bool ShouldRollback = false;

  /// The loop global variables: Packed in a structure.
  /// They are loop invariant.
  NestParamsStruc Param;

  /// Static nest information
  std::unique_ptr<StaticNestInfo> StaticInfo;

  /// Results of instrumentation
  std::shared_ptr<struct InstrumentationResult> InstrumentationResults;
  std::shared_ptr<struct PredictionModel> PredictionModel;

  /// The range of backed up data
  BackupContainer BackupContainerObj;

  /// Array analysis to simplify coefficients
  std::shared_ptr<ArrayAnalysis> AA;

  /// Code bones transformation
  std::shared_ptr<ScanTypeStruct> CodeBonesTransformation;

  /// The state of the chunked execution
  SwitchingState *State;

  /// The minimum and maximum values for the loop bounds.
  /// For the current ChunkLower and ChunkUpper.
  std::vector<BoundRangeTy> ExtremeBounds;
  std::vector<MemRangeTy> ExtremeMemRanges;

  /// An approximation of the number of iterations for the next chunk.
  double ChunkNumberOfIterations;
};

#endif
