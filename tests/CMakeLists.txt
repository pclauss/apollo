cmake_minimum_required(VERSION 3.4.3)
project(apollo_tests)

##
# Required values

# CMAKE_BINARY_DIR: The toplevel build directory
if (NOT DEFINED CMAKE_BINARY_DIR)
  message(ERROR "CMAKE_BINARY_DIR must be set")
endif ()
message(STATUS "Configuring tests for build in ${CMAKE_BINARY_DIR}")

# APOLLO_BUILD_DIR: The build directory for APOLLO
if (NOT DEFINED APOLLO_BUILD_DIR)
 set(APOLLO_BUILD_DIR ${CMAKE_BINARY_DIR}/apollo)
endif ()
message(STATUS "Tests will use APOLLO built in ${APOLLO_BUILD_DIR}")
set(APOLLO_BINARY_DIR ${APOLLO_BUILD_DIR}/bin)

# APOLLO_LLVM_BUILD_DIR: The build directory for the Apollo build of LLVM
if (NOT DEFINED APOLLO_LLVM_BUILD_DIR)
 set(APOLLO_LLVM_BUILD_DIR ${CMAKE_BINARY_DIR}/llvm)
endif ()
message(STATUS "Tests will use LLVM built in ${APOLLO_LLVM_BUILD_DIR}")
set(APOLLO_LLVM_BINARY_DIR ${APOLLO_LLVM_BUILD_DIR}/bin)

set(APOLLO_BENCHS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/apollo-benchs)
set(POLYBENCH_DIR ${CMAKE_CURRENT_SOURCE_DIR}/PolyBench)

add_subdirectory(lit-tests)
add_subdirectory(correctness-tests)