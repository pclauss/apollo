#!/usr/bin/python3

import argparse
import commons
import filecmp
import os
import tomllib

from pathlib import Path

RESULT_FILE = 'result.txt'

def directory_check(build_path, subdirectory, src_path, config_data, args):

    # Check directories where built tests should be
    res_apollo_path = Path(build_path, commons.BUILD_APOLLO_ROOT_PATH, subdirectory)
    if not res_apollo_path.exists():
        raise FileNotFoundError(f'The build-apollo does not exist in {build_path}')
    res_clang_path = Path(build_path, commons.BUILD_CLANG_ROOT_PATH, subdirectory)
    if not res_clang_path.exists():
        raise FileNotFoundError(f'The build-apollo does not exist in {build_path}')
    
    # Explore the executables to trigger tests and check consistency
    for exe in config_data['name']:
        
        # We setup the variables
        inputs = {'empty': [[]]}
        my_env = os.environ.copy()

        # For each test directory, we search the execution inputs
        src_exe_path = src_path.joinpath(exe)
        inputs_path = src_exe_path.joinpath(commons.INPUTS_PATH)
        if inputs_path.exists():
            inputs_options = commons.parse_inputs_file(inputs_path, str(src_exe_path))
            inputs = inputs_options['inputs']
            options = inputs_options['options']
            if args.details:
                options['APOLLO_BACKDOOR'] = '--stdio --time'
            my_env.update(options)
        
        # For each input, we have to make a comparison
        for input_size, input_args in inputs.items():
            # We execute the tests in build-apollo and build-clang
            exe_apollo_build = res_apollo_path.joinpath(exe, exe)
            exe_apollo_result = res_apollo_path.joinpath(exe, RESULT_FILE)
            if exe_apollo_result.exists():
                exe_apollo_result.unlink()
            exe_clang_build = res_clang_path.joinpath(exe, exe)
            exe_clang_result = res_clang_path.joinpath(exe, RESULT_FILE)
            if exe_clang_result.exists():
                exe_clang_result.unlink()
            if exe_apollo_build.exists() and exe_clang_build.exists():
                print(commons.Bcolors.HEADER + f'Running {exe} with {input_size} inputs' + commons.Bcolors.ENDC)
                print(commons.Bcolors.HEADER + f'Running apollo version of {exe}' + commons.Bcolors.ENDC)
                commons.run_cmd([str(exe_apollo_build), *input_args[0]], my_env, res_apollo_path.joinpath(exe), args.details)
                print(commons.Bcolors.HEADER + f'Running clang version of {exe}' + commons.Bcolors.ENDC)
                commons.run_cmd([str(exe_clang_build), *input_args[0]], my_env, res_clang_path.joinpath(exe), args.details)
                print(commons.Bcolors.HEADER + f'Checking diff between {exe_apollo_result} and {exe_clang_result}' + commons.Bcolors.ENDC)
                if filecmp.cmp(exe_apollo_result, exe_clang_result):
                    print(commons.Bcolors.OKGREEN + '\t' + 'similar outputs between apollo and clang' + commons.Bcolors.ENDC)
                else:
                    print(commons.Bcolors.FAIL + '\t' + 'different outputs between apollo and clang' + commons.Bcolors.ENDC)

def main():

    # Argument parser
    parser = argparse.ArgumentParser(description='Check APOLLO consistency')
    parser.add_argument('-b', '--build', help='The location of the built tests',
                        required=True)
    parser.add_argument('--apollo-benchs', help='The location of apollo-benchs')
    parser.add_argument('--polybench', help='The location of PolyBench')
    parser.add_argument('--correctness', help='The location of standard correctness tests')
    parser.add_argument('--details', action=argparse.BooleanOptionalAction,
                        default=False)
    args = parser.parse_args()
    script_path = os.path.dirname(os.path.realpath(__file__))
    build_path = args.build

    # We parse the config
    config_data = {}
    with Path(script_path, 'config.toml').open('rb') as config_f:
        config_data = tomllib.load(config_f)

    # Explore the test directories
    if args.apollo_benchs:
        directory_check(Path(build_path, 'apollo-benchs'),
                        commons.BENCHS_SRC,
                        Path(args.apollo_benchs, commons.BENCHS_SRC),
                        config_data['executable']['apollo-benchs'],
                        args)
    if args.correctness:
        directory_check(Path(build_path, 'correctness'),
                        '',
                        Path(args.correctness),
                        config_data['executable']['correctness'],
                        args)
    if args.polybench:
        for exe in config_data['executable']['polybench']['name']:
            for size in config_data['executable']['polybench']['size']:
                commons.run_cmd([str(Path(script_path, 'polybench-correctness-check.sh')), str(Path(args.polybench, exe)), size, args.polybench],
                                os.environ, Path(build_path, 'polybench'), True)
                    
if __name__ == "__main__":
    main()