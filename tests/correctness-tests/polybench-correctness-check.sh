#!/bin/sh

POLYBENCH_DIR=$3

UTILITIES_DIR=$POLYBENCH_DIR/utilities/
POLYBENCH_SRC=$POLYBENCH_DIR/utilities/polybench.c

function print_usage() {
  echo "Usage: $0 <file> <dataset_size> <polybench_dir>"
}

# Handle dataset size
function handle_size() {
  case $1 in
    "mini") dataset_size=$(echo $1|tr a-z A-Z)_DATASET;;
    "small") dataset_size=$(echo $1|tr a-z A-Z)_DATASET;;
    "medium") dataset_size=$(echo $1|tr a-z A-Z)_DATASET;;
    "large") dataset_size=$(echo $1|tr a-z A-Z)_DATASET;;
    "extralarge") dataset_size=$(echo $1|tr a-z A-Z)_DATASET;;
    "") dataset_size="MEDIUM_DATASET";;
    *) print_usage; exit 1;;
  esac
}

function replace_scop_with_dcop() {
  local filename=$1
  sed -i 's/#pragma scop/#pragma apollo dcop\n{/' $filename
  sed -i 's/#pragma endscop/}/' $filename
}

function compile() {
  local filename=$1
  local dataset_size=$2
  local cc=$3
  local out_name=$4
  dir=$(dirname $filename)
  $cc $filename $POLYBENCH_SRC -I$dir -I$UTILITIES_DIR -o $out_name -D$dataset_size -DPOLYBENCH_DUMP_ARRAYS -DPOLYBENCH_TIME -lm
}

function execute() {
  local bin_name=$1
  local bin_time=${bin_name}_time.txt
  local bin_result=${bin_name}_results.txt
  rm ${bin_result}
  timeout 120s ./${bin_name} > ${bin_time} 2> ${bin_result}
  if [[ $? -eq 124 ]]; then
    echo "timeout"
  else
    cat ${bin_time} | tail -1
  fi
}

if [[ $# -lt 2 ]]; then
  print_usage
  exit 1
fi

filename=$1
handle_size $2
echo "Copying $filename"

dir=$(dirname $filename)
base=$(basename $filename)
apollo_name=$dir/$base.apollo.c
cp $filename $apollo_name

echo "Replacing pragma scop with pragma dcop in source code"
replace_scop_with_dcop $apollo_name

bin_name=$(basename $filename .c)

echo "Compiling file with clang"
compile $apollo_name $dataset_size "clang -O3" ${bin_name}_clang

echo "Compiling file with apollo"
compile $apollo_name $dataset_size "apolloc -O3" ${bin_name}_apollo

echo "Executing clang version"
clang_time=$(execute ${bin_name}_clang)
if [[ ${clang_time} == "timeout" ]]; then
  echo "$(tput setaf 1)NOT GOOD$(tput sgr0)"
  echo "${bin_name}_clang timed out"
  exit
fi

export APOLLO_BACKDOOR="--file=${bin_name}_apollo.out --step"
echo "Executing apollo version"
apollo_time=$(execute ${bin_name}_apollo)
if [[ ${apollo_time} == "timeout" ]]; then
  echo "$(tput setaf 1)NOT GOOD$(tput sgr0)"
  echo "${bin_name}_apollo timed out"
  exit
fi

echo "File difference:"
# Apollo can sometimes print stuff
sed -ni '/^==BEGIN DUMP_ARRAYS==/,/^==END   DUMP_ARRAYS==/{p;/^==END   DUMP_ARRAYS==/q}' ${bin_name}_apollo_results.txt
difference=$(diff ${bin_name}_clang_results.txt ${bin_name}_apollo_results.txt > /dev/null)
if [[ $? -eq 0 ]]; then
  echo "$(tput setaf 2)ALL GOOD$(tput sgr0)"
  speedup=$(echo "$clang_time / $apollo_time" | bc -l)
  echo "Apollo speedup: $speedup"
else
  echo "$(tput setaf 1)NOT GOOD$(tput sgr0)"
  echo $difference
fi
