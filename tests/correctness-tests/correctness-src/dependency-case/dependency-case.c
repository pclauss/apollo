#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/cdefs.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>

#define START_TIMER()                                                          \
  struct timeval start, end;                                                   \
  gettimeofday(&start, NULL)

#define STOP_TIMER() gettimeofday(&end, NULL)

#define DISPLAY_TIME(str)                                                      \
  {                                                                            \
    double time_taken = (end.tv_sec - start.tv_sec) * 1e6;                     \
    time_taken = (time_taken + (end.tv_usec - start.tv_usec)) * 1e-6;          \
    printf(str ": %fs\n", time_taken);                                         \
  }

#define VEC_SIZE 100000

#define ALLOC_VEC(size) (vec_type *)malloc(sizeof(vec_type) * size)

#define ADD_VEC_IMPL                                                           \
  {                                                                            \
    for (int i = 2; i < size; ++i) {                                           \
      B[i] = B[i - 1] + B[i - 2];\
    }                                                                          \
  }

typedef double vec_type;

__attribute__((noinline)) void add_vec(vec_type *A, vec_type *B, int size, int *C) {
  START_TIMER();
  ADD_VEC_IMPL
  STOP_TIMER();
  DISPLAY_TIME("original function")
}

__attribute__((noinline)) void add_vec_apollo(vec_type *A, vec_type *B,
                                              int size, int* C) {
  START_TIMER();
#pragma apollo dcop
  {ADD_VEC_IMPL} STOP_TIMER();
  DISPLAY_TIME("apollo function")
}

float randf() { return (float)rand() / (float)(RAND_MAX / 1.); }

int main() {
  vec_type input = randf() * 1000;
  srand(time(NULL));
  vec_type *A = ALLOC_VEC(VEC_SIZE);
  vec_type *A_apollo = ALLOC_VEC(VEC_SIZE);
  vec_type *B = ALLOC_VEC(VEC_SIZE);
  vec_type *B_apollo = ALLOC_VEC(VEC_SIZE);
  int *C = malloc(sizeof(int) * VEC_SIZE);
  int *C_apollo = malloc(sizeof(int) * VEC_SIZE);
  for (int i = 0; i < VEC_SIZE; ++i) {
    A[i] = randf();
    A_apollo[i] = A[i];
    B[i] = randf();
    B_apollo[i] = B[i];
    C[i] = i;
    C_apollo[i] = i;
  }

  add_vec(A, B, VEC_SIZE, C);
  add_vec_apollo(A_apollo, B_apollo, VEC_SIZE, C_apollo);

  // Check validity of orig / apollo
  FILE *result = fopen("result.txt", "w+");
  for (int i = 0; i < VEC_SIZE; ++i) {
    fprintf(result, "%d\n", B[i] == B_apollo[i]);
  }
  fclose(result);

  printf("Results are good :)\n");
}
