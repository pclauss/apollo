Compilation tests
=================

Tests on the compilation phases. Executable programs aren't expected
to be produced or run.
